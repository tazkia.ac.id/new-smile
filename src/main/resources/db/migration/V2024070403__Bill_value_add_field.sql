
alter table `bill_value`
    add column `type` varchar(45);

alter table `bill_value`
    add column `installment` int;

alter table `bill_value`
    add column `start_date` date;