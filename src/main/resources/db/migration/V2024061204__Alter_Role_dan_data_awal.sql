ALTER TABLE `s_role`
    ADD COLUMN `after_url_redirect` VARCHAR(45) NULL AFTER `name`;

ALTER TABLE `s_role_permission`
    CHANGE COLUMN `id_permission` `id_permission` VARCHAR(45) NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`id_role`, `id_permission`);
;


ALTER TABLE `s_user_role`
    CHANGE COLUMN `id_role` `id_role` VARCHAR(45) NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`id_user`, `id_role`);
;

ALTER TABLE `employes`
    ADD COLUMN `photo` VARCHAR(255) NULL AFTER `id_user`;


INSERT INTO `foudation` (`id`, `foundation_name`, `description`, `status`) VALUES ('01', 'Yayasan Cendikia Tazkia', '-', 'ACTIVE');

INSERT INTO `institution` (`id`, `institution_name`, `description`, `status`, `id_foundation`) VALUES ('01', ' INSTITUT AGAMA ISLAM TAZKIA', '-', 'ACTIVE', '01');

INSERT INTO `department` (`id`, `id_institution`, `department_name`, `description`, `status`) VALUES ('01', '01', 'Test', 'Test', 'ACTIVE');

INSERT INTO `institution_faculty` (`id`, `id_institution`, `id_department`, `faculty_name`, `description`, `active_status`, `status`) VALUES ('01', '01', '01', 'Ekonomi dan Bisnis Syariah', 'Islamic Economics and Business', 'ACTIVE', 'ACTIVE');
INSERT INTO `institution_faculty` (`id`, `id_institution`, `id_department`, `faculty_name`, `description`, `active_status`, `status`) VALUES ('02', '01', '01', 'Syariah', 'Hukum Islam', 'ACTIVE', 'ACTIVE');
INSERT INTO `institution_faculty` (`id`, `id_institution`, `id_department`, `faculty_name`, `description`, `active_status`, `status`) VALUES ('8a6a20d4-0d43-45a1-93cf-61bab3208f04', '01', '01', 'Pascasarjana', 'Pascasarjana', 'ACTIVE', 'ACTIVE');
INSERT INTO `institution_faculty` (`id`, `id_institution`, `id_department`, `faculty_name`, `description`, `active_status`, `status`) VALUES ('da7a3026-9372-45f5-a0ac-ee519d53c6b1', '01', '01', 'Tarbiyah', 'Tarbiyah', 'ACTIVE', 'ACTIVE');

INSERT INTO `institution_prodi` (`id`, `id_faculty`, `id_department`, `prodi_name`, `description`, `status`) VALUES ('01', '01', '01', 'Manajemen Bisnis Syariah', 'Manajemen Bisnis Syariah', 'ACTIVE');

INSERT INTO `institution_program` (`id`, `id_prodi`, `program_code`, `program_name`, `description`, `status`) VALUES ('01', '01', '2', 'Reguler', 'Program Umum Mahasiswa', 'ACTIVE');




