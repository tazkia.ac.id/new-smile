ALTER TABLE `employee_lecturer`
    CHANGE COLUMN `position_functioanl` `position_functional` VARCHAR(45) NULL DEFAULT NULL ;

CREATE TABLE `institution_concentration` (
    `id` VARCHAR(45) NOT NULL,
    `id_prodi` VARCHAR(45) NULL,
    `concentration_name` VARCHAR(45) NULL,
    `status` VARCHAR(45) NULL,
    `last_update` DATETIME NULL,
    `user_update` VARCHAR(45) NULL,
    PRIMARY KEY (`id`));

ALTER TABLE `employee_lecturer`
    ADD COLUMN `lecturer_type` VARCHAR(45) NULL AFTER `sk_position_fungtional`;
