ALTER TABLE `institution`
    ADD COLUMN `type_institution` VARCHAR(45) NULL AFTER `id_foundation`;

ALTER TABLE `department`
    ADD COLUMN `status_active` VARCHAR(45) NULL AFTER `user_update`;
