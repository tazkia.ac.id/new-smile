CREATE TABLE `student_academic_advisor` (
                                            `id` varchar(45) NOT NULL,
                                            `id_student` varchar(45) DEFAULT NULL,
                                            `id_employee` varchar(45) DEFAULT NULL,
                                            `id_lecturer` varchar(45) DEFAULT NULL,
                                            `status` varchar(45) DEFAULT NULL,
                                            `user_update` varchar(45) DEFAULT NULL,
                                            `last_update` datetime DEFAULT NULL,
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;