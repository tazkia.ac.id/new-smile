CREATE TABLE `bank` (
                        `id` varchar(45) NOT NULL,
                        `bank_code` varchar(45) DEFAULT NULL,
                        `bank_name` varchar(255) DEFAULT NULL,
                        `virtual_account_price` decimal(19,2) DEFAULT NULL,
                        `status` varchar(45) DEFAULT NULL,
                        `last_update` datetime DEFAULT NULL,
                        `user_update` varchar(45) DEFAULT NULL,
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `bill_type` (
                             `id` varchar(45) NOT NULL,
                             `bill_code` varchar(45) DEFAULT NULL,
                             `bill_name` varchar(150) DEFAULT NULL,
                             `description` longtext,
                             `status` varchar(45) DEFAULT NULL,
                             `last_update` datetime DEFAULT NULL,
                             `user_update` varchar(45) DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `bill_value` (
                              `id` varchar(45) NOT NULL,
                              `id_bill_type` varchar(45) DEFAULT NULL,
                              `id_period_academic` varchar(45) DEFAULT NULL,
                              `id_period_academic_finance` varchar(45) DEFAULT NULL,
                              `id_program` varchar(45) DEFAULT NULL,
                              `generation` varchar(45) DEFAULT NULL,
                              `id_prodi` varchar(45) DEFAULT NULL,
                              `value` decimal(10,0) DEFAULT NULL,
                              `status` varchar(45) DEFAULT NULL,
                              `last_update` datetime DEFAULT NULL,
                              `user_update` varchar(45) DEFAULT NULL,
                              `student_status` varchar(45) DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `building` (
                            `id` varchar(45) NOT NULL,
                            `id_institution` varchar(45) DEFAULT NULL,
                            `building_name` varchar(45) DEFAULT NULL,
                            `building_address` longtext,
                            `building_area` decimal(10,0) DEFAULT NULL,
                            `status` varchar(45) DEFAULT NULL,
                            `last_update` datetime DEFAULT NULL,
                            `user_update` varchar(45) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `building_floor` (
                                  `id` varchar(45) NOT NULL,
                                  `id_building` varchar(45) DEFAULT NULL,
                                  `floor` varchar(45) DEFAULT NULL,
                                  `status` varchar(45) DEFAULT NULL,
                                  `last_update` datetime DEFAULT NULL,
                                  `user_update` varchar(45) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `building_room` (
                                 `id` varchar(45) NOT NULL,
                                 `id_floor` varchar(100) DEFAULT NULL,
                                 `room_name` varchar(45) DEFAULT NULL,
                                 `capacity` int DEFAULT NULL,
                                 `dimension_length` decimal(10,0) DEFAULT NULL,
                                 `dimension_width` decimal(10,0) DEFAULT NULL,
                                 `dimension_height` decimal(10,0) DEFAULT NULL,
                                 `status` varchar(45) DEFAULT NULL,
                                 `last_update` datetime DEFAULT NULL,
                                 `user_update` varchar(45) DEFAULT NULL,
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `campus` (
                          `id` varchar(45) NOT NULL,
                          `id_institution` varchar(45) DEFAULT NULL,
                          `campus_name` varchar(45) DEFAULT NULL,
                          `description` varchar(45) DEFAULT NULL,
                          `address` varchar(45) DEFAULT NULL,
                          `status` varchar(45) DEFAULT NULL,
                          `last_update` datetime DEFAULT NULL,
                          `user_update` varchar(45) DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `curiculum` (
                             `id` varchar(45) NOT NULL,
                             `id_prodi` varchar(45) DEFAULT NULL,
                             `curiculum_code` varchar(45) DEFAULT NULL,
                             `curiculum_name` longtext,
                             `status` varchar(45) DEFAULT NULL,
                             `last_update` datetime DEFAULT NULL,
                             `user_update` varchar(45) DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `curiculum_subject` (
                                     `id` varchar(45) NOT NULL, 
                                     `id_curiculum` varchar(45) DEFAULT NULL,
                                     `id_subject` varchar(45) DEFAULT NULL,
                                     `semester` int DEFAULT NULL,
                                     `sks` decimal(10,0) DEFAULT NULL,
                                     `id_prodi` varchar(45) DEFAULT NULL,
                                     `id_konsentrasi` varchar(45) DEFAULT NULL,
                                     `silabus` longtext,
                                     `akses_subject` varchar(45) DEFAULT NULL,
                                     `block_note` varchar(45) DEFAULT NULL,
                                     `block_sempro` varchar(45) DEFAULT NULL,
                                     `block_thesis` varchar(45) DEFAULT NULL,
                                     `allow_transkript` varchar(45) DEFAULT NULL,
                                     `book_reference` longtext,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `curiculum_subject_equivalent` (
                                                `id` varchar(45) NOT NULL,
                                                `id_curiculum_subject` varchar(45) DEFAULT NULL,
                                                `id_curiculum_subject_equivalent` varchar(45) DEFAULT NULL,
                                                `status` varchar(45) DEFAULT NULL,
                                                `last_update` datetime DEFAULT NULL,
                                                `user_update` varchar(45) DEFAULT NULL,
                                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `curiculum_subject_precondition` (
                                                  `id` varchar(45) NOT NULL,
                                                  `id_curiculum_subject` varchar(45) DEFAULT NULL,
                                                  `id_curiculum_subject_precondition` varchar(45) DEFAULT NULL,
                                                  `minimum_weight` decimal(19,2) DEFAULT NULL,
                                                  `status` varchar(45) DEFAULT NULL,
                                                  `last_update` datetime DEFAULT NULL,
                                                  `user_update` varchar(45) DEFAULT NULL,
                                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `curiculum_subject_rps` (
                                         `id` varchar(45) NOT NULL,
                                         `id_curiculum_subject` varchar(45) DEFAULT NULL,
                                         `session` int DEFAULT NULL,
                                         `description` longtext,
                                         `file` longtext,
                                         `status` varchar(45) DEFAULT NULL,
                                         `last_update` datetime DEFAULT NULL,
                                         `user_update` varchar(45) DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `department` (
                              `id` varchar(45) NOT NULL,
                              `id_institution` varchar(45) DEFAULT NULL,
                              `department_name` varchar(100) DEFAULT NULL,
                              `description` longtext,
                              `status` varchar(45) DEFAULT NULL,
                              `last_update` datetime DEFAULT NULL,
                              `user_update` varchar(45) DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `department_head` (
                                   `id` varchar(45) NOT NULL,
                                   `id_department` varchar(45) DEFAULT NULL,
                                   `id_employee` varchar(45) DEFAULT NULL,
                                   `period_start` date DEFAULT NULL,
                                   `period_end` date DEFAULT NULL,
                                   `active_status` varchar(45) DEFAULT NULL,
                                   `status` varchar(45) DEFAULT NULL,
                                   `last_update` datetime DEFAULT NULL,
                                   `user_update` varchar(45) DEFAULT NULL,
                                   `signature` varchar(45) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `employee_institution` (
                                        `id` varchar(45) NOT NULL,
                                        `id_employee` varchar(45) DEFAULT NULL,
                                        `id_institution` varchar(45) DEFAULT NULL,
                                        `status` varchar(45) DEFAULT NULL,
                                        `last_update` datetime DEFAULT NULL,
                                        `user_update` varchar(45) DEFAULT NULL,
                                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `employee_job_position` (
                                         `id` varchar(45) NOT NULL,
                                         `id_employee_staff` varchar(45) DEFAULT NULL,
                                         `id_job_position` varchar(45) DEFAULT NULL,
                                         `date_start` date DEFAULT NULL,
                                         `date_end` date DEFAULT NULL,
                                         `active_status` varchar(45) DEFAULT NULL,
                                         `status` varchar(45) DEFAULT NULL,
                                         `last_update` datetime DEFAULT NULL,
                                         `user_update` varchar(45) DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `employee_lecturer` (
                                     `id` varchar(45) NOT NULL,
                                     `id_employee` varchar(45) DEFAULT NULL,
                                     `id_prodi_home_base` varchar(45) DEFAULT NULL,
                                     `nidn` varchar(45) DEFAULT NULL,
                                     `kartu_nidn` longtext,
                                     `no_certificate_lecturer` varchar(45) DEFAULT NULL,
                                     `certificate_lecturer` longtext,
                                     `position_structural` varchar(45) DEFAULT NULL,
                                     `sk_position_structural` longtext,
                                     `position_functioanl` varchar(45) DEFAULT NULL,
                                     `sk_position_fungtional` longtext,
                                     `status` varchar(45) DEFAULT NULL,
                                     `last_update` datetime DEFAULT NULL,
                                     `user_update` varchar(45) DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `employee_staff` (
                                  `id` varchar(45) NOT NULL,
                                  `id_employee` varchar(45) DEFAULT NULL,
                                  `date_start` varchar(45) DEFAULT NULL,
                                  `status` varchar(45) DEFAULT NULL,
                                  `last_update` datetime DEFAULT NULL,
                                  `user_update` varchar(45) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `employes` (
                            `id` varchar(45) NOT NULL,
                            `number` varchar(100) DEFAULT NULL,
                            `full_name` varchar(45) DEFAULT NULL,
                            `nick_name` varchar(45) DEFAULT NULL,
                            `front_title` varchar(45) DEFAULT NULL,
                            `back_title` varchar(45) DEFAULT NULL,
                            `gender` varchar(45) DEFAULT NULL,
                            `status` varchar(45) DEFAULT NULL,
                            `last_update` datetime DEFAULT NULL,
                            `user_update` varchar(45) DEFAULT NULL,
                            `email` varchar(100) DEFAULT NULL,
                            `id_user` varchar(45) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `foudation` (
                             `id` varchar(45) NOT NULL,
                             `foundation_name` varchar(45) DEFAULT NULL,
                             `description` longtext,
                             `status` varchar(45) DEFAULT NULL,
                             `last_update` datetime DEFAULT NULL,
                             `user_update` varchar(45) DEFAULT NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `institution` (
                               `id` varchar(45) NOT NULL,
                               `institution_name` varchar(100) DEFAULT NULL,
                               `description` longtext,
                               `status` varchar(45) DEFAULT NULL,
                               `last_update` datetime DEFAULT NULL,
                               `user_update` varchar(45) DEFAULT NULL,
                               `id_foundation` varchar(45) DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `institution_faculty` (
                                       `id` varchar(45) NOT NULL,
                                       `id_institution` varchar(45) DEFAULT NULL,
                                       `id_department` varchar(45) DEFAULT NULL,
                                       `faculty_name` varchar(45) DEFAULT NULL,
                                       `description` longtext,
                                       `active_status` varchar(45) DEFAULT NULL,
                                       `status` varchar(45) DEFAULT NULL,
                                       `last_update` datetime DEFAULT NULL,
                                       `user_update` varchar(45) DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `institution_prodi` (
                                     `id` varchar(45) NOT NULL,
                                     `id_faculty` varchar(45) DEFAULT NULL,
                                     `id_department` varchar(45) DEFAULT NULL,
                                     `prodi_name` varchar(45) DEFAULT NULL,
                                     `description` longtext,
                                     `status` varchar(45) DEFAULT NULL,
                                     `last_update` datetime DEFAULT NULL,
                                     `date_update` varchar(45) DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `institution_program` (
                                       `id` varchar(45) NOT NULL,
                                       `id_prodi` varchar(45) DEFAULT NULL,
                                       `program_code` varchar(45) DEFAULT NULL,
                                       `program_name` varchar(255) DEFAULT NULL,
                                       `description` varchar(45) DEFAULT NULL,
                                       `status` varchar(45) DEFAULT NULL,
                                       `last_update` datetime DEFAULT NULL,
                                       `user_update` varchar(45) DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `job_position` (
                                `id` varchar(45) NOT NULL,
                                `id_department` varchar(45) DEFAULT NULL,
                                `position_name` varchar(100) DEFAULT NULL,
                                `description` longtext,
                                `active_status` varchar(45) DEFAULT NULL,
                                `status` varchar(45) DEFAULT NULL,
                                `last_update` datetime DEFAULT NULL,
                                `user_update` varchar(45) DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `payment_plan_type` (
                                     `id` varchar(45) NOT NULL,
                                     `plan_type` varchar(255) DEFAULT NULL,
                                     `installment` int DEFAULT NULL,
                                     `description` longtext,
                                     `status` varchar(45) DEFAULT NULL,
                                     `last_update` datetime DEFAULT NULL,
                                     `date_update` varchar(45) DEFAULT NULL,
                                     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `period_academic` (
                                   `id` varchar(45) NOT NULL,
                                   `period_code` varchar(45) DEFAULT NULL,
                                   `period_name` varchar(45) DEFAULT NULL,
                                   `period_start` date DEFAULT NULL,
                                   `period_end` date DEFAULT NULL,
                                   `status` varchar(45) DEFAULT NULL,
                                   `last_update` datetime DEFAULT NULL,
                                   `user_update` varchar(45) DEFAULT NULL,
                                   `id_institution` varchar(45) DEFAULT NULL,
                                   `active_status` varchar(45) DEFAULT NULL,
                                   `preparation_start` date DEFAULT NULL,
                                   `type` varchar(45) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `period_academic_finance` (
                                           `id` varchar(45) NOT NULL,
                                           `id_period_academic` varchar(45) DEFAULT NULL,
                                           `id_prodi` varchar(45) DEFAULT NULL,
                                           `id_program` varchar(45) DEFAULT NULL,
                                           `start_date` date DEFAULT NULL,
                                           `end_date` date DEFAULT NULL,
                                           `status` varchar(45) DEFAULT NULL,
                                           `last_update` datetime DEFAULT NULL,
                                           `user_update` varchar(45) DEFAULT NULL,
                                           `pereparation_start` date DEFAULT NULL,
                                           `preparation_end` date DEFAULT NULL,
                                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `period_academic_prodi` (
                                         `id` varchar(45) NOT NULL,
                                         `id_prodi` varchar(45) DEFAULT NULL,
                                         `id_period_academic` varchar(45) DEFAULT NULL,
                                         `id_program` varchar(45) DEFAULT NULL,
                                         `ploting_start` date DEFAULT NULL,
                                         `ploting_end` date DEFAULT NULL,
                                         `scheduling_start` date DEFAULT NULL,
                                         `scheduling_end` date DEFAULT NULL,
                                         `krs_start` date DEFAULT NULL,
                                         `krs_end` date DEFAULT NULL,
                                         `final_date` date DEFAULT NULL,
                                         `status` varchar(45) DEFAULT NULL,
                                         `last_update` datetime DEFAULT NULL,
                                         `user_update` varchar(45) DEFAULT NULL,
                                         PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ploting` (
                           `id` varchar(45) NOT NULL,
                           `id_period_academic` varchar(45) DEFAULT NULL,
                           `id_period_academic_prodi` varchar(45) DEFAULT NULL,
                           `id_prodi` varchar(45) DEFAULT NULL,
                           `id_class` varchar(45) DEFAULT NULL,
                           `id_curiculum_subject` varchar(45) DEFAULT NULL,
                           `id_subject` varchar(45) DEFAULT NULL,
                           `status` varchar(45) DEFAULT NULL,
                           `last_update` datetime DEFAULT NULL,
                           `user_update` varchar(45) DEFAULT NULL,
                           `id_curiculum` varchar(45) DEFAULT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ploting_lecturer` (
                                    `id` varchar(45) NOT NULL,
                                    `id_ploting` varchar(45) DEFAULT NULL,
                                    `lecturer_status` varchar(45) DEFAULT NULL,
                                    `id_lecturer` varchar(45) DEFAULT NULL,
                                    `status` varchar(45) DEFAULT NULL,
                                    `last_update` datetime DEFAULT NULL,
                                    `user_update` varchar(45) DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `re_registration` (
                                   `id` varchar(45) NOT NULL,
                                   `id_period_academic` varchar(45) DEFAULT NULL,
                                   `id_period_academic_finance` varchar(45) DEFAULT NULL,
                                   `id_period_academic_prodi` varchar(45) DEFAULT NULL,
                                   `id_employee` varchar(45) DEFAULT NULL,
                                   `request_date` datetime DEFAULT NULL,
                                   `type` varchar(45) DEFAULT NULL,
                                   `status_approve` varchar(45) DEFAULT NULL,
                                   `status` varchar(45) DEFAULT NULL,
                                   `last_update` datetime DEFAULT NULL,
                                   `user_update` varchar(45) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `s_permission` (
                                `id` varchar(45) NOT NULL,
                                `permission_label` varchar(45) DEFAULT NULL,
                                `permission_value` varchar(45) DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `s_role` (
                          `id` varchar(45) NOT NULL,
                          `description` varchar(45) DEFAULT NULL,
                          `name` varchar(45) DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `s_role_permission` (
                                     `id_role` varchar(45) NOT NULL,
                                     `id_permission` varchar(45) DEFAULT NULL,
                                     PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student` (
                           `id` varchar(45) NOT NULL,
                           `id_prodi` varchar(45) DEFAULT NULL,
                           `id_program` varchar(45) DEFAULT NULL,
                           `student_number` varchar(45) DEFAULT NULL,
                           `student_name` varchar(255) DEFAULT NULL,
                           `gender` varchar(45) DEFAULT NULL,
                           `email` varchar(45) DEFAULT NULL,
                           `generation` varchar(45) DEFAULT NULL,
                           `status` varchar(45) DEFAULT NULL,
                           `last_update` datetime DEFAULT NULL,
                           `user_update` varchar(45) DEFAULT NULL,
                           `id_user` varchar(45) DEFAULT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_bill` (
                                `id` varchar(45) NOT NULL,
                                `id_re_registration` varchar(45) DEFAULT NULL,
                                `id_period_academic` varchar(45) DEFAULT NULL,
                                `id_period_academic_finance` varchar(45) DEFAULT NULL,
                                `id_period_academic_prodi` varchar(45) DEFAULT NULL,
                                `id_student` varchar(45) DEFAULT NULL,
                                `total_value` decimal(10,0) DEFAULT NULL,
                                `bill_status` varchar(45) DEFAULT NULL,
                                `status` varchar(45) DEFAULT NULL,
                                `last_update` datetime DEFAULT NULL,
                                `date_update` varchar(45) DEFAULT NULL,
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_bill_detail` (
                                       `id` varchar(45) NOT NULL,
                                       `id_student_bill` varchar(45) DEFAULT NULL,
                                       `id_bill_value` varchar(45) DEFAULT NULL,
                                       `value` decimal(10,0) DEFAULT NULL,
                                       `status` varchar(45) DEFAULT NULL,
                                       `last_update` datetime DEFAULT NULL,
                                       `user_update` varchar(45) DEFAULT NULL,
                                       `type` varchar(45) DEFAULT NULL,
                                       `id_student_bill_before` varchar(45) DEFAULT NULL,
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_invoice` (
                                   `id` varchar(45) NOT NULL,
                                   `id_student_payment_plan` varchar(45) DEFAULT NULL,
                                   `id_periode_academic` varchar(45) DEFAULT NULL,
                                   `id_periode_academic_finance` varchar(45) DEFAULT NULL,
                                   `installment_number` int DEFAULT NULL,
                                   `nominal` decimal(19,2) DEFAULT NULL,
                                   `send_status` varchar(45) DEFAULT NULL,
                                   `plan_date` date DEFAULT NULL,
                                   `due_date` date DEFAULT NULL,
                                   `status` varchar(45) DEFAULT NULL,
                                   `id_student` varchar(45) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_krs` (
                               `id` varchar(45) NOT NULL,
                               `id_student_status_academic` varchar(45) DEFAULT NULL,
                               `id_period_academic` varchar(45) DEFAULT NULL,
                               `id_period_academic_finance` varchar(45) DEFAULT NULL,
                               `id_period_academic_prodi` varchar(45) DEFAULT NULL,
                               `id_studies_schedule` varchar(45) DEFAULT NULL,
                               `id_curiculum_subject` varchar(45) DEFAULT NULL,
                               `id_subject` varchar(45) DEFAULT NULL,
                               `subject_code` varchar(45) DEFAULT NULL,
                               `subject_name` varchar(255) DEFAULT NULL,
                               `subject_name_english` varchar(255) DEFAULT NULL,
                               `sks` decimal(19,2) DEFAULT NULL,
                               `status` varchar(45) DEFAULT NULL,
                               `last_update` datetime DEFAULT NULL,
                               `user_update` varchar(45) DEFAULT NULL,
                               `final_score` decimal(19,2) DEFAULT NULL,
                               `grade` varchar(1) DEFAULT NULL,
                               `weight` decimal(19,2) DEFAULT NULL,
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_payment` (
                                   `id` varchar(45) NOT NULL,
                                   `id_student_invoice` varchar(45) DEFAULT NULL,
                                   `id_bank` varchar(45) DEFAULT NULL,
                                   `virtual_account_number` varchar(50) DEFAULT NULL,
                                   `amount` decimal(19,2) DEFAULT NULL,
                                   `payment_date` datetime DEFAULT NULL,
                                   `reference` varchar(255) DEFAULT NULL,
                                   `status` varchar(45) DEFAULT NULL,
                                   `amount_real` decimal(19,0) DEFAULT NULL,
                                   `id_student` varchar(45) DEFAULT NULL,
                                   `id_period_academic` varchar(45) DEFAULT NULL,
                                   `id_period_academic_finance` varchar(45) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_payment_plan` (
                                        `id` varchar(45) NOT NULL,
                                        `id_student_bill` varchar(45) DEFAULT NULL,
                                        `payment_plan_type_id` varchar(45) DEFAULT NULL,
                                        `status_approve` varchar(45) DEFAULT NULL,
                                        `date_approve` datetime DEFAULT NULL,
                                        `description` longtext,
                                        `status` varchar(45) DEFAULT NULL,
                                        `last_update` datetime DEFAULT NULL,
                                        `user_update` varchar(45) DEFAULT NULL,
                                        `id_periode_academic` varchar(45) DEFAULT NULL,
                                        `id_periode_academic_finance` varchar(45) DEFAULT NULL,
                                        `id_student` varchar(45) DEFAULT NULL,
                                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_status` (
                                  `id` varchar(45) NOT NULL,
                                  `code` varchar(45) DEFAULT NULL,
                                  `name` varchar(150) DEFAULT NULL,
                                  `status` varchar(45) DEFAULT NULL,
                                  `last_update` datetime DEFAULT NULL,
                                  `user_update` varchar(45) DEFAULT NULL,
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_status_academic` (
                                           `id` varchar(45) NOT NULL,
                                           `id_re_registration` varchar(45) DEFAULT NULL,
                                           `id_period_academic` varchar(45) DEFAULT NULL,
                                           `id_period_academic_prodi` varchar(45) DEFAULT NULL,
                                           `student_status` varchar(45) DEFAULT NULL,
                                           `id_student` varchar(45) DEFAULT NULL,
                                           `sks_total_before` decimal(19,2) DEFAULT NULL,
                                           `current_sks_total` decimal(19,2) DEFAULT NULL,
                                           `current_ip` decimal(19,2) DEFAULT NULL,
                                           `ipk` decimal(19,2) DEFAULT NULL,
                                           `semester` int DEFAULT NULL,
                                           `status` varchar(45) DEFAULT NULL,
                                           `last_update` datetime DEFAULT NULL,
                                           `user_update` varchar(45) DEFAULT NULL,
                                           `banned_status` varchar(45) DEFAULT NULL,
                                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `student_virtual_account` (
                                           `id` varchar(45) NOT NULL,
                                           `id_student_invoice` varchar(45) DEFAULT NULL,
                                           `id_student` varchar(45) DEFAULT NULL,
                                           `id_bank` varchar(45) DEFAULT NULL,
                                           `virtual_account_number` varchar(50) DEFAULT NULL,
                                           `status` varchar(45) DEFAULT NULL,
                                           `last_update` varchar(45) DEFAULT NULL,
                                           `user_update` varchar(45) DEFAULT NULL,
                                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_attendance_lecturer` (
                                               `id` varchar(45) NOT NULL,
                                               `id_studies_session` varchar(45) DEFAULT NULL,
                                               `id_period_academic_prodi` varchar(45) DEFAULT NULL,
                                               `id_period_academic` varchar(45) DEFAULT NULL,
                                               `id_lecturer` varchar(45) DEFAULT NULL,
                                               `attendance_in` datetime DEFAULT NULL,
                                               `attendance_out` datetime DEFAULT NULL,
                                               `report_description` varchar(45) DEFAULT NULL,
                                               `conformity_with_rps` tinyint DEFAULT NULL,
                                               `status` varchar(45) DEFAULT NULL,
                                               `last_update` datetime DEFAULT NULL,
                                               `user_update` varchar(45) DEFAULT NULL,
                                               `attendance_status` varchar(45) DEFAULT NULL,
                                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_attendance_student` (
                                              `id` varchar(45) NOT NULL,
                                              `id_studies_session` varchar(45) DEFAULT NULL,
                                              `id_studies_attendance_lecturer` varchar(45) DEFAULT NULL,
                                              `id_studies_schedule` varchar(45) DEFAULT NULL,
                                              `attendance_in` datetime DEFAULT NULL,
                                              `attendance_out` datetime DEFAULT NULL,
                                              `attendance_status` varchar(45) DEFAULT NULL,
                                              `description` longtext,
                                              `status` varchar(45) DEFAULT NULL,
                                              `last_update` datetime DEFAULT NULL,
                                              `date_update` varchar(45) DEFAULT NULL,
                                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule` (
                                    `id` varchar(45) NOT NULL,
                                    `id_ploting` varchar(45) DEFAULT NULL,
                                    `id_period_academic` varchar(45) DEFAULT NULL,
                                    `id_period_academic_prodi` varchar(45) DEFAULT NULL,
                                    `id_prodi` varchar(45) DEFAULT NULL,
                                    `id_curiculum` varchar(45) DEFAULT NULL,
                                    `id_curiculum_subject` varchar(45) DEFAULT NULL,
                                    `id_subject` varchar(45) DEFAULT NULL,
                                    `subject_code` varchar(45) DEFAULT NULL,
                                    `subject_name` varchar(255) DEFAULT NULL,
                                    `subject_name_english` varchar(255) DEFAULT NULL,
                                    `sks` decimal(19,2) DEFAULT NULL,
                                    `status` varchar(45) DEFAULT NULL,
                                    `last_update` datetime DEFAULT NULL,
                                    `user_update` varchar(45) DEFAULT NULL,
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule_lecturer` (
                                             `id` varchar(45) NOT NULL,
                                             `id_studies_schedule` varchar(45) DEFAULT NULL,
                                             `id_lecturer` varchar(45) DEFAULT NULL,
                                             `lecturer_name` varchar(255) DEFAULT NULL,
                                             `lecturer_status` varchar(45) DEFAULT NULL,
                                             `status` varchar(45) DEFAULT NULL,
                                             `last_update` datetime DEFAULT NULL,
                                             `user_update` varchar(45) DEFAULT NULL,
                                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule_session` (
                                            `id` varchar(45) NOT NULL,
                                            `id_studies_schedule` varchar(45) DEFAULT NULL,
                                            `id_building_room` varchar(45) DEFAULT NULL,
                                            `id_day` varchar(45) DEFAULT NULL,
                                            `session_start` time DEFAULT NULL,
                                            `session_end` time DEFAULT NULL,
                                            `session_id` varchar(45) DEFAULT NULL,
                                            `session_number` int DEFAULT NULL,
                                            `status` varchar(45) DEFAULT NULL,
                                            `last_update` datetime DEFAULT NULL,
                                            `user_update` varchar(45) DEFAULT NULL,
                                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule_tasks` (
                                          `id` varchar(45) NOT NULL,
                                          `id_studies_schedule` varchar(45) DEFAULT NULL,
                                          `id_period_academic` varchar(45) DEFAULT NULL,
                                          `tasks_name` varchar(45) DEFAULT NULL,
                                          `tasks_description` longtext,
                                          `allow_from` datetime DEFAULT NULL,
                                          `due_date` datetime DEFAULT NULL,
                                          `remain_me_to_grade_by` datetime DEFAULT NULL,
                                          `maximum_upload_file` int DEFAULT NULL,
                                          `percentase_grade` decimal(19,2) DEFAULT NULL,
                                          `status` varchar(45) DEFAULT NULL,
                                          `last_update` datetime DEFAULT NULL,
                                          `user_update` varchar(45) DEFAULT NULL,
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule_tasks_answer` (
                                                 `id` varchar(45) NOT NULL,
                                                 `id_schedule_tasks` varchar(45) DEFAULT NULL,
                                                 `id_krs` varchar(45) DEFAULT NULL,
                                                 `id_period_academic` varchar(45) DEFAULT NULL,
                                                 `id_student` varchar(45) DEFAULT NULL,
                                                 `description` longtext,
                                                 `date_answer` datetime DEFAULT NULL,
                                                 `answer_status` varchar(45) DEFAULT NULL,
                                                 `score` decimal(19,2) DEFAULT NULL,
                                                 `score_status` varchar(45) DEFAULT NULL,
                                                 `status` varchar(45) DEFAULT NULL,
                                                 `last_update` datetime DEFAULT NULL,
                                                 `user_update` varchar(45) DEFAULT NULL,
                                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule_tasks_answer_evidance` (
                                                          `id` varchar(45) NOT NULL,
                                                          `id_studies_schedule_tasks` varchar(45) DEFAULT NULL,
                                                          `id_studies_schedule_tasks_answer` varchar(45) DEFAULT NULL,
                                                          `id_period_academic` varchar(45) DEFAULT NULL,
                                                          `file_name` longtext,
                                                          `file` longtext,
                                                          `status` varchar(45) DEFAULT NULL,
                                                          `last_update` datetime DEFAULT NULL,
                                                          `user_update` varchar(45) DEFAULT NULL,
                                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule_tasks_files` (
                                                `id` varchar(45) NOT NULL,
                                                `id_studies_schedule_tasks` varchar(45) DEFAULT NULL,
                                                `id_period_academic` varchar(45) DEFAULT NULL,
                                                `file_name` longtext,
                                                `file` longtext,
                                                `status` varchar(45) DEFAULT NULL,
                                                `last_update` datetime DEFAULT NULL,
                                                `user_update` varchar(45) DEFAULT NULL,
                                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_schedule_tasks_file_type` (
                                                    `id` varchar(45) NOT NULL,
                                                    `id_studies_schedule_tasks` varchar(45) DEFAULT NULL,
                                                    `id_period_academic` varchar(45) DEFAULT NULL,
                                                    `file_type` varchar(45) DEFAULT NULL,
                                                    `status` varchar(45) DEFAULT NULL,
                                                    `last_update` datetime DEFAULT NULL,
                                                    `date_update` varchar(45) DEFAULT NULL,
                                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `studies_session` (
                                   `id` varchar(45) NOT NULL,
                                   `id_studies_schedule` varchar(45) DEFAULT NULL,
                                   `id_studies_schedule_session` varchar(45) DEFAULT NULL,
                                   `session_start` datetime DEFAULT NULL,
                                   `session_end` datetime DEFAULT NULL,
                                   `session_id` varchar(45) DEFAULT NULL,
                                   `session_status` varchar(45) DEFAULT NULL,
                                   `session_type` varchar(45) DEFAULT NULL,
                                   `status` varchar(45) DEFAULT NULL,
                                   `last_update` datetime DEFAULT NULL,
                                   `user_update` varchar(45) DEFAULT NULL,
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `subject` (
                           `id` varchar(45) NOT NULL,
                           `id_prodi` varchar(45) DEFAULT NULL,
                           `code` varchar(45) DEFAULT NULL,
                           `subject_name` varchar(155) DEFAULT NULL,
                           `subject_name_english` varchar(155) DEFAULT NULL,
                           `abbreviation` varchar(100) DEFAULT NULL,
                           `status` varchar(45) DEFAULT NULL,
                           `last_update` datetime DEFAULT NULL,
                           `user_update` varchar(45) DEFAULT NULL,
                           PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `s_user` (
                          `id` varchar(45) NOT NULL,
                          `user_name` varchar(100) DEFAULT NULL,
                          `active` bit(1) DEFAULT NULL,
                          `user` varchar(100) DEFAULT NULL,
                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `s_user_role` (
                               `id_user` varchar(45) NOT NULL,
                               `id_role` varchar(45) DEFAULT NULL,
                               `status` varchar(45) DEFAULT NULL,
                               PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `user_log` (
                            `id` varchar(45) NOT NULL,
                            `date_log` datetime DEFAULT NULL,
                            `user_log` varchar(100) DEFAULT NULL,
                            `type` varchar(45) DEFAULT NULL,
                            `description` longtext,
                            `tabel` varchar(100) DEFAULT NULL,
                            `old_value` longtext,
                            `new_value` longtext,
                            `status` varchar(45) DEFAULT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


