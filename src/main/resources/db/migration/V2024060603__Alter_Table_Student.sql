ALTER TABLE student
    ADD address varchar(255),
    ADD photo varchar(255),
    ADD date_of_birth DATE;
