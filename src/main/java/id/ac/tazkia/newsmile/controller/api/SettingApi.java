package id.ac.tazkia.newsmile.controller.api;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.CampusDao;
import id.ac.tazkia.newsmile.dao.FoundationDao;
import id.ac.tazkia.newsmile.dao.SubjectDao;
import id.ac.tazkia.newsmile.dao.building.BuildingDao;
import id.ac.tazkia.newsmile.dao.building.BuildingFloorDao;
import id.ac.tazkia.newsmile.dao.building.BuildingRoomDao;
import id.ac.tazkia.newsmile.dao.curriculum.CuriculumDao;
import id.ac.tazkia.newsmile.dao.department.DepartmentDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionConcentrationDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionFacultyDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionProdiDao;
import id.ac.tazkia.newsmile.dto.BaseResponseDto;
import id.ac.tazkia.newsmile.entity.Campus;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.Subject;
import id.ac.tazkia.newsmile.entity.building.Building;
import id.ac.tazkia.newsmile.entity.building.BuildingFloor;
import id.ac.tazkia.newsmile.entity.building.BuildingRoom;
import id.ac.tazkia.newsmile.entity.curriculum.Curiculum;
import id.ac.tazkia.newsmile.entity.department.Department;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import id.ac.tazkia.newsmile.entity.institution.InstitutionConcentration;
import id.ac.tazkia.newsmile.entity.institution.InstitutionFaculty;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@Slf4j
@RequestMapping("/setting")
public class SettingApi {
    @Autowired
    private FoundationDao foundationDao;
    @Autowired
    private CampusDao campusDao;
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private BuildingDao buildingDao;
    @Autowired
    private BuildingFloorDao buildingFloorDao;
    @Autowired
    private BuildingRoomDao buildingRoomDao;
    @Autowired
    private InstitutionFacultyDao institutionFacultyDao;
    @Autowired
    private InstitutionProdiDao institutionProdiDao;
    @Autowired
    private CuriculumDao curiculumDao;
    @Autowired
    private InstitutionConcentrationDao institutionConcentrationDao;
    @Autowired
    private SubjectDao subjectDao;

    @PutMapping("/foundation/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editFoundationStatus(@PathVariable String id, @RequestBody Map<String, Object> requestData , BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<Foundation> foundationOptional = foundationDao.findById(id);
            if (foundationOptional.isPresent()){
                Foundation foundation = foundationOptional.get();
                foundation.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                foundationDao.save(foundation);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            }else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Institution not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - Foundation] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PostMapping("/institution/update-status/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editInstitutionStatus(@PathVariable String id,
                                                                 @RequestBody Map<String, Object> requestData , BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<Institution> optionalInstitution = institutionDao.findById(id);
            if (optionalInstitution.isPresent()){
                Institution institution = optionalInstitution.get();
                institution.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                institutionDao.save(institution);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            }else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Institution not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - Campus] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PostMapping("/campus/update-status/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editCampusStatus(@PathVariable String id,
                                                            @RequestBody Map<String, Object> requestData, BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<Campus> optionalCampus = campusDao.findById(id);
            if (optionalCampus.isPresent()) {
                Campus campus = optionalCampus.get();
                campus.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                campusDao.save(campus);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Campus not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - Campus] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PutMapping("/department/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editDepartmentStatus(@PathVariable String id,
                                                                @RequestBody Map<String, Object> requestData, BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<Department> optionalDepartment = departmentDao.findById(id);
            if (optionalDepartment.isPresent()) {
                Department department = optionalDepartment.get();
                department.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                departmentDao.save(department);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Departmnet not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - Department] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PutMapping("/concentration/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editConcentrationStatus(@PathVariable String id,
                                                                @RequestBody Map<String, Object> requestData, BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<InstitutionConcentration> optionalInstitutionConcentration = institutionConcentrationDao.findById(id);
            if (optionalInstitutionConcentration.isPresent()) {
                InstitutionConcentration institutionConcentration = optionalInstitutionConcentration.get();
                institutionConcentration.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                institutionConcentrationDao.save(institutionConcentration);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("InstitutionConcentration not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - InstitutionConcentration] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PutMapping("/building/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editBuildingStatus(@PathVariable String id,
                                                                @RequestBody Map<String, Object> requestData, BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<Building> optionalBuilding = buildingDao.findById(id);
            if (optionalBuilding.isPresent()) {
                Building building = optionalBuilding.get();
                building.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                buildingDao.save(building);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Building not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - Building] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PutMapping("/floor/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editFloorStatus(@PathVariable String id,
                                                              @RequestBody Map<String, Object> requestData, BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<BuildingFloor> optionalBuildingFloor = buildingFloorDao.findById(id);
            if (optionalBuildingFloor.isPresent()) {
                BuildingFloor buildingFloor = optionalBuildingFloor.get();
                buildingFloor.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                buildingFloorDao.save(buildingFloor);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("buildingFloor not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - buildingFloor] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PutMapping("/room/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editRoomStatus(@PathVariable String id,
                                                           @RequestBody Map<String, Object> requestData, BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<BuildingRoom> optionalBuildingRoom = buildingRoomDao.findById(id);
            if (optionalBuildingRoom.isPresent()) {
                BuildingRoom BuildingRoom = optionalBuildingRoom.get();
                BuildingRoom.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                buildingRoomDao.save(BuildingRoom);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("BuildingRoom not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - BuildingRoom] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @PutMapping("/faculty/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> editFacultyStatus(@PathVariable String id,
                                                          @RequestBody Map<String, Object> requestData, BindingResult bindingResult){
        try {
            if (bindingResult.hasErrors()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new BaseResponseDto("ERR01",
                        "Invalid Request Body, " + bindingResult.getFieldError().getDefaultMessage()));
            }

            Optional<InstitutionFaculty> optionalInstitutionFaculty = institutionFacultyDao.findById(id);
            if (optionalInstitutionFaculty.isPresent()) {
                InstitutionFaculty faculty = optionalInstitutionFaculty.get();
                faculty.setStatus("ACTIVE".equals(requestData.get("status")) ? StatusRecord.ACTIVE : StatusRecord.INACTIVE);
                institutionFacultyDao.save(faculty);
                return ResponseEntity.status(HttpStatus.OK).body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("success")
                                .build());
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("InstitutionFaculty not found")
                                .build());
            }
        }catch (Exception e){
            log.error("[UPDATE - InstitutionFaculty] - [ERROR] : Unable to save data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/get-department/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getDepartment(@PathVariable String id) {
        try {
            Optional<Institution> optionalInstitution = institutionDao.findById(id);
            if (optionalInstitution.isPresent()){
                List<Department> departments = departmentDao.findByStatusAndInstitutionOrderByDepartmentNameAsc(StatusRecord.ACTIVE, optionalInstitution.get());
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(departments)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Institution Id Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET DEPARTMENTT] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/get-curriculum/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getCurriculumProdi(@PathVariable String id) {
        try {
            Optional<InstitutionProdi> optionalInstitutionProdi = institutionProdiDao.findById(id);
            if (optionalInstitutionProdi.isPresent()){
                List<Curiculum> curiculums = curiculumDao.findByStatusAndInstitutionProdiOrderByCuriculumNameAsc(StatusRecord.ACTIVE, optionalInstitutionProdi.get());
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(curiculums)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Curriculum Id Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Curriculum] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/get-concentration/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getConcentrationProdi(@PathVariable String id) {
        try {
            Optional<InstitutionProdi> optionalInstitutionProdi = institutionProdiDao.findById(id);
            if (optionalInstitutionProdi.isPresent()){
                List<InstitutionConcentration> concentrations = institutionConcentrationDao.findByStatusAndInstitutionProdiOrderByConcentrationNameAsc(StatusRecord.ACTIVE, optionalInstitutionProdi.get());
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(concentrations)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Concentration Id Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Curriculum] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/get-prodi")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getProdi(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession();
            List<InstitutionProdi> institutionProdis =  SessionHelper.getProdiList(session,institutionProdiDao);
            if (!institutionProdis.isEmpty()){
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(institutionProdis)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Curriculum Id Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Curriculum] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/get-subjects")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getSubjects(@RequestParam(required = false)String search, @RequestParam String prodiId,Pageable pageable) {
        try {
            Optional<InstitutionProdi> optionalInstitutionProdi = institutionProdiDao.findById(prodiId);
            if (optionalInstitutionProdi.isPresent()){
                Page<Subject> subjects = subjectDao.pageSearchProdi(Arrays.asList(optionalInstitutionProdi.get().getId()), search,pageable);
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(subjects)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Prodi Id Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Curriculum] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }
}
