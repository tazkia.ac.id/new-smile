package id.ac.tazkia.newsmile.controller.web.employee;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.employee.EmployeeInstitutionDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dto.employee.RequestEmployee;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.employee.EmployeeInstitution;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.service.EmployeeService;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Controller
public class EmployeeController {
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private EmployeeInstitutionDao employeeInstitutionDao;
    @Autowired
    EmployeeService employeeService;

    @GetMapping("/employee")
    public String curriculumPage(@RequestParam(required = false) String trash , Model model, BaseRequestParamDto params, HttpServletRequest request,
                                 @ActiveMenu(link = "employee", dropdown = "") String activeMenu) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        Employes employes = employesDao.findByUser(user);
        List<Institution> institutionList = new ArrayList<>();
        List<EmployeeInstitution> employeeInstitutions = employeeInstitutionDao.findByEmployesAndStatusOrderByInstitutionInstitutionNameAsc(employes, StatusRecord.ACTIVE);
        if (employeeInstitutions.isEmpty()){
            institutionList.addAll(SessionHelper.getInstitutionList(session,institutionDao));
            model.addAttribute("institutionList", institutionList);
        }else {
            institutionList.addAll(employeeInstitutions.stream()
                    .map(EmployeeInstitution::getInstitution)
                    .collect(Collectors.toList()));
            model.addAttribute("institutionList", institutionList);
        }
        List<String> institutionIds = institutionList.stream()
                .map(inst -> inst.getId())
                .collect(Collectors.toList());

        if (StringUtils.hasText(trash)){
            model.addAttribute("trash", true);
        }

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                institutionIds,
                trash,
                (search, ins, tr) -> employesDao.pageSearchInstitutionDeleted(institutionIds,search,pageable),
                (search, ins) -> employesDao.pageSearchInstitution(institutionIds,search,pageable),
                search -> employesDao.pageSearchFoundation(foundation.getId(),search, pageable),
                (search, tr) -> employesDao.pageSearchFoundationDeleted(foundation.getId(),search, pageable),
                page -> employesDao.findAll(page),
                "employeeList",
                "employee/index",
                "employee/index",
                "employee/index",
                "employee/index"
        );

    }

    @PostMapping("/employee")
    @Transactional
    public String saveEmployee(@Valid RequestEmployee request, RedirectAttributes attributes){
        employeeService.createEmployee(request);
        attributes.addFlashAttribute("success", true);
        return "redirect:employee";
    }

    @PostMapping("/delete-employee")
    @Transactional
    public String deleteEmployee(@RequestParam Employes employes){
        employesDao.delete(employes);
        employeeInstitutionDao.setStatusEmployee(employes.getId(), StatusRecord.INACTIVE.name(), StatusRecord.ACTIVE.name());
        return "redirect:employee";
    }

    @PostMapping("/restore-employee")
    @Transactional
    public String restoreEmployee(@RequestParam Employes employes){
        employes.setStatus(StatusRecord.ACTIVE);
        employesDao.save(employes);
        employeeInstitutionDao.setStatusEmployee(employes.getId(), StatusRecord.ACTIVE.name(), StatusRecord.INACTIVE.name());
        return "redirect:employee";
    }

}
