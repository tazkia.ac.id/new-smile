package id.ac.tazkia.newsmile.controller.api;

import id.ac.tazkia.newsmile.dao.EventsDao;
import id.ac.tazkia.newsmile.dto.BaseResponseDto;
import id.ac.tazkia.newsmile.entity.Events;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/public")
@Slf4j
public class PublicApi {
    @Autowired
    private EventsDao eventsDao;

    @GetMapping("/get-events")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getEvents(@RequestParam LocalDate start, LocalDate end) {
        try {
            List<Events> events = eventsDao.getEventsCalendar(start, end);
            return ResponseEntity.ok().body(
                    BaseResponseDto.builder()
                            .responseCode("00")
                            .responseMessage("Data Found")
                            .data(events)
                            .build());
        } catch (Exception e) {
            log.error("[GET GRAPHIC IPS] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }
}
