package id.ac.tazkia.newsmile.controller.web;

import id.ac.tazkia.newsmile.dao.User.RoleDao;
import id.ac.tazkia.newsmile.dao.User.UserDao;
import id.ac.tazkia.newsmile.dao.student.StudentDao;
import id.ac.tazkia.newsmile.entity.config.Role;
import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.student.Student;
import id.ac.tazkia.newsmile.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;
import java.util.Set;


@Controller
@Slf4j
public class AuthController {
    @Autowired
    private UserService userService;
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserDao userDao;

    @GetMapping("/")
    public String indexPage() {
        return "home/index";
    }

    @GetMapping("/onboard/select-role")
    public String selectRolePage(Authentication authentication, Model model) {
        User user = userService.currentUser(authentication);
        Set<Role> userRoles = user.getRoles();

        if (userRoles.size() == 1){
            Role userRole = userRoles.stream().findFirst().orElse(null);
        }

        model.addAttribute("availableRoles", userRoles);
        return "home/select-role";
    }

        @PostMapping("/onboard/setAuthority")
        public String setAuthority(String selectedRole,Authentication authentication, HttpServletRequest request) {
            User user = userService.currentUser(authentication);
            HttpSession session = userService.setAuthority(request,user,selectedRole);
            Role role = (Role) session.getAttribute("authority");
            return "redirect:../"+ role.getAfterUrlRedirect();

        }

}
