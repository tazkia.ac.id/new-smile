package id.ac.tazkia.newsmile.controller.web.employee;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.employee.EmployeeInstitutionDao;
import id.ac.tazkia.newsmile.dao.employee.EmployeeStaffDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.employee.EmployeeStaff;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@Slf4j
public class StaffController {
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private EmployeeInstitutionDao employeeInstitutionDao;
    @Autowired
    private EmployeeStaffDao employeeStaffDao;
    @Autowired
    private InstitutionDao institutionDao;

    @GetMapping("/staff")
        public String staffPage(@RequestParam (required = false)String trash , Model model, @ActiveMenu(link = "staff", dropdown = "") String activeMenu, BaseRequestParamDto params, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        List<String> institutionIds = SessionHelper.getEmployeeInstitutionId(session,institutionDao,employesDao,employeeInstitutionDao);
        if (StringUtils.hasText(trash)){
            model.addAttribute("trash", true);
        }

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                institutionIds,
                trash,
                (search, ins, tr) -> employeeStaffDao.pageSearchInstitutionDeleted(institutionIds,search,pageable),
                (search, ins) -> employeeStaffDao.pageSearchInstitution(institutionIds,search, pageable),
                search -> employeeStaffDao.pageSearchFoundation(foundation.getId(),search, pageable),
                (search, tr) -> employeeStaffDao.pageSearchFoundationDeleted(foundation.getId(),search, pageable),
                page -> employeeStaffDao.findAll(page),
                "staffList",
                "staff/index",
                "staff/index",
                "staff/index",
                "staff/index"
        );
    }

    @PostMapping("/staff")
    public String saveStaff(@Valid EmployeeStaff employeeStaff, RedirectAttributes attributes){
        employeeStaffDao.save(employeeStaff);
        attributes.addFlashAttribute("success", true);

        return "redirect:staff";
    }

    @PostMapping("/delete-staff")
    public String deleteStaff(@RequestParam EmployeeStaff staff){
        employeeStaffDao.delete(staff);
        return "redirect:staff";
    }

    @PostMapping("/restore-staff")
    public String restoreStaff(@RequestParam EmployeeStaff staff){
        staff.setStatus(StatusRecord.ACTIVE);
        employeeStaffDao.save(staff);
        return "redirect:staff";
    }
}
