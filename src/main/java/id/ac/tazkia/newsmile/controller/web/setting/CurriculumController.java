package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.curriculum.CuriculumDao;
import id.ac.tazkia.newsmile.dao.curriculum.CuriculumSubjectDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionProdiDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.dto.filter.CurriculumSubjectFilter;
import id.ac.tazkia.newsmile.entity.curriculum.Curiculum;
import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubject;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.service.FileService;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Controller
@Slf4j
public class CurriculumController {
    @Value("${silabus.folder}")
    private String silabusFolder;
    @Autowired
    private InstitutionProdiDao institutionProdiDao;
    @Autowired
    private CuriculumDao curiculumDao;
    @Autowired
    private CuriculumSubjectDao curiculumSubjectDao;
    @Autowired
    private FileService fileService;

    @GetMapping("/setting/curriculum")
    public String curriculumPage(Model model, @ActiveMenu(link = "curriculum", dropdown = "dropdown-settings", tab = "curriculum") String activeMenu,
                                 BaseRequestParamDto params, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        List<InstitutionProdi> institutionProdis =  SessionHelper.getProdiList(session,institutionProdiDao);
        model.addAttribute("prodiList", institutionProdis);

        return EntityUtil.preparePage(
                model,
                params,
                pageable,
                institutionProdis,
                (search, found) -> curiculumDao.pageSearchProdi(institutionProdis.stream().map(InstitutionProdi::getId).collect(Collectors.toList()),search, pageable),
                search -> curiculumDao.pageSearch(search, pageable),
                page -> curiculumDao.findAll(page),
                "curriculumList",
                "setting/curriculum/index",
                "setting/curriculum/index"
        );
    }

    @PostMapping("/setting/curriculum")
    public String saveCurriculum(@Valid Curiculum curiculum, RedirectAttributes attributes) {
        curiculumDao.save(curiculum);
        attributes.addFlashAttribute("success", true);
        return "redirect:curriculum";
    }

    @GetMapping("/setting/curriculum/subject")
    public String subjectPage(Model model,@ActiveMenu(link = "curriculum", dropdown = "dropdown-settings", tab = "subject") String activeMenu,
                              CurriculumSubjectFilter params, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        List<InstitutionProdi> institutionProdis =  SessionHelper.getProdiList(session,institutionProdiDao);
        List<Integer> allSemesters = IntStream.rangeClosed(1, 9).boxed().collect(Collectors.toList());

        if(!StringUtils.hasText(params.getCurriculum())){
            Curiculum curiculum = curiculumDao.findFirstByStatusAndInstitutionProdiOrderByCuriculumName(StatusRecord.ACTIVE,institutionProdis.get(0));
            params.setCurriculum(curiculum.getId());
        }else {
            Curiculum curiculum = curiculumDao.findById(params.getCurriculum()).get();
            params.setProdi(curiculum.getInstitutionProdi().getId());
        }
        if(!StringUtils.hasText(params.getProdi())){
            InstitutionProdi prodi = institutionProdis.get(0);
            params.setProdi(prodi.getId());
        }

        if(params.getSemester() == null){
            params.setSemester(allSemesters);
        }
        model.addAttribute("selectedProdi", params.getProdi());
        model.addAttribute("selectedCurriculum", params.getCurriculum());
        String selectedSemesterString = params.getSemester().stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));

        List<Integer> selectedSemesterList = Arrays.stream(selectedSemesterString.split(","))
                .map(Integer::valueOf)
                .collect(Collectors.toList());

        model.addAttribute("selectedSemester", selectedSemesterList);
        model.addAttribute("prodiList", institutionProdis);
        model.addAttribute("allSemesters", allSemesters);

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                institutionProdis,
                params.getCurriculum(),
                (search, prod, cr) -> curiculumSubjectDao.pageSearchProdiCurriculum(params.getProdi(),params.getCurriculum(),search,params.getSemester(), pageable),
                (search, prod) -> curiculumSubjectDao.pageSearchProdi(params.getProdi(),search, params.getSemester(),pageable),
                search -> curiculumSubjectDao.pageSearch(search,params.getSemester(), pageable),
                (search, cr) -> curiculumSubjectDao.pageSearchCurriculum(search,params.getCurriculum(),params.getSemester(), pageable),
                page -> curiculumSubjectDao.findAll(page),
                "curriculumSubjectList",
                "setting/curriculum/subject/index",
                "setting/curriculum/subject/index",
                "setting/curriculum/subject/index",
                "setting/curriculum/subject/index"
        );
    }

    @PostMapping("/setting/curriculum/subject")
    public String saveCurriculumSubject(@Valid CuriculumSubject curiculumSubject, MultipartFile file, RedirectAttributes attributes) throws Exception {
        if (!file.isEmpty()) {
            String lokasiUpload = silabusFolder + File.separator + curiculumSubject.getSubject().getSubjectName();
            String fileName = fileService.uploadFile(file, lokasiUpload, "Silabus " + curiculumSubject.getSubject().getCode() + " - " + curiculumSubject.getSubject().getSubjectName());
            curiculumSubject.setSilabus(fileName);
        } else {
            if (!curiculumSubject.getId().isEmpty()){
                CuriculumSubject existingSubject = curiculumSubjectDao.findById(curiculumSubject.getId()).orElseThrow(() -> new IllegalArgumentException("Invalid subject ID"));
                curiculumSubject.setSilabus(existingSubject.getSilabus());
            }
        }

        if ("true".equals(curiculumSubject.getAllowTranskript())) {
            curiculumSubject.setAllowTranskript(true);
        }
        curiculumSubjectDao.save(curiculumSubject);
        attributes.addFlashAttribute("success", true);
        return "redirect:subject";
    }

    @GetMapping("/setting/curriculum/subject/detail")
    public String subjectDetailPage(Model model,@ActiveMenu(link = "curriculum", dropdown = "dropdown-settings", tab = "subject-detail") String activeMenu) {
        return "setting/curriculum/subject/detail/index";
    }
    
    @GetMapping("/setting/curriculum/subject/cpl-cpmk")
    public String subjectCplPage(Model model,@ActiveMenu(link = "curriculum", dropdown = "dropdown-settings", tab = "cpl-cpmk") String activeMenu) {
        return "setting/curriculum/subject/cpl-cpmk/index";
    }

    @GetMapping("/setting/curriculum/subject/cpl-cpmk/edit")
    public String subjectCplEditPage(Model model,@ActiveMenu(link = "curriculum", dropdown = "dropdown-settings", tab = "cpl-cpmk") String activeMenu) {
        return "setting/curriculum/subject/cpl-cpmk/edit";
    }

    @GetMapping("/setting/curriculum/subject/rps")
    public String subjectRpsPage(Model model,@ActiveMenu(link = "curriculum", dropdown = "dropdown-settings", tab = "rps") String activeMenu) {
        return "setting/curriculum/subject/rps/index";
    }

    @GetMapping("/setting/curriculum/subject/rps/edit")
    public String subjectRpsEditPage(Model model,@ActiveMenu(link = "curriculum", dropdown = "dropdown-settings", tab = "rps") String activeMenu) {
        return "setting/curriculum/subject/rps/edit";
    }
}
