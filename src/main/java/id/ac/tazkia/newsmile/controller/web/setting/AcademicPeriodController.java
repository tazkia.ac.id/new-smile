package id.ac.tazkia.newsmile.controller.web.setting;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;



@Controller
public class AcademicPeriodController {

    @GetMapping("/setting/academic-period")
        public String academicPeriodPage(Model model, @ActiveMenu(link = "academic-period", dropdown = "dropdown-settings", tab="academic") String activeMenu) {
            return "setting/academic-period/index";
    }

    @GetMapping("/setting/academic-period/program-study")
        public String programStudyPeriodPage(Model model, @ActiveMenu(link = "academic-period", dropdown = "dropdown-settings", tab="program-study") String activeMenu) {
            return "setting/academic-period/program-study/index";
    }

    @GetMapping("/setting/academic-period/finance")
        public String financePeriodPage(Model model, @ActiveMenu(link = "academic-period", dropdown = "dropdown-settings", tab="finance") String activeMenu) {
            return "setting/academic-period/finance/index";
    }
}
