package id.ac.tazkia.newsmile.controller.web.employee;

import id.ac.tazkia.newsmile.constants.Gender;
import id.ac.tazkia.newsmile.constants.LecturerType;
import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.User.RoleDao;
import id.ac.tazkia.newsmile.dao.User.UserDao;
import id.ac.tazkia.newsmile.dao.employee.EmployeeInstitutionDao;
import id.ac.tazkia.newsmile.dao.employee.EmployeeLecturerDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionProdiDao;
import id.ac.tazkia.newsmile.dto.filter.LecturerFilter;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.config.Role;
import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.employee.EmployeeLecturer;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class LecturerController {
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private EmployeeInstitutionDao employeeInstitutionDao;
    @Autowired
    private EmployeeLecturerDao employeeLecturerDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private InstitutionProdiDao institutionProdiDao;

    @GetMapping("/lecturer")
        public String curriculumPage(Model model, LecturerFilter params, HttpServletRequest request,
                                     @ActiveMenu(link = "lecturer", dropdown = "") String activeMenu) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        List<String> institutionIds = SessionHelper.getEmployeeInstitutionId(session,institutionDao,employesDao,employeeInstitutionDao);
        List<InstitutionProdi> institutionProdis = institutionProdiDao.getProdiIns(institutionIds);
        if (params.getGender() == null){
            params.setGender(Arrays.asList(Gender.PRIA.name(),Gender.WANITA.name()));
        }

        if (StringUtils.hasText(params.getTrash())){
            model.addAttribute("trash", true);
        }

        if (params.getType() == null){
            params.setType(Arrays.asList(LecturerType.DOSEN_TETAP.name(),LecturerType.DOSEN_TIDAK_TETAP.name(),LecturerType.DOSEN_HONORER.name()));
        }
        if (params.getProdi() == null){
            params.setProdi(institutionProdis.stream()
                    .map(InstitutionProdi::getId)
                    .collect(Collectors.toList()));
        }

        model.addAttribute("selectedProdi", params.getProdi());
        model.addAttribute("selectedType", params.getType());
        model.addAttribute("selectedGender", params.getGender());
        model.addAttribute("prodiList", institutionProdis);
        model.addAttribute("lecturerType", LecturerType.values());
        model.addAttribute("GenderList", Gender.values());

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                institutionIds,
                params.getTrash(),
                (search, ins, tr) -> employeeLecturerDao.pageSearchInstitutionDeleted(institutionIds,search,params.getProdi(),params.getGender(),params.getType(),pageable),
                (search, ins) -> employeeLecturerDao.pageSearchInstitution(institutionIds,search,params.getProdi(),params.getGender(),params.getType(), pageable),
                search -> employeeLecturerDao.pageSearchFoundation(foundation.getId(),search,params.getProdi(),params.getGender(),params.getType(), pageable),
                (search, tr) -> employeeLecturerDao.pageSearchFoundationDeleted(foundation.getId(),search,params.getProdi(),params.getGender(),params.getType(), pageable),
                page -> employeeLecturerDao.findAll(page),
                "lectureList",
                "lecturer/index",
                "lecturer/index",
                "lecturer/index",
                "lecturer/index"
        );
    }

    @PostMapping("/lecturer")
    public String saveLecturer(@Valid  EmployeeLecturer employeeLecturer, RedirectAttributes attributes){
        employeeLecturerDao.save(employeeLecturer);
        User user = employeeLecturer.getEmployes().getUser();
        if (user != null) {
            // Pastikan Role ada di database
            Optional<Role> optionalRole = roleDao.findById("dosen");
            if (optionalRole.isPresent()) {
                Set<Role> roles = new HashSet<>();
                roles.add(optionalRole.get());
                user.setRoles(roles);
                userDao.save(user);
            } else {
                log.error("Role with ID 'dosen' not found");
            }
        } else {
            log.error("User not found in EmployeeLecturer");
        }
        attributes.addFlashAttribute("success", true);

        return "redirect:lecturer";
    }

    @PostMapping("/delete-lecture")
    public String deleteLecture(@RequestParam EmployeeLecturer lecturer){
        employeeLecturerDao.delete(lecturer);
        return "redirect:lecturer";
    }

    @PostMapping("/restore-lecture")
    public String restoreLecture(@RequestParam EmployeeLecturer lecturer){
        lecturer.setStatus(StatusRecord.ACTIVE);
        employeeLecturerDao.save(lecturer);
        return "redirect:lecturer";
    }
}
