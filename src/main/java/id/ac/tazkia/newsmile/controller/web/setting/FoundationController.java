package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.CampusDao;
import id.ac.tazkia.newsmile.dao.FoundationDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dto.foundation.FoundationDto;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Campus;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.service.FoundationService;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class FoundationController {
    @Autowired
    private FoundationDao foundationDao;

    @Autowired
    private InstitutionDao institutionDao;

    @Autowired
    private CampusDao campusDao;

    @Autowired
    private FoundationService foundationService;

    public List<Foundation> getFoundationList(HttpSession session) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Foundation> foundationList = new ArrayList<>();
        if (foundation == null) {
            foundationList = foundationDao.findByStatus(StatusRecord.ACTIVE);
        } else {
            foundationList.add(foundationDao.findById(foundation.getId()).get());
        }
        return foundationList;
    }

    public List<Institution> getInstitutionList(HttpSession session) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Institution> institutionList = new ArrayList<>();
        if (foundation == null) {
            institutionList = institutionDao.findByStatus(StatusRecord.ACTIVE);
        } else {
            institutionList = institutionDao.findByStatusAndFoundation(StatusRecord.ACTIVE,foundation);
        }
        return institutionList;
    }

    @GetMapping("/setting/foundation")
    public String foundationPage(Model model, BaseRequestParamDto params,
                                 @ActiveMenu(link = "foundation", dropdown = "dropdown-settings") String activeMenu) {
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        model.addAttribute("foundation", new Foundation());
        if (StringUtils.hasText(params.getSearch())) {
            model.addAttribute("foundationList", foundationDao.findByFoundationNameContainingIgnoreCaseOrderByFoundationName(params.getSearch(), pageable));
        } else {
            model.addAttribute("foundationList", foundationDao.findAll(pageable));
        }
        return "setting/foundation/index";
    }

    @PostMapping("/setting/foundation")
    public String saveFoundation(@Valid @ModelAttribute FoundationDto foundationDto, BindingResult result, RedirectAttributes attributes){

        if (!foundationDto.getId().isBlank()) {
            foundationService.editFoundation(foundationDto.getId(), foundationDto);
        } else {
            foundationService.createFoundation(foundationDto);
        }
       attributes.addFlashAttribute("success", true);
        return "redirect:foundation";
    }

    @GetMapping("/setting/institution")
    public String institutionPage(Model model, BaseRequestParamDto params,HttpServletRequest request,
                                  @ActiveMenu(link = "institution", dropdown = "dropdown-settings") String activeMenu) {
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());

        HttpSession session = request.getSession();
        Foundation foundation = SessionHelper.getFoundation(session);

        model.addAttribute("foundationList", getFoundationList(session));

        return EntityUtil.preparePage(
                model,
                params,
                pageable,
                foundation,
                (search, found) -> institutionDao.findByInstitutionNameContainingIgnoreCaseAndFoundationOrderByInstitutionName(search, (Foundation) found, pageable),
                search -> institutionDao.findByInstitutionNameContainingIgnoreCaseOrderByInstitutionName(search, pageable),
                page -> institutionDao.findAll(page),
                "institutionList",
                "setting/institution/index",
                "setting/institution/index"
        );

    }

    @PostMapping("/setting/institution")
    public String saveInstitution(@Valid Institution institution, RedirectAttributes attributes) {
        institutionDao.save(institution);
        attributes.addFlashAttribute("success", true);
        return "redirect:institution";
    }

    @GetMapping("/setting/campus")
    public String campusPage(Model model, BaseRequestParamDto params,HttpServletRequest request,
                             @ActiveMenu(link = "campus", dropdown = "dropdown-settings") String activeMenu) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        model.addAttribute("institutionList", getInstitutionList(session));

        return EntityUtil.preparePage(
                model,
                params,
                pageable,
                foundation,
                (search, found) -> campusDao.findByInstitutionFoundationAndCampusNameContainingIgnoreCaseOrderByCampusName((Foundation) found,search, pageable),
                search -> campusDao.findByCampusNameContainingIgnoreCaseOrderByCampusName(search, pageable),
                page -> campusDao.findAll(page),
                "campusList",
                "setting/campus/index",
                "setting/campus/index"
        );
    }

    @PostMapping("/setting/campus")
    public String saveCampus(@Valid Campus campus, RedirectAttributes attributes){
        campusDao.save(campus);
        return "redirect:campus";
    }
}
