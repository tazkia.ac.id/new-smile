package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.dao.JobPositionDao;
import id.ac.tazkia.newsmile.dao.department.DepartmentDao;
import id.ac.tazkia.newsmile.dao.employee.EmployeeInstitutionDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.department.Department;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;


@Controller
@Slf4j
public class DepartmentController {
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private JobPositionDao jobPositionDao;
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private EmployeeInstitutionDao employeeInstitutionDao;

    @GetMapping("/setting/department")
    public String departmentPage(Model model, BaseRequestParamDto params, @ActiveMenu(link = "department", dropdown = "dropdown-settings") String activeMenu, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        model.addAttribute("institutionList", SessionHelper.getInstitutionList(session,institutionDao));

        return EntityUtil.preparePage(
                model,
                params,
                pageable,
                foundation,
                (search, found) -> departmentDao.pageSearchFoundation(foundation.getId(),search, pageable),
                search -> departmentDao.pageSearch(search, pageable),
                page -> departmentDao.findAll(page),
                "departmentList",
                "setting/department/index",
                "setting/department/index"
        );
    }

    @PostMapping("/setting/department")
    public String saveDepartment(@Valid Department department, RedirectAttributes attributes){
        departmentDao.save(department);
        attributes.addFlashAttribute("success", true);

        return "redirect:department";
    }

    @GetMapping("/setting/job-position")
    public String jobPositionPage(Model model, BaseRequestParamDto params,@RequestParam(required = false) String trash,
                                  HttpServletRequest request,@ActiveMenu(link = "job-position", dropdown = "dropdown-settings") String activeMenu) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        List<String> institutionIds = SessionHelper.getEmployeeInstitutionId(session,institutionDao,employesDao,employeeInstitutionDao);

        if (StringUtils.hasText(trash)){
            model.addAttribute("trash", true);
        }

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                institutionIds,
                trash,
                (search, ins, tr) -> jobPositionDao.pageSearchInstitutionDeleted(institutionIds,search,pageable),
                (search, ins) -> jobPositionDao.pageSearchInstitution(institutionIds,search, pageable),
                search -> jobPositionDao.pageSearchFoundation(foundation.getId(),search, pageable),
                (search, tr) -> jobPositionDao.pageSearchFoundationDeleted(foundation.getId(),search, pageable),
                page -> jobPositionDao.findAll(page),
                "jobPositionList",
                "setting/job-position/index",
                "setting/job-position/index",
                "setting/job-position/index",
                "setting/job-position/index"
        );
    }
    
    @GetMapping("/setting/job-position/role")
    public String jobPositionRolePage(Model model, @ActiveMenu(link = "job-position", dropdown = "dropdown-settings") String activeMenu) {
        return "setting/job-position/role/index";
    }
    
}
