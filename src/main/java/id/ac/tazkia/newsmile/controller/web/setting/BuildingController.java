package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.dao.CampusDao;
import id.ac.tazkia.newsmile.dao.building.BuildingDao;
import id.ac.tazkia.newsmile.dao.building.BuildingFloorDao;
import id.ac.tazkia.newsmile.dao.building.BuildingRoomDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.building.Building;
import id.ac.tazkia.newsmile.entity.building.BuildingFloor;
import id.ac.tazkia.newsmile.entity.building.BuildingRoom;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class BuildingController {
    @Autowired
    private BuildingDao buildingDao;
    @Autowired
    private BuildingFloorDao buildingFloorDao;
    @Autowired
    BuildingRoomDao buildingRoomDao;
    @Autowired
    private CampusDao campusDao;

    @GetMapping("/setting/building")
    public String buildingPage(Model model, BaseRequestParamDto params, HttpServletRequest request,
                               @ActiveMenu(link = "building", dropdown = "dropdown-settings", tab = "building") String activeMenu) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        model.addAttribute("campusList", SessionHelper.getCampusList(session,campusDao));

        return EntityUtil.preparePage(
                model,
                params,
                pageable,
                foundation,
                (search, found) -> buildingDao.pageSearchFoundation(foundation.getId(),search, pageable),
                search -> buildingDao.pageSearch(search, pageable),
                page -> buildingDao.findAll(page),
                "buildingList",
                "setting/building/index",
                "setting/building/index"
        );
    }

    @PostMapping("/setting/building")
    public String saveBuilding(@Valid Building building, RedirectAttributes attributes) {
        building.setInstitution(building.getCampus().getInstitution());
        buildingDao.save(building);
        attributes.addFlashAttribute("success", true);
        return "redirect:building";
    }

    @GetMapping("/setting/building/floor")
    public String buildingFloorPage(@RequestParam(required = false) Building building, Model model, BaseRequestParamDto params, HttpServletRequest request,
                                    @ActiveMenu(link = "building", dropdown = "dropdown-settings", tab = "floor") String activeMenu) {

        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        model.addAttribute("buildingList", SessionHelper.getBuildingList(session,buildingDao));
        model.addAttribute("building", building);

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                foundation,
                building,
                (search, found, fl) -> buildingFloorDao.pageSearchFoundationBuilding(foundation.getId(), search, building.getId(), pageable),
                (search, found) -> buildingFloorDao.pageSearchFoundation(foundation.getId(),search, pageable),
                search -> buildingFloorDao.pageSearch(search, pageable),
                (search, fl) -> buildingFloorDao.pageSearchBuilding(search,building.getId(), pageable),
                page -> buildingFloorDao.findAll(page),
                "floorList",
                "setting/floor/index",
                "setting/floor/index",
                "setting/floor/index",
                "setting/floor/index"
        );

    }

    @PostMapping("/setting/building/floor")
    public String saveFloor(@Valid BuildingFloor buildingFloor, RedirectAttributes attributes) {
        buildingFloorDao.save(buildingFloor);
        attributes.addFlashAttribute("success", true);
        return "redirect:floor?building="+buildingFloor.getBuilding().getId();
    }

    @GetMapping("/setting/building/room")
    public String floorRoomPage(@RequestParam(required = false) BuildingFloor floor, Model model, BaseRequestParamDto params, HttpServletRequest request,
                                @ActiveMenu(link = "building", dropdown = "dropdown-settings", tab = "room") String activeMenu) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        model.addAttribute("floorList", SessionHelper.getFloorList(session,buildingFloorDao));
        model.addAttribute("floor", floor);

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                foundation,
                floor,
                (search, found, fl) -> buildingRoomDao.pageSearchFoundationFloor(floor.getId(), foundation.getId(), search, pageable),
                (search, found) -> buildingRoomDao.pageSearchFoundation(foundation.getId(), search, pageable),
                search -> buildingRoomDao.pageSearch(search, pageable),
                (search, fl) -> buildingRoomDao.pageSearchFloor(search,floor.getId(), pageable),
                page -> buildingRoomDao.findAll(page),
                "roomList",
                "setting/room/index",
                "setting/room/index",
                "setting/room/index",
                "setting/room/index"
        );
    }

    @PostMapping("/setting/building/room")
    public String saveRoom(@Valid BuildingRoom buildingRoom, RedirectAttributes attributes) {
        buildingRoomDao.save(buildingRoom);
        attributes.addFlashAttribute("success", true);
        return "redirect:room?floor="+buildingRoom.getBuildingFloor().getId();
    }
}
