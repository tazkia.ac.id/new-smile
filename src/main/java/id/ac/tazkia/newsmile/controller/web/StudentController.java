package id.ac.tazkia.newsmile.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;



@Controller
public class StudentController {

    @GetMapping("/student")
        public String curriculumPage(Model model, @ActiveMenu(link = "student", dropdown = "") String activeMenu) {
            return "student/index";
    }
}
