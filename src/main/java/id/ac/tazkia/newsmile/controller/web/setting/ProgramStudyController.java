package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.dao.department.DepartmentDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionConcentrationDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionFacultyDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionProdiDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.institution.InstitutionConcentration;
import id.ac.tazkia.newsmile.entity.institution.InstitutionFaculty;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class ProgramStudyController {
    @Autowired
    private InstitutionFacultyDao institutionFacultyDao;
    @Autowired
    private InstitutionProdiDao institutionProdiDao;
    @Autowired
    private InstitutionConcentrationDao institutionConcentrationDao;
    @Autowired
    private DepartmentDao departmentDao;

    @GetMapping("/setting/program-study")
    public String facultyPage(@RequestParam(required = false) InstitutionFaculty faculty, Model model, BaseRequestParamDto params, HttpServletRequest request,
                              @ActiveMenu(link = "faculty", dropdown = "dropdown-settings", tab = "program-study") String activeMenu) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        model.addAttribute("facultyList", SessionHelper.getFacultyList(session,institutionFacultyDao));
        model.addAttribute("departmentList", SessionHelper.getDepartmentProdi(session,departmentDao));
        model.addAttribute("faculty", faculty);

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                foundation,
                faculty,
                (search, found, fac) -> institutionProdiDao.pageSearchFoundationFaculty(foundation.getId(),faculty.getId(),search,pageable),
                (search, found) -> institutionProdiDao.pageSearchFoundation(foundation.getId(),search, pageable),
                search -> institutionProdiDao.pageSearch(search, pageable),
                (search, fl) -> institutionProdiDao.pageSearchFaculty(faculty.getId(),search, pageable),
                page -> institutionProdiDao.findAll(page),
                "prodiList",
                "setting/program-study/index",
                "setting/program-study/index",
                "setting/program-study/index",
                "setting/program-study/index"
        );
    }

    @PostMapping("/setting/program-study")
    public String saveProgramStudy(@Valid InstitutionProdi institutionProdi, RedirectAttributes attributes) {
        institutionProdiDao.save(institutionProdi);
        attributes.addFlashAttribute("success", true);
        return "redirect:program-study?faculty="+institutionProdi.getInstitutionFaculty().getId();
    }

    @GetMapping("/setting/program-study/concentration")
    public String concentrationPage(@RequestParam(required = false, name = "prodiId", value = "prodiId") String prodiId, Model model, @ActiveMenu(link = "faculty", dropdown = "dropdown-settings", tab = "concentration") String activeMenu,
                                    BaseRequestParamDto params, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        List<InstitutionProdi> institutionProdis =  SessionHelper.getProdiList(session,institutionProdiDao);
        InstitutionProdi prodi;
        if (StringUtils.hasText(prodiId)){
            prodi = institutionProdiDao.findById(prodiId).get();
        }else {
            prodi = institutionProdis.get(0);
        }

        model.addAttribute("prodi", prodi);
        model.addAttribute("prodiList", institutionProdis);
        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                foundation,
                prodi,
                (search, found, pr) -> institutionConcentrationDao.pageSearchFoundationProdi(foundation.getId(), search, prodi.getId(), pageable),
                (search, found) -> institutionConcentrationDao.pageSearchFoundation(foundation.getId(),search, pageable),
                search -> institutionConcentrationDao.pageSearch(search, pageable),
                (search, pr) -> institutionConcentrationDao.pageSearchProdi(search, prodi.getId(), pageable),
                page -> institutionConcentrationDao.findAll(page),
                "concentrationList",
                "setting/concentration/index",
                "setting/concentration/index",
                "setting/concentration/index",
                "setting/concentration/index"
        );

    }

    @PostMapping("/setting/program-study/concentration")
    public String saveConcentration(@Valid InstitutionConcentration institutionConcentration, RedirectAttributes attributes) {
        institutionConcentrationDao.save(institutionConcentration);
        attributes.addFlashAttribute("success", true);
        return "redirect:concentration?prodi="+institutionConcentration.getInstitutionProdi().getId();
    }
    
}
