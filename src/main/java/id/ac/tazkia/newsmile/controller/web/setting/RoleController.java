package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.dao.User.PermissionDao;
import id.ac.tazkia.newsmile.dao.User.RoleDao;
import id.ac.tazkia.newsmile.dao.User.UserDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.config.Permission;
import id.ac.tazkia.newsmile.entity.config.Role;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;
import java.util.Set;

@Controller
@Slf4j
public class RoleController {
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private PermissionDao permissionDao;

    @GetMapping("/setting/role")
    public String rolePage(Model model, @ActiveMenu(link = "role", dropdown = "dropdown-settings") String activeMenu, BaseRequestParamDto params) {
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        model.addAttribute("params",params);

        if (!StringUtils.hasText(params.getSearch())){
            model.addAttribute("roleList", roleDao.findAll(pageable));
        }else {
            model.addAttribute("roleList", roleDao.findByNameContainingIgnoreCaseOrderByNameAsc(params.getSearch(),pageable));
        }
        return "setting/role/index";
    }

    @PostMapping("/setting/role")
    public String saveRole(@Valid Role role, RedirectAttributes attributes){
        roleDao.save(role);
        attributes.addFlashAttribute("success", true);
        return "redirect:role";
    }


    @GetMapping("/setting/role/permission")
    public String rolePermissionPage(Model model,@RequestParam String id, @ActiveMenu(link = "role", dropdown = "dropdown-settings") String activeMenu) {
        Optional<Role> optionalRole = roleDao.findById(id);
        if (optionalRole.isEmpty()){
            log.error("role id {} not found", id);
        }

        model.addAttribute("role", optionalRole.get());
        model.addAttribute("permission", permissionDao.findAll());
        model.addAttribute("permissionList", optionalRole.get().getPermissions());
        return "setting/role/permission/index";
    }

    @PostMapping("/setting/role/permission")
    public String saveRolePeremission(@RequestParam Role role, @RequestParam("permission") Set<Permission> permissions){
        role.setPermissions(permissions);
        roleDao.save(role);
        return "redirect:permission?id="+role.getId();
    }

    @PostMapping("/setting/role/delete-permission")
    @Transactional
    public String saveRolePeremission(@RequestParam Role role, @RequestParam Permission permission){
        permissionDao.deletePermissionByRoleAndPermission(role.getId(),permission.getId());
        return "redirect:permission?id="+role.getId();
    }

}
