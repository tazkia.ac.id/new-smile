package id.ac.tazkia.newsmile.controller.api;

import id.ac.tazkia.newsmile.dao.employee.EmployeeInstitutionDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dto.BaseResponseDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeApi {
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private EmployeeInstitutionDao employeeInstitutionDao;

    @GetMapping("/institution/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getEmployeeInstitution(@PathVariable String id) {
        try {
            Optional<Employes> optionalEmployes = employesDao.findById(id);
            if (optionalEmployes.isEmpty()){
                log.info("Employee id {} not found", id);
            }
            List<Institution> employeeInstitution = institutionDao.getEmployeeInstitution(optionalEmployes.get().getId());
            if (!employeeInstitution.isEmpty()){
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(employeeInstitution)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Institution Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Employee] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/lecture")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getEmployeeLecture(@RequestParam(required = false) String id,@RequestParam(required = false) String search ,HttpServletRequest request, Pageable pageable) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Foundation foundation = SessionHelper.getFoundation(session);
        List<String> institutionIds = SessionHelper.getEmployeeInstitutionId(session,institutionDao,employesDao,employeeInstitutionDao);
        Optional<Employes> optionalEmployes = employesDao.findById(id);

        if (optionalEmployes.isEmpty()){
            log.info("Employee id {} not found", id);
        }
        try {
            Page<Employes> employes = employesDao.getEmployeeForLecturer(institutionIds,optionalEmployes.get().getId(),search, pageable);

            if (!employes.isEmpty()){
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(employes)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Institution Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Employee] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/lecturer")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getLecture(@RequestParam(required = false) String search ,HttpServletRequest request, Pageable pageable) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Foundation foundation = SessionHelper.getFoundation(session);
        List<String> institutionIds = SessionHelper.getEmployeeInstitutionId(session,institutionDao,employesDao,employeeInstitutionDao);
        try {
            Page<Employes> employes = employesDao.getLecturer(institutionIds,search, pageable);

            if (!employes.isEmpty()){
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(employes)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Institution Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Employee] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

    @GetMapping("/staff")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> getStaff(@RequestParam(required = false) String search ,@RequestParam(required = false) String id ,HttpServletRequest request, Pageable pageable) {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        Foundation foundation = SessionHelper.getFoundation(session);
        List<String> institutionIds = SessionHelper.getEmployeeInstitutionId(session,institutionDao,employesDao,employeeInstitutionDao);
        Optional<Employes> optionalEmployes = employesDao.findById(id);
        Page<Employes> employes = null;
        if (optionalEmployes.isEmpty()){
            employes = employesDao.getStaff(institutionIds,search,pageable);
        }else {
            employes = employesDao.getStaffEdit(institutionIds,search,id,pageable);
        }
        try {

            if (!employes.isEmpty()){
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("00")
                                .responseMessage("Data Found")
                                .data(employes)
                                .build());
            }else {
                return ResponseEntity.ok().body(
                        BaseResponseDto.builder()
                                .responseCode("404")
                                .responseMessage("Institution Not Found")
                                .build());
            }
        } catch (Exception e) {
            log.error("[GET Employee] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

}
