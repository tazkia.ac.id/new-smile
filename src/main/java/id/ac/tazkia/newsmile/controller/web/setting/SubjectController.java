package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.SubjectDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionProdiDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Subject;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class SubjectController {
    @Autowired
    private InstitutionProdiDao institutionProdiDao;
    @Autowired
    private SubjectDao subjectDao;


    @GetMapping("/setting/subject")
    public String curriculumPage(@RequestParam(required = false) String trash, Model model, @ActiveMenu(link = "subject", dropdown = "dropdown-settings", tab = "subject") String activeMenu,
                                 BaseRequestParamDto params, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        List<InstitutionProdi> institutionProdis =  SessionHelper.getProdiList(session,institutionProdiDao);
        model.addAttribute("prodiList", institutionProdis);
        model.addAttribute("trash", trash);

        return EntityUtil.preparePageWithOptionalCategory(
                model,
                params,
                pageable,
                institutionProdis,
                trash,
                (search, prodi, tr) -> subjectDao.pageSearchProdiDeleted(institutionProdis.stream().map(InstitutionProdi::getId).collect(Collectors.toList()),search, pageable),
                (search, prodi) -> subjectDao.pageSearchProdi(institutionProdis.stream().map(InstitutionProdi::getId).collect(Collectors.toList()),search, pageable),
                search -> subjectDao.pageSearch(search, pageable),
                (search, tr) -> subjectDao.pageSearchDeleted(search, pageable),
                page -> subjectDao.findAll(page),
                "subjectList",
                "setting/subject/index",
                "setting/subject/index",
                "setting/subject/index",
                "setting/subject/index"
        );

    }

    @PostMapping("/setting/subject")
    public String saveSubject(@Valid Subject subject, RedirectAttributes attributes) {
        subject.setInstitutionFaculty(subject.getInstitutionProdi().getInstitutionFaculty());
        subject.setInstitution(subject.getInstitutionProdi().getInstitutionFaculty().getInstitution());
        subjectDao.save(subject);
        attributes.addFlashAttribute("success", true);
        return "redirect:subject";
    }

    @PostMapping("/setting/subject/delete")
    public String deleteSubject(@RequestParam Subject subject, RedirectAttributes attributes) {
        subjectDao.delete(subject);
        attributes.addFlashAttribute("success", true);
        return "redirect:../subject";
    }

    @PostMapping("/setting/subject/restore")
    public String restoreSubject(@RequestParam Subject subject, RedirectAttributes attributes) {
        subject.setStatus(StatusRecord.ACTIVE);
        subjectDao.save(subject);
        attributes.addFlashAttribute("success", true);
        return "redirect:../subject?trash=true";
    }
}
