package id.ac.tazkia.newsmile.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class DashboardController {
@GetMapping("/dashboard/student")
public String dashboardStudentPage() {
    return "dashboard/student";
}

@GetMapping("/dashboard/admin")
public String dashboardAdminpage(Model model) {
    model.addAttribute("linkActive", "dashboard" );
    return "dashboard/admin";
}

}
