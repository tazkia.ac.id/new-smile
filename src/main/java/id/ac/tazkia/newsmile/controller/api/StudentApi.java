package id.ac.tazkia.newsmile.controller.api;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.student.StudentDao;
import id.ac.tazkia.newsmile.dao.student.StudentStatusAcademicDao;
import id.ac.tazkia.newsmile.dto.BaseResponseDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.student.Student;
import id.ac.tazkia.newsmile.entity.student.StudentStatusAcademic;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/student")
public class StudentApi {
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private StudentStatusAcademicDao studentStatusAcademicDao;

    @GetMapping("/get-graphic-ips/{studentId}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> grafikIndekPrestasi(@PathVariable String studentId, HttpSession session) {
        try {
            Student student = (Student) session.getAttribute("userDetail");
            List<StudentStatusAcademic> studentStatusAcademic = studentStatusAcademicDao.findByStudentAndStatusOrderBySemesterAsc(student, StatusRecord.ACTIVE);
            return ResponseEntity.ok().body(
                    BaseResponseDto.builder()
                            .responseCode("00")
                            .responseMessage("Data Found")
                            .data(studentStatusAcademic)
                            .build());
        } catch (Exception e) {
            log.error("[GET GRAPHIC IPS] - [ERROR] : Unable to Get data , {} ", e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
                    BaseResponseDto.builder()
                            .responseCode("ERR500")
                            .responseMessage(e.getLocalizedMessage())
                            .build());
        }
    }

}
