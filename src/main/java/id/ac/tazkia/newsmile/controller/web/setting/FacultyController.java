package id.ac.tazkia.newsmile.controller.web.setting;

import id.ac.tazkia.newsmile.dao.department.DepartmentDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionFacultyDao;
import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.institution.InstitutionFaculty;
import id.ac.tazkia.newsmile.helper.SessionHelper;
import id.ac.tazkia.newsmile.utils.EntityUtil;
import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class FacultyController {
    @Autowired
    private InstitutionFacultyDao institutionFacultyDao;
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private DepartmentDao departmentDao;

    @GetMapping("/setting/faculty")
    public String facultyPage(BaseRequestParamDto params, Model model, @ActiveMenu(link = "faculty", dropdown = "dropdown-settings", tab = "faculty") String activeMenu, HttpServletRequest request) {
        HttpSession session = request.getSession();
        Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());
        Foundation foundation = SessionHelper.getFoundation(session);
        model.addAttribute("departmentList", SessionHelper.getDepartmentList(session, departmentDao));
        model.addAttribute("institutionList", SessionHelper.getInstitutionList(session, institutionDao));

        return EntityUtil.preparePage(
                model,
                params,
                pageable,
                foundation,
                (search, found) -> institutionFacultyDao.pageSearchFoundation(foundation.getId(),search, pageable),
                search -> institutionFacultyDao.pageSearch(search, pageable),
                page -> institutionFacultyDao.findAll(page),
                "facultyList",
                "setting/faculty/index",
                "setting/faculty/index"
        );
    }

    @PostMapping("/setting/faculty")
    public String saveFaculty(@Valid InstitutionFaculty institutionFaculty, RedirectAttributes attributes) {
        institutionFacultyDao.save(institutionFaculty);
        attributes.addFlashAttribute("success", true);
        return "redirect:faculty";
    }
    
}
