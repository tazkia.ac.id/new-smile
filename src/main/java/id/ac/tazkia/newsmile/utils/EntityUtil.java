package id.ac.tazkia.newsmile.utils;

import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
import id.ac.tazkia.newsmile.dto.filter.CurriculumSubjectFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import java.util.function.BiFunction;
import java.util.function.Function;

public class EntityUtil {
    public static <T> String preparePage(Model model, BaseRequestParamDto params, Pageable pageable,
                                         Object entity,
                                         BiFunction<String, Object, Page<T>> searchFunctionWithEntity,
                                         Function<String, Page<T>> searchFunction,
                                         Function<Pageable, Page<T>> findAllFunction,
                                         String searchAttribute,
                                         String viewName,
                                         String viewNameWithFoundation) {
        if (entity != null && StringUtils.hasText(params.getSearch())) {
            return preparePageWithEntityAndSearch(model, params, pageable, entity, searchFunctionWithEntity, searchAttribute, viewNameWithFoundation);
        } else if (entity != null) {
            return preparePageWithEntity(model, params, pageable, entity, searchFunctionWithEntity, searchAttribute, viewNameWithFoundation);
        } else {
            return preparePageForAll(model, params, pageable, searchFunction, findAllFunction, searchAttribute, viewName);
        }
    }

    private static <T> String preparePageWithEntityAndSearch(Model model, BaseRequestParamDto params, Pageable pageable,
                                                             Object entity,
                                                             BiFunction<String, Object, Page<T>> searchFunctionWithEntity,
                                                             String searchAttribute, String viewNameWithFoundation) {
        model.addAttribute("search", params.getSearch());
        Page<T> pageResult = searchFunctionWithEntity.apply(params.getSearch(), entity);
        model.addAttribute(searchAttribute, pageResult);
        return viewNameWithFoundation;
    }

    private static <T> String preparePageWithEntity(Model model, BaseRequestParamDto params, Pageable pageable,
                                                        Object entity,
                                                        BiFunction<String, Object, Page<T>> searchFunctionWithEntity,
                                                        String searchAttribute, String viewNameWithFoundation) {
        Page<T> pageResult = searchFunctionWithEntity.apply("", entity);
        model.addAttribute(searchAttribute, pageResult);
        return viewNameWithFoundation;
    }

    private static <T> String preparePageForAll(Model model, BaseRequestParamDto params, Pageable pageable,
                                                Function<String, Page<T>> searchFunction,
                                                Function<Pageable, Page<T>> findAllFunction,
                                                String searchAttribute, String viewName) {
        if (StringUtils.hasText(params.getSearch())) {
            model.addAttribute("search", params.getSearch());
            Page<T> pageResult = searchFunction.apply(params.getSearch());
            model.addAttribute(searchAttribute, pageResult);
        } else {
            Page<T> pageResult = findAllFunction.apply(pageable);
            model.addAttribute(searchAttribute, pageResult);
        }
        return viewName;
    }

    public static <T> String preparePageWithOptionalCategory(Model model, BaseRequestParamDto params, Pageable pageable,
                                                             Object entity,
                                                             Object floor,
                                                             TriFunction<String, Object, Object, Page<T>> searchFunctionWithEntityAndCategory,
                                                             BiFunction<String, Object, Page<T>> searchFunctionWithEntity,
                                                             Function<String, Page<T>> searchFunction,
                                                             BiFunction<String, Object, Page<T>> searchFunctionWithFloor,
                                                             Function<Pageable, Page<T>> findAllFunction,
                                                             String searchAttribute,
                                                             String viewName,
                                                             String viewNameWithEntity,
                                                             String viewNameWithEntityAndFloor,
                                                             String viewNameWithFloor) {
        if (entity != null && floor != null && StringUtils.hasText(params.getSearch())) {
            return preparePageWithEntityFloorAndSearch(model, params, pageable, entity, floor, searchFunctionWithEntityAndCategory, searchAttribute, viewNameWithEntityAndFloor);
        } else if (entity != null && floor != null) {
            return preparePageWithEntityAndFloor(model, params, pageable, entity, floor, searchFunctionWithEntityAndCategory, searchAttribute, viewNameWithEntityAndFloor);
        } else if (entity != null && StringUtils.hasText(params.getSearch())) {
            return preparePageWithEntityAndSearch(model, params, pageable, entity, searchFunctionWithEntity, searchAttribute, viewNameWithEntity);
        } else if (entity != null) {
            return preparePageWithEntity(model, params, pageable, entity, searchFunctionWithEntity, searchAttribute, viewNameWithEntity);
        } else if (floor != null && StringUtils.hasText(params.getSearch())) {
            return preparePageWithFloorAndSearch(model, params, pageable, floor, searchFunctionWithFloor, searchAttribute, viewNameWithFloor);
        } else if (floor != null) {
            return preparePageWithFloor(model, params, pageable, floor, searchFunctionWithFloor, searchAttribute, viewNameWithFloor);
        } else {
            return preparePageForAll(model, params, pageable, searchFunction, findAllFunction, searchAttribute, viewName);
        }
    }

    public static <T> String prepareCurriculumWithOptionalCategory(Model model, CurriculumSubjectFilter params, Pageable pageable,
                                                                   Object entity,
                                                                   Object floor,
                                                                   TriFunction<String, Object, Object, Page<T>> searchFunctionWithEntityAndCategory,
                                                                   BiFunction<String, Object, Page<T>> searchFunctionWithEntity,
                                                                   Function<String, Page<T>> searchFunction,
                                                                   BiFunction<String, Object, Page<T>> searchFunctionWithFloor,
                                                                   Function<Pageable, Page<T>> findAllFunction,
                                                                   String searchAttribute,
                                                                   String viewName,
                                                                   String viewNameWithEntity,
                                                                   String viewNameWithEntityAndFloor,
                                                                   String viewNameWithFloor) {
        if (entity != null && floor != null && StringUtils.hasText(params.getSearch())) {
            return preparePageWithEntityFloorAndSearch(model, params, pageable, entity, floor, searchFunctionWithEntityAndCategory, searchAttribute, viewNameWithEntityAndFloor);
        } else if (entity != null && floor != null) {
            return preparePageWithEntityAndFloor(model, params, pageable, entity, floor, searchFunctionWithEntityAndCategory, searchAttribute, viewNameWithEntityAndFloor);
        } else if (entity != null && StringUtils.hasText(params.getSearch())) {
            return preparePageWithEntityAndSearch(model, params, pageable, entity, searchFunctionWithEntity, searchAttribute, viewNameWithEntity);
        } else if (entity != null) {
            return preparePageWithEntity(model, params, pageable, entity, searchFunctionWithEntity, searchAttribute, viewNameWithEntity);
        } else if (floor != null && StringUtils.hasText(params.getSearch())) {
            return preparePageWithFloorAndSearch(model, params, pageable, floor, searchFunctionWithFloor, searchAttribute, viewNameWithFloor);
        } else if (floor != null) {
            return preparePageWithFloor(model, params, pageable, floor, searchFunctionWithFloor, searchAttribute, viewNameWithFloor);
        } else {
            return preparePageForAll(model, params, pageable, searchFunction, findAllFunction, searchAttribute, viewName);
        }
    }

    private static <T> String preparePageWithEntityFloorAndSearch(Model model, BaseRequestParamDto params, Pageable pageable,
                                                                  Object entity, Object floor,
                                                                  TriFunction<String, Object, Object, Page<T>> searchFunctionWithEntityAndFloor,
                                                                  String searchAttribute, String viewNameWithEntityAndFloor) {
        model.addAttribute("search", params.getSearch());
        Page<T> pageResult = searchFunctionWithEntityAndFloor.apply(params.getSearch(), entity, floor);
        model.addAttribute(searchAttribute, pageResult);
        return viewNameWithEntityAndFloor;
    }

    private static <T> String preparePageWithEntityAndFloor(Model model, BaseRequestParamDto params, Pageable pageable,
                                                            Object entity, Object floor,
                                                            TriFunction<String, Object, Object, Page<T>> searchFunctionWithEntityAndFloor,
                                                            String searchAttribute, String viewNameWithEntityAndFloor) {
        Page<T> pageResult = searchFunctionWithEntityAndFloor.apply("", entity, floor);
        model.addAttribute(searchAttribute, pageResult);
        return viewNameWithEntityAndFloor;
    }

    private static <T> String preparePageWithFloorAndSearch(Model model, BaseRequestParamDto params, Pageable pageable,
                                                            Object floor,
                                                            BiFunction<String, Object, Page<T>> searchFunctionWithFloor,
                                                            String searchAttribute, String viewNameWithFloor) {
        model.addAttribute("search", params.getSearch());
        Page<T> pageResult = searchFunctionWithFloor.apply(params.getSearch(), floor);
        model.addAttribute(searchAttribute, pageResult);
        return viewNameWithFloor;
    }

    private static <T> String preparePageWithFloor(Model model, BaseRequestParamDto params, Pageable pageable,
                                                   Object floor,
                                                   BiFunction<String, Object, Page<T>> searchFunctionWithFloor,
                                                   String searchAttribute, String viewNameWithFloor) {
        Page<T> pageResult = searchFunctionWithFloor.apply("", floor);
        model.addAttribute(searchAttribute, pageResult);
        return viewNameWithFloor;
    }
}