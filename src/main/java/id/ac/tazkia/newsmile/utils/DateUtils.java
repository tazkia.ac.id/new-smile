package id.ac.tazkia.newsmile.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateUtils {
    private static final DateTimeFormatter INDONESIAN_DATE_FORMATTER =
            DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("id", "ID"));

    public static String formatIndonesianDate(LocalDate date) {
        return date.format(INDONESIAN_DATE_FORMATTER);
    }
}
