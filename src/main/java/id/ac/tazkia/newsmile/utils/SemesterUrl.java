package id.ac.tazkia.newsmile.utils;

import java.util.List;

public class SemesterUrl {
    public static String convertToSemesterString(List<Integer> semesters) {
        if (semesters == null || semesters.isEmpty()) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        sb.append(semesters.get(0)); // Elemen pertama tidak perlu &semester=

        for (int i = 1; i < semesters.size(); i++) {
            sb.append("&semester=").append(semesters.get(i));
        }

        return sb.toString();
    }
}
