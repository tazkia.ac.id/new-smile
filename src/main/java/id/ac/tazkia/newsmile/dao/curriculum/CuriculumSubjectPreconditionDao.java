package id.ac.tazkia.newsmile.dao.curriculum;

import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubjectPrecondition;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CuriculumSubjectPreconditionDao extends CrudRepository<CuriculumSubjectPrecondition, String>,
        PagingAndSortingRepository<CuriculumSubjectPrecondition, String> {

}
