package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Campus;
import id.ac.tazkia.newsmile.entity.Foundation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CampusDao extends CrudRepository<Campus, String>, PagingAndSortingRepository<Campus, String> {
    Page<Campus> findByCampusNameContainingIgnoreCaseOrderByCampusName(String campusNama, Pageable pageable);
    Page<Campus> findByInstitutionFoundationAndCampusNameContainingIgnoreCaseOrderByCampusName(Foundation foundation, String campusNama, Pageable pageable);
    List<Campus> findByStatusOrderByCampusNameAsc(StatusRecord statusRecord);
    List<Campus> findByInstitutionFoundationAndStatusOrderByCampusNameAsc(Foundation foundation, StatusRecord statusRecord);

}
