package id.ac.tazkia.newsmile.dao.curriculum;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.curriculum.Curiculum;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CuriculumDao extends CrudRepository<Curiculum, String>, PagingAndSortingRepository<Curiculum, String> {
    @Query(value = "SELECT c.* FROM curiculum as c inner join institution_prodi as p on c.id_prodi = p.id " +
            "where c.status = 'ACTIVE' and c.id_prodi in (?1) and (c.curiculum_code like %?2% or c.curiculum_name like %?2% or p.prodi_name like %?2%) order by p.prodi_name, c.curiculum_name asc;",
            countQuery = "SELECT c.* FROM curiculum as c inner join institution_prodi as p on c.id_prodi = p.id " +
                    "where c.status = 'ACTIVE' and c.id_prodi in (?1) and (c.curiculum_code like %?2% or c.curiculum_name like %?2% or p.prodi_name like %?2%) order by p.prodi_name, c.curiculum_name asc;",
            nativeQuery = true)
    Page<Curiculum> pageSearchProdi(List<String> prodId, String search, Pageable pageable);

    @Query(value = "SELECT c.* FROM curiculum as c inner join institution_prodi as p on c.id_prodi = p.id where c.status = 'ACTIVE' and  (c.curiculum_code like %?1% or c.curiculum_name like %?1% or p.prodi_name like %?1%) order by p.prodi_name, c.curiculum_name asc;",
            countQuery = "SELECT c.* FROM curiculum as c inner join institution_prodi as p on c.id_prodi = p.id where c.status = 'ACTIVE'  and (c.curiculum_code like %?1% or c.curiculum_name like %?1% or p.prodi_name like %?1%) order by p.prodi_name, c.curiculum_name asc;",
            nativeQuery = true)
    Page<Curiculum> pageSearch(String search, Pageable pageable);

    List<Curiculum> findByStatusAndInstitutionProdiOrderByCuriculumNameAsc(StatusRecord statusRecord, InstitutionProdi institutionProdi);

    Curiculum findFirstByStatusAndInstitutionProdiOrderByCuriculumName(StatusRecord statusRecord, InstitutionProdi institutionProdi);

}
