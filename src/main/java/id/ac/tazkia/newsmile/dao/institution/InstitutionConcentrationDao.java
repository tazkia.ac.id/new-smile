package id.ac.tazkia.newsmile.dao.institution;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.institution.InstitutionConcentration;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstitutionConcentrationDao extends PagingAndSortingRepository<InstitutionConcentration, String>, CrudRepository<InstitutionConcentration, String> {
    List<InstitutionConcentration> findByStatusAndInstitutionProdiOrderByConcentrationNameAsc(StatusRecord statusRecord, InstitutionProdi institutionProdi);

    @Query(value = "SELECT c.* FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
            "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
            "where c.status in ('ACTIVE','INACTIVE') and i.id_foundation = ?1 and concentration_name like %?2% and c.id_prodi = ?3 \n" +
            "order by concentration_name,p.prodi_name asc",
            countQuery = "SELECT count(*) FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
                    "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
                    "where c.status in ('ACTIVE','INACTIVE') and i.id_foundation = ?1 and concentration_name like %?2% and c.id_prodi = ?3 \n" +
                    "order by concentration_name,p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionConcentration> pageSearchFoundationProdi(String foundation, String search, String prodi, Pageable pageable);

    @Query(value = "SELECT c.* FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
            "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
            "where c.status in ('ACTIVE','INACTIVE') and i.id_foundation = ?1 and concentration_name like %?2% \n" +
            "order by concentration_name,p.prodi_name asc",
            countQuery = "SELECT count(*) FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
                    "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
                    "where c.status in ('ACTIVE','INACTIVE') and i.id_foundation = ?1 and concentration_name like %?2% \n" +
                    "order by concentration_name,p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionConcentration> pageSearchFoundation(String foundation, String search, Pageable pageable);

    @Query(value = "SELECT c.* FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
            "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
            "where c.status in ('ACTIVE','INACTIVE') and concentration_name like %?1%\n" +
            "order by concentration_name,p.prodi_name asc",
            countQuery = "SELECT count(*) FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
                    "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
                    "where c.status in ('ACTIVE','INACTIVE') and concentration_name like %?1%\n" +
                    "order by concentration_name,p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionConcentration> pageSearch(String search, Pageable pageable);

    @Query(value = "SELECT c.* FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
            "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
            "where c.status in ('ACTIVE','INACTIVE') and concentration_name like %?1% and c.id_prodi = ?2 \n" +
            "order by concentration_name,p.prodi_name asc",
            countQuery = "SELECT count(*) FROM institution_concentration as c inner join institution_prodi as p on c.id_prodi = p.id\n" +
                    "inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id\n" +
                    "where c.status in ('ACTIVE','INACTIVE') and concentration_name like %?1% and c.id_prodi = ?2 \n" +
                    "order by concentration_name,p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionConcentration> pageSearchProdi(String search, String prodi, Pageable pageable);
}
