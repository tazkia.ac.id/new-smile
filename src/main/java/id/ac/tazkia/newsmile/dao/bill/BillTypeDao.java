package id.ac.tazkia.newsmile.dao.bill;

import id.ac.tazkia.newsmile.entity.bill.BillType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BillTypeDao extends PagingAndSortingRepository<BillType, String> {
}
