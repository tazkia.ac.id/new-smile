package id.ac.tazkia.newsmile.dao.employee;

import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployesDao extends CrudRepository<Employes, String>, PagingAndSortingRepository<Employes, String> {
    Employes findByUser(User user);

    @Query(value = "SELECT e.* FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
            "where ei.id_institution in (?1) and ei.status = 'ACTIVE' and e.status = 'ACTIVE' and (e.full_name like %?2%) \n" +
            "group by ei.id_employee order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
                    "where ei.id_institution in (?1) and ei.status = 'ACTIVE' and e.status = 'ACTIVE' and (e.full_name like %?2%) \n" +
                    "group by ei.id_employee order by e.full_name asc",
            nativeQuery = true)
    Page<Employes> pageSearchInstitution(List<String> institutionList, String search, Pageable pageable);

    @Query(value = "SELECT e.* FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
            "where ei.id_institution in (?1) and ei.status = 'INACTIVE' and e.status = 'DELETED' and (e.full_name like %?2%) \n" +
            "group by ei.id_employee order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
                    "where ei.id_institution in (?1) and ei.status = 'INACTIVE' and e.status = 'DELETED' and (e.full_name like %?2%) \n" +
                    "group by ei.id_employee order by e.full_name asc",
            nativeQuery = true)
    Page<Employes> pageSearchInstitutionDeleted(List<String> institutionList, String search, Pageable pageable);

    @Query(value = "SELECT e.* FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
            "where ei.status = 'ACTIVE' and e.status = 'ACTIVE' and (e.full_name like %?1%) \n" +
            "group by ei.id_employee order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
                    "where ei.status = 'ACTIVE' and e.status = 'ACTIVE' and (e.full_name like %?1%) \n" +
                    "group by ei.id_employee order by e.full_name asc",
            nativeQuery = true)
    Page<Employes> pageSearch(String search, Pageable pageable);

    @Query(value = "SELECT e.* FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
            "where i.id_foundation = ?1 and ei.status = 'ACTIVE' and e.status = 'ACTIVE' and (e.full_name like %?2%) \n" +
            "group by ei.id_employee order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
                    "where i.id_foundation = ?1 and ei.status = 'ACTIVE' and e.status = 'ACTIVE' and (e.full_name like %?2%) \n" +
                    "group by ei.id_employee order by e.full_name asc",
            nativeQuery = true)
    Page<Employes> pageSearchFoundation(String foundation, String search, Pageable pageable);

    @Query(value = "SELECT e.* FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
            "where i.id_foundation = ?1 and ei.status = 'INACTIVE' and e.status = 'DELETED' and (e.full_name like %?2%) \n" +
            "group by ei.id_employee order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_institution as ei inner join employes as e on ei.id_employee = e.id inner join institution as i on ei.id_institution = i.id\n" +
                    "where i.id_foundation = ?1 and ei.status = 'INACTIVE' and e.status = 'DELETED' and (e.full_name like %?2%) \n" +
                    "group by ei.id_employee order by e.full_name asc",
            nativeQuery = true)
    Page<Employes> pageSearchFoundationDeleted(String foundation, String search, Pageable pageable);

    @Query(value = "SELECT e.* FROM employes e LEFT JOIN employee_lecturer el ON e.id = el.id_employee LEFT JOIN employee_institution ei ON e.id = ei.id_employee\n" +
            "WHERE e.status = 'ACTIVE' AND ei.id_institution in (?1) AND ei.status = 'ACTIVE' AND (el.id IS NULL OR el.status IN ('DELETED', 'INACTIVE') or e.id = ?2) and e.full_name like %?3% group by e.id order by e.full_name",
            countQuery = "SELECT count(*) FROM employes e LEFT JOIN employee_lecturer el ON e.id = el.id_employee LEFT JOIN employee_institution ei ON e.id = ei.id_employee\n" +
                    "WHERE e.status = 'ACTIVE' AND ei.id_institution in ('01') AND ei.status = 'ACTIVE' AND (el.id IS NULL OR el.status IN ('DELETED', 'INACTIVE') or e.id = ?2) and e.full_name like %?3% group by e.id order by e.full_name",
            nativeQuery = true)
    Page<Employes> getEmployeeForLecturer(List<String> institutionId,String id,String search ,Pageable pageable);

    @Query(value = "SELECT e.* FROM employes e LEFT JOIN employee_lecturer el ON e.id = el.id_employee LEFT JOIN employee_institution ei ON e.id = ei.id_employee\n" +
            "WHERE e.status = 'ACTIVE' AND ei.id_institution in (?1) AND ei.status = 'ACTIVE' AND (el.id IS NULL OR el.status IN ('DELETED', 'INACTIVE')) and e.full_name like %?2% group by e.id order by e.full_name",
            countQuery = "SELECT count(*) FROM employes e LEFT JOIN employee_lecturer el ON e.id = el.id_employee LEFT JOIN employee_institution ei ON e.id = ei.id_employee\n" +
                    "WHERE e.status = 'ACTIVE' AND ei.id_institution in ('01') AND ei.status = 'ACTIVE' AND (el.id IS NULL OR el.status IN ('DELETED', 'INACTIVE')) and e.full_name like %?2% group by e.id order by e.full_name",
            nativeQuery = true)
    Page<Employes> getLecturer(List<String> institutionId,String search ,Pageable pageable);

    @Query(value = "select e.* from employes as e left join employee_staff as es on e.id = es.id_employee inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where e.status = 'ACTIVE' and ei.id_institution in (?1) and (es.id is null or es.status IN ('DELETED', 'INACTIVE')) and e.full_name like %?2% group by e.id order by e.full_name asc",
            countQuery = "select count(*) from employes as e left join employee_staff as es on e.id = es.id_employee inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where e.status = 'ACTIVE' and ei.id_institution in (?1) and (es.id is null or es.status IN ('DELETED', 'INACTIVE')) and e.full_name like %?2% group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<Employes> getStaff(List<String> institutionId,String search ,Pageable pageable);

    @Query(value = "select e.* from employes as e left join employee_staff as es on e.id = es.id_employee inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where e.status = 'ACTIVE' and ei.id_institution in (?1) and (es.id is null or es.status IN ('DELETED', 'INACTIVE') or e.id = ?3) and e.full_name like %?2% group by e.id order by e.full_name asc",
            countQuery = "select count(*) from employes as e left join employee_staff as es on e.id = es.id_employee inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where e.status = 'ACTIVE' and ei.id_institution in (?1) and (es.id is null or es.status IN ('DELETED', 'INACTIVE') or e.id = ?3) and e.full_name like %?2% group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<Employes> getStaffEdit(List<String> institutionId,String search, String id,Pageable pageable);

}
