package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.entity.JobPosition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JobPositionDao extends CrudRepository<JobPosition, String>, PagingAndSortingRepository<JobPosition, String> {
    @Query(value = "SELECT jp.* FROM job_position as jp inner join department as d on jp.id_department = d.id where jp.status = 'DELETED' and id_institution in (?1) and jp.position_name like %?2%",
            countQuery = "SELECT count(*) FROM job_position as jp inner join department as d on jp.id_department = d.id where jp.status = 'DELETED' and id_institution in (?1) and jp.position_name like %?2%",
            nativeQuery = true)
    Page<JobPosition> pageSearchInstitutionDeleted(List<String> institutionId, String search, Pageable pageable);

    @Query(value = "SELECT jp.* FROM job_position as jp inner join department as d on jp.id_department = d.id where jp.status = 'ACTIVE' and id_institution in (?1) and jp.position_name like %?2%",
            countQuery = "SELECT count(*) FROM job_position as jp inner join department as d on jp.id_department = d.id where jp.status = 'ACTIVE' and id_institution in (?1) and jp.position_name like %?2%",
            nativeQuery = true)
    Page<JobPosition> pageSearchInstitution(List<String> institutionId, String search, Pageable pageable);

    @Query(value = "SELECT jp.* FROM job_position as jp inner join department as d on jp.id_department = d.id inner join institution as i on d.id_institution = i.id where jp.status = 'ACTIVE' and i.id_foundation = ?1 and jp.position_name like %?2% ",
            countQuery = "SELECT (*) FROM job_position as jp inner join department as d on jp.id_department = d.id inner join institution as i on d.id_institution = i.id where jp.status = 'ACTIVE' and i.id_foundation = ?1 and jp.position_name like %?2% ",
            nativeQuery = true)
    Page<JobPosition> pageSearchFoundation(String foundationId, String search, Pageable pageable);

    @Query(value = "SELECT jp.* FROM job_position as jp inner join department as d on jp.id_department = d.id inner join institution as i on d.id_institution = i.id where jp.status = 'DELETED' and i.id_foundation = ?1 and jp.position_name like %?2% ",
            countQuery = "SELECT (*) FROM job_position as jp inner join department as d on jp.id_department = d.id inner join institution as i on d.id_institution = i.id where jp.status = 'DELETED' and i.id_foundation = ?1 and jp.position_name like %?2% ",
            nativeQuery = true)
    Page<JobPosition> pageSearchFoundationDeleted(String foundationId, String search, Pageable pageable);
}
