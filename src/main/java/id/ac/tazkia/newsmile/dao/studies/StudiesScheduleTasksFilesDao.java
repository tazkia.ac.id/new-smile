package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesScheduleTasksFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleTasksFilesDao extends CrudRepository<StudiesScheduleTasksFiles, String>,
        PagingAndSortingRepository<StudiesScheduleTasksFiles, String> {
}
