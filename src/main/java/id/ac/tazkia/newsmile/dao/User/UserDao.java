package id.ac.tazkia.newsmile.dao.User;

import id.ac.tazkia.newsmile.entity.config.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserDao extends CrudRepository<User, String>, PagingAndSortingRepository<User,String> {
    User findByUserName(String Username);

    Optional<User> findByFoundationId(String foundationId);
}
