package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentBill;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentBillDao extends CrudRepository<StudentBill, String>, PagingAndSortingRepository<StudentBill, String> {
}
