package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesAttendanceStudent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesAttendanceStudentDao extends CrudRepository<StudiesAttendanceStudent, String>,
        PagingAndSortingRepository<StudiesAttendanceStudent, String> {
}
