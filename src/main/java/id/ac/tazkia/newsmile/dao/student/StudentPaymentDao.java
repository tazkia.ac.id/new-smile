package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentPayment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentPaymentDao extends CrudRepository<StudentPayment, String>, PagingAndSortingRepository<StudentPayment, String> {
}
