package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesScheduleTasksAnswer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleTasksAnswerDao extends CrudRepository<StudiesScheduleTasksAnswer, String>,
        PagingAndSortingRepository<StudiesScheduleTasksAnswer, String> {
}
