package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.student.Student;
import id.ac.tazkia.newsmile.entity.student.StudentStatusAcademic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StudentStatusAcademicDao extends CrudRepository<StudentStatusAcademic, String>, PagingAndSortingRepository<StudentStatusAcademic, String> {
    StudentStatusAcademic findFirstByStudentAndStatusOrderBySemesterDesc(Student student, StatusRecord status);
    List<StudentStatusAcademic> findByStudentAndStatusOrderBySemesterAsc(Student student, StatusRecord statusRecord);
}
