package id.ac.tazkia.newsmile.dao.User;

import id.ac.tazkia.newsmile.entity.config.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PermissionDao extends CrudRepository<Permission, String>, PagingAndSortingRepository<Permission, String> {
    @Query(value = "SELECT p.* FROM s_role_permission as rp inner join s_permission as p on rp.id_permission = p.id inner join s_role as r on rp.id_role = r.id where rp.id_role = ?1 and p.permission_label like %?2%",
            countQuery = "SELECT count(*) FROM s_role_permission as rp inner join s_permission as p on rp.id_permission = p.id inner join s_role as r on rp.id_role = r.id where rp.id_role = ?1 and p.permission_label like %?2%", nativeQuery = true)
    Page<Permission> listPermission(String role, String search, Pageable pageable);

    @Query(value = "delete from s_role_permission where id_role = ?1 and id_permission = ?2", nativeQuery = true)
    @Modifying
    void deletePermissionByRoleAndPermission(String role, String permission);

}
