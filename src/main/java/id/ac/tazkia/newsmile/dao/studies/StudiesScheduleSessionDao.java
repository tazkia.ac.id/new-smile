package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesScheduleSession;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleSessionDao extends CrudRepository<StudiesScheduleSession, String>,
        PagingAndSortingRepository<StudiesScheduleSession, String> {
}
