package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesScheduleTasksFileType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleTasksFileTypeDao extends CrudRepository<StudiesScheduleTasksFileType, String>,
        PagingAndSortingRepository<StudiesScheduleTasksFileType, String> {
}
