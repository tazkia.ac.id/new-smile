package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Foundation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface FoundationDao extends CrudRepository<Foundation, String>, PagingAndSortingRepository<Foundation, String> {
    Page<Foundation> findByFoundationNameContainingIgnoreCaseOrderByFoundationName(String search, Pageable pageable);
    List<Foundation> findByStatus(StatusRecord statusRecord);
}
