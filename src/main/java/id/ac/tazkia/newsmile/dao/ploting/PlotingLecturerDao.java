package id.ac.tazkia.newsmile.dao.ploting;

import id.ac.tazkia.newsmile.entity.ploting.PlotingLecturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PlotingLecturerDao extends CrudRepository<PlotingLecturer, String>, PagingAndSortingRepository<PlotingLecturer, String> {
}
