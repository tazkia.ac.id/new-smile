package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentPaymentPlan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentPaymentPlanDao extends CrudRepository<StudentPaymentPlan, String>, PagingAndSortingRepository<StudentPaymentPlan, String> {
}
