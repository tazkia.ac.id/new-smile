package id.ac.tazkia.newsmile.dao.curriculum;

import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CuriculumSubjectDao extends CrudRepository<CuriculumSubject, String>, PagingAndSortingRepository<CuriculumSubject, String> {
    @Query(value = "select cs.* from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
            "where cs.id_prodi = ?1 and cs.id_curiculum = ?2 and (s.subject_name like %?3% or s.subject_name_english like %?3%) and cs.semester in (?4) order by cs.semester,s.subject_name asc",
            countQuery = "select count(cs.*) from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
                    "where cs.id_prodi = ?1 and cs.id_curiculum = ?2 and (s.subject_name like %?3% or s.subject_name_english like %?3%) and cs.semester in (?4) order by cs.semester,s.subject_name asc",
            nativeQuery = true)
    Page<CuriculumSubject> pageSearchProdiCurriculum(String prodi, String curriculum, String search, List<Integer> semester, Pageable pageable);

    @Query(value = "select cs.* from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
            "where cs.id_prodi = ?1  and (s.subject_name like %?2% or s.subject_name_english like %?2%) and cs.semester in (?3) order by cs.semester,s.subject_name asc",
            countQuery = "select count(cs.*) from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
                    "where cs.id_prodi = ?1 and (s.subject_name like %?2% or s.subject_name_english like %?2%) and cs.semester in (?3) order by cs.semester,s.subject_name asc",
            nativeQuery = true)
    Page<CuriculumSubject> pageSearchProdi(String prodi, String search,List<Integer> semester ,Pageable pageable);

    @Query(value = "select cs.cs.* from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
            "where (s.subject_name like %?1% or s.subject_name_english like %?1%) and cs.semester in (?2) order by cs.semester,s.subject_name asc",
            countQuery = "select count(cs.*) from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
                    "where (s.subject_name like %?1% or s.subject_name_english like %?1%) and cs.semester in (?2) order by cs.semester,s.subject_name asc",
            nativeQuery = true)
    Page<CuriculumSubject> pageSearch(String search,List<Integer> semester,Pageable pageable);

    @Query(value = "select cs.* from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
            "where cs.id_curiculum = ?1  and (s.subject_name like %?2% or s.subject_name_english like %?2%) and cs.semester in (?3) order by cs.semester,s.subject_name asc",
            countQuery = "select count(cs.*) from curiculum_subject as cs inner join curiculum as c on cs.id_curiculum = c.id inner join subject as s on cs.id_subject = s.id " +
                    "where cs.id_curiculum = ?1 and (s.subject_name like %?2% or s.subject_name_english like %?2%) and cs.semester in (?3) order by cs.semester,s.subject_name asc",
            nativeQuery = true)
    Page<CuriculumSubject> pageSearchCurriculum(String search, String id,List<Integer> semester, Pageable pageable);
}
