package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentInvoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentInvoiceDao extends PagingAndSortingRepository<StudentInvoice, String>, CrudRepository<StudentInvoice, String> {
}
