package id.ac.tazkia.newsmile.dao.building;

import id.ac.tazkia.newsmile.entity.building.BuildingFloor;
import id.ac.tazkia.newsmile.entity.building.BuildingRoom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BuildingRoomDao extends CrudRepository<BuildingRoom, String>, PagingAndSortingRepository<BuildingRoom, String> {
    @Query(value = "select br.* from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
            "where i.id_foundation = ?1 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?2% or br.room_name like %?2%) order by br.room_name asc",
            countQuery = "select * from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id \" +\n" +
                    "\"where i.id_foundation = ?1 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?2% or br.room_name like %?2%)", nativeQuery = true)
    Page<BuildingRoom> pageSearchFoundation(String foundation, String search, Pageable pageable);

    @Query(value = "select br.* from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
            "where br.id_floor = ?1 and i.id_foundation = ?2 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?3% or br.room_name like %?3%) order by br.room_name asc",
            countQuery = "select * from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id" +
                    " where br.id_floor = ?1 and i.id_foundation = ?2 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?3% or br.room_name like %?3%);", nativeQuery = true)
    Page<BuildingRoom> pageSearchFoundationFloor(String floor,  String foundation, String search, Pageable pageable);

    @Query(value = "select br.* from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id" +
            " where b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?1% or br.room_name like %?1%) order by br.room_name asc",
            countQuery = "select * from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
                    "where b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?1% or br.room_name like %?1%)", nativeQuery = true)
    Page<BuildingRoom> pageSearch(String search, Pageable pageable);

    @Query(value = "select br.* from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id" +
            " where b.status in ('ACTIVE','INACTIVE') and bf.id = ?2 and (bf.floor like %?1% or br.room_name like %?1%) order by br.room_name asc",
            countQuery = "select * from building_room as br inner join building_floor as bf on br.id_floor = bf.id inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
                    "where b.status in ('ACTIVE','INACTIVE') and bf.id = ?2 and (bf.floor like %?1% or br.room_name like %?1%)", nativeQuery = true)
    Page<BuildingRoom> pageSearchFloor(String search,String floor, Pageable pageable);
}
