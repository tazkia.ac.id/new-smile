package id.ac.tazkia.newsmile.dao.period;

import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PeriodAcademicProdiDao extends CrudRepository<PeriodAcademicProdi, String>
        ,PagingAndSortingRepository<PeriodAcademicProdi, String> {
}
