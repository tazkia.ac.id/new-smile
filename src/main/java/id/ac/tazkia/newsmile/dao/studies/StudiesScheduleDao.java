package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesSchedule;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleDao extends CrudRepository<StudiesSchedule, String>,
        PagingAndSortingRepository<StudiesSchedule, String> {
}
