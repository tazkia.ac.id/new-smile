package id.ac.tazkia.newsmile.dao.department;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.department.Department;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DepartmentDao extends CrudRepository<Department, String>, PagingAndSortingRepository<Department, String> {
    @Query(value = "select d.* from department as d inner join institution as i on d.id_institution = i.id where d.status in ('ACTIVE','INACTIVE') and i.id_foundation = ?1 and (d.department_name like %?2% and i.institution_name like %?2%) order by d.department_name asc",
            countQuery = "select d.* from department as d inner join institution as i on d.id_institution = i.id where d.status in ('ACTIVE','INACTIVE') and i.id_foundation = ?1 and (d.department_name like %?2% and i.institution_name like %?2%) order by d.department_name asc",
            nativeQuery = true)
    Page<Department> pageSearchFoundation(String foundation, String search, Pageable pageable);

    @Query(value = "select d.* from department as d inner join institution as i on d.id_institution = i.id where d.status in ('ACTIVE','INACTIVE') and (d.department_name like %?1% and i.institution_name like %?1%) order by d.department_name asc",
            countQuery = "select d.* from department as d inner join institution as i on d.id_institution = i.id where d.status in ('ACTIVE','INACTIVE') and (d.department_name like %?1% and i.institution_name like %?1%) order by d.department_name asc",
            nativeQuery = true)
    Page<Department> pageSearch(String search, Pageable pageable);

    @Query(value = "SELECT d.* FROM department as d left join institution_faculty as f on d.id = f.id_department left join institution_prodi as p on d.id = p.id_department" +
            " where d.status = 'ACTIVE' and f.id is null and p.id is null order by d.department_name asc", nativeQuery = true)
    List<Department> departmentNotInFaculty();

    @Query(value = "SELECT d.* FROM department as d left join institution_faculty as f on d.id = f.id_department inner join institution as i on d.id_institution = i.id left join institution_prodi as p on d.id = p.id_department" +
            " where d.status = 'ACTIVE' and f.id is null and p.id is null and i.id_foundation = ?1 order by d.department_name asc", nativeQuery = true)
    List<Department> departmentFoundationNotInFaculty(String foundation);

    List<Department> findByStatusOrderByDepartmentNameAsc(StatusRecord statusRecord);
    List<Department> findByStatusAndInstitutionFoundationOrderByDepartmentNameAsc(StatusRecord statusRecord, Foundation foundation);

    List<Department> findByStatusAndInstitutionOrderByDepartmentNameAsc(StatusRecord statusRecord, Institution institution);
}
