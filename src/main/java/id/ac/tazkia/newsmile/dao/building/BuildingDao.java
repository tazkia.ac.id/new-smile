package id.ac.tazkia.newsmile.dao.building;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.building.Building;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BuildingDao extends PagingAndSortingRepository<Building, String>, CrudRepository<Building, String> {
    @Query(value = "SELECT b.* FROM building as b inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id" +
            " where i.id_foundation = ?1 and b.status in ('ACTIVE','INACTIVE') and (b.building_name like %?2% or c.campus_name like %?2%) order by b.building_name asc",
            countQuery = "SELECT count(*) FROM building as b inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id where i.id_foundation = ?1 and b.status in ('ACTIVE','INACTIVE') and (b.building_name like %?2% or c.campus_name like %?2%)",
            nativeQuery = true)
    Page<Building> pageSearchFoundation(String foundation, String search, Pageable pageable);

    @Query(value = "SELECT b.* FROM building as b inner join campus as c on b.id_campus = c.id " +
            "where b.status in ('ACTIVE','INACTIVE') and (b.building_name like %?1% or c.campus_name like %?1%) order by b.building_name asc",
            countQuery = "SELECT count(*) FROM building as b inner join campus as c on b.id_campus = c.id  where b.status in ('ACTIVE','INACTIVE') and (b.building_name like %?2% or c.campus_name like %?2%)",
            nativeQuery = true)
    Page<Building> pageSearch(String search, Pageable pageable);

    List<Building> findByStatusOrderByBuildingNameAsc(StatusRecord statusRecord);
    List<Building> findByStatusAndInstitutionFoundationOrderByBuildingNameAsc(StatusRecord statusRecord, Foundation foundation);
}
