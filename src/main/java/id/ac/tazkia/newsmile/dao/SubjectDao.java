package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Subject;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.stream.Stream;

public interface SubjectDao extends CrudRepository<Subject, String>, PagingAndSortingRepository<Subject, String> {
    @Query(value ="SELECT * FROM subject where id_prodi in (?1) and (code like %?2% or subject_name like %?2% or id = ?2) and status = 'ACTIVE' order by subject_name asc",
            countQuery = "SELECT count(*) FROM subject where id_prodi in (?1) and (code like %?2% or subject_name like %?2% or id = ?2) and status = 'ACTIVE' order by subject_name asc",
            nativeQuery = true)
    Page<Subject> pageSearchProdi(List<String> prodis, String search, Pageable pageable);

    @Query(value ="SELECT * FROM subject where id_prodi in (?1) and (code like %?2% or subject_name like %?2%) and status = 'DELETED' order by subject_name asc",
            countQuery = "SELECT count(*) FROM subject where id_prodi in (?1) and (code like %?2% or subject_name like %?2%) and status = 'DELETED' order by subject_name asc",
            nativeQuery = true)
    Page<Subject> pageSearchProdiDeleted(List<String> prodis, String search, Pageable pageable);

    @Query(value ="SELECT * FROM subject where (code like %?1% or subject_name like %?1%) and status = 'ACTIVE' order by subject_name asc",
            countQuery = "SELECT count(*) FROM subject where (code like %?1% or subject_name like %?1%) and status = 'ACTIVE' order by subject_name asc",
            nativeQuery = true)
    Page<Subject> pageSearch(String search, Pageable pageable);

    @Query(value ="SELECT * FROM subject where (code like %?1% or subject_name like %?1%) and status = 'DELETED' order by subject_name asc",
            countQuery = "SELECT count(*) FROM subject where (code like %?1% or subject_name like %?1%) and status = 'DELETED' order by subject_name asc",
            nativeQuery = true)
    Page<Subject> pageSearchDeleted(String search, Pageable pageable);

    Page<Subject> findByStatusAndInstitutionProdiOrderBySubjectNameAsc(StatusRecord statusRecord, InstitutionProdi institutionProdi, Pageable pageable);
}
