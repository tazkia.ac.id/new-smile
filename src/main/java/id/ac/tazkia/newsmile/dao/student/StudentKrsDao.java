package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentKrs;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentKrsDao extends CrudRepository<StudentKrs, String>, PagingAndSortingRepository<StudentKrs, String> {
}
