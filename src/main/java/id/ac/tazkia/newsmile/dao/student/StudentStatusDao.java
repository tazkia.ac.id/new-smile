package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentStatusDao extends CrudRepository<StudentStatus, String>, PagingAndSortingRepository<StudentStatus, String> {
}
