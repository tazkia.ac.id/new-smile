package id.ac.tazkia.newsmile.dao.bill;

import id.ac.tazkia.newsmile.entity.bill.BillValue;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BillValueDao extends CrudRepository<BillValue, String>, PagingAndSortingRepository<BillValue, String> {
}
