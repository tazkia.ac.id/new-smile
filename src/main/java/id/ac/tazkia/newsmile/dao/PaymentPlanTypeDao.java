package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.entity.PaymentPlanType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PaymentPlanTypeDao extends CrudRepository<PaymentPlanType, String>, PagingAndSortingRepository<PaymentPlanType, String> {
}
