package id.ac.tazkia.newsmile.dao.institution;

import id.ac.tazkia.newsmile.entity.institution.InstitutionProgram;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface InstitutionProgramDao extends CrudRepository<InstitutionProgram, String>, PagingAndSortingRepository<InstitutionProgram, String> {
}
