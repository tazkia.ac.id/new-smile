package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.student.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentDao extends CrudRepository<Student, String>, PagingAndSortingRepository<Student, String> {
    Student findByUser(User user);
}
