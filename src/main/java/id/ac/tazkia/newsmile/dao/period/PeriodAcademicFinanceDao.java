package id.ac.tazkia.newsmile.dao.period;

import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PeriodAcademicFinanceDao extends CrudRepository<PeriodAcademicFinance, String>,
        PagingAndSortingRepository<PeriodAcademicFinance, String> {
}
