package id.ac.tazkia.newsmile.dao.institution;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.institution.InstitutionFaculty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstitutionFacultyDao extends CrudRepository<InstitutionFaculty, String>, PagingAndSortingRepository<InstitutionFaculty, String> {
    @Query(value = "SELECT f.* FROM institution_faculty as f inner join institution as i on f.id_institution = i.id inner join department as d on f.id_department = d.id" +
            " where f.status in ('ACTIVE', 'INACTIVE') and i.id_foundation = ?1 and (f.faculty_name like %?2% or d.department_name like %?2%) order by f.faculty_name asc",
            countQuery = "SELECT f.* FROM institution_faculty as f inner join institution as i on f.id_institution = i.id inner join department as d on f.id_department = d.id" +
                    " where f.status in ('ACTIVE', 'INACTIVE') and i.id_foundation = ?1 and (f.faculty_name like %?2% or d.department_name like %?2%)  order by f.faculty_name asc",
            nativeQuery = true)
    Page<InstitutionFaculty> pageSearchFoundation(String foundation, String search, Pageable pageable);

    @Query(value = "SELECT f.* FROM institution_faculty as f inner join institution as i on f.id_institution = i.id inner join department as d on f.id_department = d.id " +
            "where f.status in ('ACTIVE', 'INACTIVE') and (f.faculty_name like %?1% or d.department_name like %?1%) order by f.faculty_name asc",
            countQuery = "SELECT f.* FROM institution_faculty as f inner join institution as i on f.id_institution = i.id inner join department as d on f.id_department = d.id" +
                    " where f.status in ('ACTIVE', 'INACTIVE') and (f.faculty_name like %?1% or d.department_name like %?1%) order by f.faculty_name asc;",
            nativeQuery = true)
    Page<InstitutionFaculty> pageSearch(String search, Pageable pageable);

    List<InstitutionFaculty> findByStatusOrderByFacultyNameAsc(StatusRecord statusRecord);

    List<InstitutionFaculty> findByStatusAndInstitutionFoundationOrderByFacultyNameAsc(StatusRecord statusRecord, Foundation foundation);
}
