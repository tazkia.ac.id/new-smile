package id.ac.tazkia.newsmile.dao.period;

import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PeriodAcademicDao extends CrudRepository<PeriodAcademic, String>, PagingAndSortingRepository<PeriodAcademic, String> {
}
