package id.ac.tazkia.newsmile.dao.curriculum;

import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubjectRps;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CuriculumSubjectRpsDao extends CrudRepository<CuriculumSubjectRps, String>, PagingAndSortingRepository<CuriculumSubjectRps, String> {
}
