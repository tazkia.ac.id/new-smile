package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.entity.Events;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.time.LocalDate;
import java.util.List;

public interface EventsDao extends PagingAndSortingRepository<Events, String>, CrudRepository<Events, String> {
    @Query(value = "SELECT *\n" +
            "FROM events\n" +
            "WHERE (start_date >= ?1 AND start_date <= ?2 and status = 'ACTIVE')\n" +
            "   OR (end_date >= ?1 AND end_date <= ?2 and status = 'ACTIVE')\n" +
            "   OR (start_date <= ?1 AND end_date >= ?2 and status = 'ACTIVE')", nativeQuery = true)
    List<Events> getEventsCalendar(LocalDate start, LocalDate end);
}
