package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesAttendanceLecturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesAttendanceLecturerDao extends CrudRepository<StudiesAttendanceLecturer, String>,
        PagingAndSortingRepository<StudiesAttendanceLecturer, String> {
}
