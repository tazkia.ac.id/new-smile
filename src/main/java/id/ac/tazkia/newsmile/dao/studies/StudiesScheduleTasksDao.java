package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesScheduleTasks;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleTasksDao extends CrudRepository<StudiesScheduleTasks, String>, PagingAndSortingRepository<StudiesScheduleTasks, String> {
}
