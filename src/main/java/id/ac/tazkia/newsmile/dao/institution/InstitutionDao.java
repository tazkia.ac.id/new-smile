package id.ac.tazkia.newsmile.dao.institution;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstitutionDao extends CrudRepository<Institution, String>, PagingAndSortingRepository<Institution, String> {
    @Query(value = "SELECT i.* FROM employee_institution as ei inner join institution as i on ei.id_institution = i.id where ei.id_employee = ?1 and ei.status = 'ACTIVE' order by i.institution_name asc",
            nativeQuery = true)
    List<Institution> getEmployeeInstitution(String employee);

    Page<Institution> findByInstitutionNameContainingIgnoreCaseOrderByInstitutionName(String name, Pageable pageable);

    Page<Institution> findByInstitutionNameContainingIgnoreCaseAndFoundationOrderByInstitutionName(String name,Foundation foundation, Pageable pageable);

    List<Institution> findByStatus(StatusRecord statusRecord);

    List<Institution> findByStatusAndFoundation(StatusRecord statusRecord, Foundation foundation);
}
