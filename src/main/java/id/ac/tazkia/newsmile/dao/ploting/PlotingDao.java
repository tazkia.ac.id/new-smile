package id.ac.tazkia.newsmile.dao.ploting;

import id.ac.tazkia.newsmile.entity.ploting.Ploting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PlotingDao extends CrudRepository<Ploting, String>, PagingAndSortingRepository<Ploting, String> {
}
