package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.entity.ReRegistration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ReRegistrationDao extends CrudRepository<ReRegistration, String>, PagingAndSortingRepository<ReRegistration, String> {
}
