package id.ac.tazkia.newsmile.dao.institution;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface InstitutionProdiDao extends CrudRepository<InstitutionProdi, String>, PagingAndSortingRepository<InstitutionProdi, String> {
    @Query(value = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id" +
            " where i.id_foundation = ?1 and p.id_faculty = ?2 and (p.prodi_name like %?3% or f.faculty_name like %?3%) order by p.prodi_name asc",
            countQuery = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id " +
                    "where i.id_foundation = ?1 and p.id_faculty = ?2 and (p.prodi_name like %?3% or f.faculty_name like %?3%) order by p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionProdi> pageSearchFoundationFaculty(String foundation, String faculty, String search, Pageable pageable);

    @Query(value = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id" +
            " where i.id_foundation = ?1 and (p.prodi_name like %?2% or f.faculty_name like %?2%) order by p.prodi_name asc",
            countQuery = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id inner join department as d on p.id_department = d.id inner join institution as i on d.id_institution = i.id" +
                    " where i.id_foundation = ?1 and (p.prodi_name like %?2% or f.faculty_name like %?2%) order by p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionProdi> pageSearchFoundation(String foundation, String seach, Pageable pageable);

    @Query(value = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id where (p.prodi_name like %?1% or f.faculty_name like %?1%) order by p.prodi_name asc",
            countQuery = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id where (p.prodi_name like %?1% or f.faculty_name like %?1%) order by p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionProdi> pageSearch(String search, Pageable pageable);

    @Query(value = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id where p.id_faculty = ?1 and (p.prodi_name like %?2% or f.faculty_name like %?2%) order by p.prodi_name asc",
            countQuery = "SELECT p.* FROM institution_prodi as p inner join institution_faculty as f on p.id_faculty = f.id where p.id_faculty = ?1 and (p.prodi_name like %?2% or f.faculty_name like %?2%) order by p.prodi_name asc",
            nativeQuery = true)
    Page<InstitutionProdi> pageSearchFaculty(String faculty, String search, Pageable pageable);

    @Query(value = "SELECT p.* FROM employee_job_position as ej inner join job_position as jp on ej.id_job_position = jp.id inner join institution_prodi as p on jp.id_department = p.id_department where ej.status = 'ACTIVE' and jp.status = 'ACTIVE' and ej.id_employee = ?1", nativeQuery = true)
    List<InstitutionProdi> getSelectedProdi(String employeeId);

    @Query(value = "SELECT ip.* FROM institution_prodi as ip inner join department as d on ip.id_department = d.id where ip.status = 'ACTIVE' and d.status = 'ACTIVE' and d.id_institution in (?1)", nativeQuery = true)
    List<InstitutionProdi> getProdiIns(List<String> institutions);

    List<InstitutionProdi> findByStatusOrderByProdiNameAsc(StatusRecord statusRecord);
}
