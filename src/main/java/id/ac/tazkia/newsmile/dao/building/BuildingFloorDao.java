package id.ac.tazkia.newsmile.dao.building;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.building.Building;
import id.ac.tazkia.newsmile.entity.building.BuildingFloor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BuildingFloorDao extends CrudRepository<BuildingFloor, String>, PagingAndSortingRepository<BuildingFloor, String> {
    @Query(value = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
            "where i.id_foundation = ?1 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?2% or b.building_name like %?2%) order by bf.floor asc"  ,
            countQuery = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
                    "where i.id_foundation = ?1 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?2% or b.building_name like %?2%)", nativeQuery = true)
    Page<BuildingFloor> pageSearchFoundation(String foundation, String search, Pageable pageable);

    @Query(value = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
            "where b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?1% or b.building_name like %?1%) order by bf.floor asc"  ,
            countQuery = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
                    "where b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?1% or b.building_name like %?1%)", nativeQuery = true)
    Page<BuildingFloor> pageSearch(String search, Pageable pageable);

    @Query(value = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
            "where b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?1% or b.building_name like %?1%) and b.id = ?2 order by bf.floor asc"  ,
            countQuery = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
                    "where b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?1% or b.building_name like %?1%) and b.id = ?2 ", nativeQuery = true)
    Page<BuildingFloor> pageSearchBuilding(String search,String building, Pageable pageable);

    @Query(value = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
            "where i.id_foundation = ?1 and b.id = ?3 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?2% or b.building_name like %?2%) order by bf.floor asc"  ,
            countQuery = "select bf.* from building_floor as bf inner join building as b on bf.id_building = b.id inner join campus as c on b.id_campus = c.id inner join institution as i on b.id_institution = i.id " +
                    "where i.id_foundation = ?1 and b.id = ?3 and b.status in ('ACTIVE','INACTIVE') and (bf.floor like %?2% or b.building_name like %?2%)", nativeQuery = true)
    Page<BuildingFloor> pageSearchFoundationBuilding(String foundation, String search,String building ,Pageable pageable);

    List<BuildingFloor> findByStatusOrderByFloorAsc(StatusRecord statusRecord);

    List<BuildingFloor> findByStatusAndBuildingInstitutionFoundationOrderByFloorAsc(StatusRecord statusRecord, Foundation foundation);
}
