package id.ac.tazkia.newsmile.dao.employee;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.employee.EmployeeInstitution;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Arrays;
import java.util.List;

public interface EmployeeInstitutionDao extends CrudRepository<EmployeeInstitution, String>, PagingAndSortingRepository<EmployeeInstitution, String> {
    @Query(value = "update employee_institution set status = ?2 where id_employee = ?1 and status =?3", nativeQuery = true)
    @Modifying
    void setStatusEmployee(String employes, String status, String currentStatus);

    @Query(value = "update employee_institution set status = ?2 where id_employee = ?1 and id_institution not in (?3) and status ='ACTIVE'", nativeQuery = true)
    @Modifying
    void setStatusEmployeeIns(String employes, String status, List<String> institutionId);

    EmployeeInstitution findByEmployesAndInstitutionAndStatus(Employes employes, Institution institution, StatusRecord statusRecord);

    List<EmployeeInstitution> findByEmployesAndStatusOrderByInstitutionInstitutionNameAsc(Employes employes, StatusRecord statusRecord);
}
