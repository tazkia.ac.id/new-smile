package id.ac.tazkia.newsmile.dao.User;

import id.ac.tazkia.newsmile.entity.config.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends CrudRepository<Role, String>, PagingAndSortingRepository<Role,String> {
    Page<Role> findByNameContainingIgnoreCaseOrderByNameAsc(String search, Pageable pageable);
}
