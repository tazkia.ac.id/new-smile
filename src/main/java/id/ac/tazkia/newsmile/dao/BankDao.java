package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.entity.Bank;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BankDao extends CrudRepository<Bank, String>,
        PagingAndSortingRepository<Bank, String>, JpaSpecificationExecutor<Bank> {
}
