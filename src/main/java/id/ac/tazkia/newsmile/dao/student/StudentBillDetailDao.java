package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentBillDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentBillDetailDao extends PagingAndSortingRepository<StudentBillDetail, String>, CrudRepository<StudentBillDetail, String> {
}
