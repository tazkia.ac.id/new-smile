package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesSession;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesSessionDao extends PagingAndSortingRepository<StudiesSession, String>, CrudRepository<StudiesSession, String> {
}
