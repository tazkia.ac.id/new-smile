package id.ac.tazkia.newsmile.dao.employee;

import id.ac.tazkia.newsmile.entity.employee.EmployeeLecturer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeLecturerDao extends CrudRepository<EmployeeLecturer, String>, PagingAndSortingRepository<EmployeeLecturer, String> {
    @Query(value = "SELECT el.* FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where el.status = 'DELETED' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_institution in (?1) and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where el.status = 'DELETED' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_institution in (?1) and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<EmployeeLecturer> pageSearchInstitutionDeleted(List<String> institutionIds, String search, List<String> prodis, List<String> gender,List<String> type,  Pageable pageable);

    @Query(value = "SELECT el.* FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where el.status = 'ACTIVE' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_institution in (?1) and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where el.status = 'ACTIVE' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_institution in (?1) and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<EmployeeLecturer> pageSearchInstitution(List<String> institutionIds, String search, List<String> prodis, List<String> gender,List<String> type,  Pageable pageable);

    @Query(value = "SELECT el.* FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where el.status = 'ACTIVE' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_foundation in ?1 and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where el.status = 'ACTIVE' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_foundation = ?1 and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<EmployeeLecturer> pageSearchFoundation(String id,String search, List<String> prodis, List<String> gender,List<String> type,  Pageable pageable);

    @Query(value = "SELECT el.* FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where el.status = 'DELETED' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_foundation in ?1 and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            countQuery = "SELECT count(*) FROM employee_lecturer as el inner join employes as e on el.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where el.status = 'DELETED' and e.status = 'ACTIVE' and ei.status = 'ACTIVE' and el.id_prodi_home_base in (?3) and e.gender in (?4) and ei.id_foundation = ?1 and e.full_name like %?2% and el.lecturer_type in (?5) group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<EmployeeLecturer> pageSearchFoundationDeleted(String id,String search, List<String> prodis, List<String> gender,List<String> type,  Pageable pageable);;
}
