package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesScheduleTasksAnswerEvidance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleTasksAnswerEvidanceDao extends CrudRepository<StudiesScheduleTasksAnswerEvidance, String>,
        PagingAndSortingRepository<StudiesScheduleTasksAnswerEvidance, String> {
}
