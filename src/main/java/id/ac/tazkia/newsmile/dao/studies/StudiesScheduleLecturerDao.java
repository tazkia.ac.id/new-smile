package id.ac.tazkia.newsmile.dao.studies;

import id.ac.tazkia.newsmile.entity.studies.StudiesScheduleLecturer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudiesScheduleLecturerDao extends CrudRepository<StudiesScheduleLecturer, String>,
        PagingAndSortingRepository<StudiesScheduleLecturer, String> {
}
