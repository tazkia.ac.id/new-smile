package id.ac.tazkia.newsmile.dao.student;

import id.ac.tazkia.newsmile.entity.student.StudentVirtualAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StudentVirtualAccountDao extends CrudRepository<StudentVirtualAccount, String>, PagingAndSortingRepository<StudentVirtualAccount, String> {
}
