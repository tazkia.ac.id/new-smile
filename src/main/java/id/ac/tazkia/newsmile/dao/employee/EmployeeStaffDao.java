package id.ac.tazkia.newsmile.dao.employee;

import id.ac.tazkia.newsmile.entity.employee.EmployeeStaff;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeStaffDao extends CrudRepository<EmployeeStaff, String>, PagingAndSortingRepository<EmployeeStaff, String> {
    @Query(value = "SELECT es.* FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where es.status = 'DELETED' and e.status = 'ACTIVE' and ei.id_institution in (?1) and e.full_name like %?2% group by e.id order by e.full_name asc",
            countQuery = "SELECT count (*) FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where es.status = 'DELETED' and e.status = 'ACTIVE' and ei.id_institution in (?1) and e.full_name like %?2% group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<EmployeeStaff> pageSearchInstitutionDeleted(List<String> institutionIds, String search, Pageable pageable);

    @Query(value = "SELECT es.* FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
            "where es.status = 'ACTIVE' and e.status = 'ACTIVE' and ei.id_institution in (?1) and e.full_name like %?2% group by e.id order by e.full_name asc",
            countQuery = "SELECT count (*) FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee\n" +
                    "where es.status = 'ACTIVE' and e.status = 'ACTIVE' and ei.id_institution in (?1) and e.full_name like %?2% group by e.id order by e.full_name asc",
            nativeQuery = true)
    Page<EmployeeStaff> pageSearchInstitution(List<String> institutionIds, String search, Pageable pageable);

    @Query(value = "SELECT * FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee inner join institution as i on ei.id_institution = i.id\n" +
            "where es.status = 'ACTIVE' and e.status = 'ACTIVE' and i.id_foundation = ?1 and e.full_name like %?2% group by e.id order by e.full_name asc;",
            countQuery = "SELECT count (*) FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee inner join institution as i on ei.id_institution = i.id\n" +
                    "where es.status = 'ACTIVE' and e.status = 'ACTIVE' and i.id_foundation = ?1 and e.full_name like %?2% group by e.id order by e.full_name asc;",
            nativeQuery = true)
    Page<EmployeeStaff> pageSearchFoundation(String id, String search, Pageable pageable);

    @Query(value = "SELECT es.* FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee inner join institution as i on ei.id_institution = i.id\n" +
            "where es.status = 'DELETED' and e.status = 'ACTIVE' and i.id_foundation = ?1 and e.full_name like %?2% group by e.id order by e.full_name asc;",
            countQuery = "SELECT count (*) FROM employee_staff as es inner join employes as e on es.id_employee = e.id inner join employee_institution as ei on e.id = ei.id_employee inner join institution as i on ei.id_institution = i.id\n" +
                    "where es.status = 'DELETED' and e.status = 'ACTIVE' and i.id_foundation = ?1 and e.full_name like %?2% group by e.id order by e.full_name asc;",
            nativeQuery = true)
    Page<EmployeeStaff> pageSearchFoundationDeleted(String id, String search, Pageable pageable);
}
