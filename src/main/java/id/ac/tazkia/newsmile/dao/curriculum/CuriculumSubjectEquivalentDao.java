package id.ac.tazkia.newsmile.dao.curriculum;

import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubjectEquivalent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CuriculumSubjectEquivalentDao extends CrudRepository<CuriculumSubjectEquivalent, String>,
        PagingAndSortingRepository<CuriculumSubjectEquivalent, String> {
}
