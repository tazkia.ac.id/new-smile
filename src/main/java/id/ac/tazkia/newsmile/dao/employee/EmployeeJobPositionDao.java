package id.ac.tazkia.newsmile.dao.employee;

import id.ac.tazkia.newsmile.entity.JobPosition;
import id.ac.tazkia.newsmile.entity.employee.EmployeeJobPosition;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EmployeeJobPositionDao extends CrudRepository<EmployeeJobPosition, String>, PagingAndSortingRepository<EmployeeJobPosition, String> {
    @Query(value = "SELECT jp.* FROM employee_job_position as ej inner join job_position as jp on ej.id_job_position = jp.id and ej.status = 'ACTIVE' and jp.status = 'ACTIVE' and ej.id_employee = ?1", nativeQuery = true)
    List<JobPosition> getJobPositionEmployee(String employeeId);
}
