package id.ac.tazkia.newsmile.dao.department;

import id.ac.tazkia.newsmile.entity.department.DepartmentHead;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DepartmentHeadDao extends CrudRepository<DepartmentHead, String>, PagingAndSortingRepository<DepartmentHead, String> {
}
