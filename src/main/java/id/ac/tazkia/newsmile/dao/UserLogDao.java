package id.ac.tazkia.newsmile.dao;

import id.ac.tazkia.newsmile.entity.UserLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserLogDao extends JpaRepository<UserLog, String> {
}
