    package id.ac.tazkia.newsmile.interceptor;

    import id.ac.tazkia.newsmile.dto.filter.BaseRequestParamDto;
    import jakarta.servlet.http.HttpServletRequest;
    import jakarta.servlet.http.HttpServletResponse;
    import org.springframework.stereotype.Component;
    import org.springframework.web.servlet.HandlerInterceptor;
    import org.springframework.web.servlet.ModelAndView;
    import org.springframework.data.domain.PageRequest;
    import org.springframework.data.domain.Pageable;


    @Component
    public class RequestParamInterceptor implements HandlerInterceptor {

        @Override
        public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                               ModelAndView modelAndView) throws Exception {
            if (modelAndView != null) {
                BaseRequestParamDto params = new BaseRequestParamDto();
                params.setSearch(request.getParameter("search"));
                params.setPageSize(request.getParameter("pageSize") != null ? Integer.parseInt(request.getParameter("pageSize")) : 10);
                params.setPage(request.getParameter("page") != null ? Integer.parseInt(request.getParameter("page")) : 0);

                Pageable pageable = PageRequest.of(params.getPage(), params.getPageSize());

                modelAndView.addObject("params", params);
                modelAndView.addObject("pageable", pageable);
            }
        }
    }
