package id.ac.tazkia.newsmile.interceptor;

import id.ac.tazkia.newsmile.entity.config.Role;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class RoleInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        Role role = (Role) session.getAttribute("authority");

        // Cek jika user telah login tetapi belum memilih peran
        if (session.getAttribute("username") != null && (role == null)) {
            response.sendRedirect("/onboard/select-role");
            return false; // hentikan eksekusi lebih lanjut
        }

        return true; // lanjutkan ke handler berikutnya
    }
}
