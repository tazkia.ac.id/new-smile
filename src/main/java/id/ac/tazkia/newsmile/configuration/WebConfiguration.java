package id.ac.tazkia.newsmile.configuration;

import id.ac.tazkia.newsmile.configuration.resolver.ActiveMenuArgumentResolver;
import id.ac.tazkia.newsmile.interceptor.RequestParamInterceptor;
import id.ac.tazkia.newsmile.interceptor.RoleInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.PageableHandlerMethodArgumentResolverCustomizer;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final RoleInterceptor roleInterceptor;
    private final ActiveMenuArgumentResolver activeMenuArgumentResolver;
    private final RequestParamInterceptor requestParamInterceptor;
    private final PageableHandlerMethodArgumentResolverCustomizer pageableCustomizer;

    @Autowired
    public WebConfiguration(RoleInterceptor roleInterceptor,
                            ActiveMenuArgumentResolver activeMenuArgumentResolver,
                            RequestParamInterceptor requestParamInterceptor,
                            PageableHandlerMethodArgumentResolverCustomizer pageableCustomizer) {
        this.roleInterceptor = roleInterceptor;
        this.activeMenuArgumentResolver = activeMenuArgumentResolver;
        this.requestParamInterceptor = requestParamInterceptor;
        this.pageableCustomizer = pageableCustomizer;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(roleInterceptor)
                .excludePathPatterns("/dist/**", "/img/**", "/", "/onboard/select-role", "/onboard/setAuthority");
        registry.addInterceptor(requestParamInterceptor).addPathPatterns("/**");
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(activeMenuArgumentResolver);
    }
}
