package id.ac.tazkia.newsmile.configuration.resolver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.PageableHandlerMethodArgumentResolverCustomizer;

@Configuration
public class CustomPageableHandlerMethodArgumentResolver {

    @Bean
    public PageableHandlerMethodArgumentResolverCustomizer pageableCustomizer() {
        return new PageableHandlerMethodArgumentResolverCustomizer() {
            @Override
            public void customize(PageableHandlerMethodArgumentResolver resolver) {
                resolver.setFallbackPageable(PageRequest.of(0, 10));
                resolver.setMaxPageSize(100);  // Set max page size if needed
            }
        };
    }
}
