    package id.ac.tazkia.newsmile.configuration;

    import id.ac.tazkia.newsmile.dao.User.UserDao;
    import id.ac.tazkia.newsmile.entity.config.User;
    import jakarta.servlet.http.HttpSession;
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
    import org.springframework.security.config.annotation.web.builders.HttpSecurity;
    import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
    import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
    import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
    import org.springframework.security.web.SecurityFilterChain;

    @Configuration
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    @Slf4j
    public class SecurityConfiguration {

        @Autowired
        private UserDao userDao;

        @Bean
        public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
            http.authorizeHttpRequests(auth -> auth
                            .requestMatchers("/dist/**", "/img/**", "/login", "/public/**", "/").permitAll()
                            .anyRequest().authenticated()
                    ).logout().permitAll()
                    .and().oauth2Login()
                    .loginPage("/").permitAll()
                    .userInfoEndpoint().userAuthoritiesMapper(authoritiesMapper())
                    .and()
                    .successHandler((request, response, authentication) -> {
                        String emailAttrName = "email";
                        String email = authentication.getAuthorities().stream()
                                .filter(OAuth2UserAuthority.class::isInstance)
                                .map(OAuth2UserAuthority.class::cast)
                                .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
                                .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
                                .findFirst()
                                .orElse(null);

                        if (email != null) {
                            HttpSession session = request.getSession();
                            session.setAttribute("username", email);
                        }

                        response.sendRedirect("/onboard/select-role");
                    });
            return http.build();
        }

        private GrantedAuthoritiesMapper authoritiesMapper() {
            return (authorities) -> {
                String emailAttrName = "email";
                String email = authorities.stream()
                        .filter(OAuth2UserAuthority.class::isInstance)
                        .map(OAuth2UserAuthority.class::cast)
                        .filter(userAuthority -> userAuthority.getAttributes().containsKey(emailAttrName))
                        .map(userAuthority -> userAuthority.getAttributes().get(emailAttrName).toString())
                        .findFirst()
                        .orElse(null);

                if (email == null) {
                    return authorities;
                }

                User user = userDao.findByUserName(email);
                if (user == null) {
                    throw new IllegalStateException("Email " + email + " Not Registered");
                }

                return authorities;
            };
        }
    }
