package id.ac.tazkia.newsmile.configuration;

import jakarta.annotation.PostConstruct;
import org.springframework.context.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RepositoryFactory {

    private Repositories repositories;

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    public void init() {
        this.repositories = new Repositories(applicationContext);
    }

    public Optional<CrudRepository> getRepositoryFor(Class<?> entityClass) {
        return Optional.ofNullable((CrudRepository) repositories.getRepositoryFor(entityClass).orElse(null));
    }
}
