package id.ac.tazkia.newsmile.configuration.resolver;

import id.ac.tazkia.newsmile.utils.annotation.ActiveMenu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Component
@Slf4j
public class ActiveMenuArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        boolean supports = parameter.hasParameterAnnotation(ActiveMenu.class);
        log.debug("supportsParameter called: {}, supports: {}", parameter.getParameterName(), supports);
        return supports;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, org.springframework.web.bind.support.WebDataBinderFactory binderFactory) throws Exception {
        ActiveMenu activeMenu = parameter.getParameterAnnotation(ActiveMenu.class);
        if (activeMenu != null && mavContainer != null) {
            mavContainer.addAttribute("linkActive", activeMenu.link());
            mavContainer.addAttribute("dropdownActive", activeMenu.dropdown());
            if (!activeMenu.tab().isEmpty()) {
                log.debug("Setting tabActive to: {}", activeMenu.tab());
                mavContainer.addAttribute("tabActive", activeMenu.tab());
            }
        }
        return null;
    }
}