package id.ac.tazkia.newsmile.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.newsmile.dao.User.RoleDao;
import id.ac.tazkia.newsmile.dao.User.UserDao;
import id.ac.tazkia.newsmile.dao.UserLogDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionProdiDao;
import id.ac.tazkia.newsmile.dao.student.StudentDao;
import id.ac.tazkia.newsmile.dao.student.StudentStatusAcademicDao;
import id.ac.tazkia.newsmile.entity.config.Role;
import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.entity.student.Student;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
@Slf4j
public class UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private StudentStatusAcademicDao studentStatusAcademicDao;
    @Autowired
    private InstitutionProdiDao institutionProdiDao;
    @Autowired
    private UserLogDao userLogDao;
    @Autowired
    private ObjectMapper objectMapper;

    public User currentUser(Authentication currentUser) {
        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) currentUser;

        String username = (String) token.getPrincipal().getAttributes().get("email");
        User u = userDao.findByUserName(username);
        return u;
    }

    public HttpSession setAuthority(HttpServletRequest request, User user, String selectedRole) {
        Role role = roleDao.findById(selectedRole).get();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, dd MMMM yyyy", new Locale("id"));
        HttpSession session = request.getSession();

        switch (role.getId()){
            case "mahasiswa":
                Student student = studentDao.findByUser(user);
//            StudentStatusAcademic studentStatusAcademic = studentStatusAcademicDao.findFirstByStudentAndStatusOrderBySemesterDesc(student, StatusRecord.ACTIVE);
                session.setAttribute("authority", role);
                session.setAttribute("userDetail", student);
                session.setAttribute("date", LocalDate.now().format(formatter));
//            if (studentStatusAcademic != null) {
                session.setAttribute("ips", 10);
                session.setAttribute("ipk", 35.6);
                session.setAttribute("semester", 1);
//            }
                break;

            case "adminyayasan":
                session.setAttribute("authority", role);
                session.setAttribute("user", user);
                session.setAttribute("date", LocalDate.now().format(formatter));
                break;

            default:
                Employes employes = employesDao.findByUser(user);
                List<InstitutionProdi> institutionProdis = institutionProdiDao.getSelectedProdi(employes.getId());
                session.setAttribute("prodi", institutionProdis);
                session.setAttribute("authority", role);
                session.setAttribute("user", user);
                session.setAttribute("employee", employes);
                session.setAttribute("date", LocalDate.now().format(formatter));
        }
        return session;

    }

    public User createUser(String email, String name, String roleId) {
        Optional<Role> optionalRole = roleDao.findById(roleId);
        if (optionalRole.isEmpty()) {
            log.info("Role id not found: {}", roleId);
            // Handle case when role is not found
            return null;
        }

        User user = new User();
        user.setUserName(email);
        user.setActive(true);
        user.setUser(name);

        // Initialize roles if not initialized
        if (user.getRoles() == null) {
            user.setRoles(new HashSet<>());
        }

        // Add role to user
        user.getRoles().add(optionalRole.get());

        userDao.save(user);

        return user;
    }

    public User createUserWithoutRole(String email, String name, String employeeId) {
        User user = new User();
        user.setUserName(email);
        user.setActive(true);
        user.setUser(name);

        User savedUser = userDao.save(user);

        if (employeeId != null && !employeeId.isEmpty()) {
            Optional<Employes> optionalEmployes = employesDao.findById(employeeId);
            if (optionalEmployes.isPresent()) {
                Employes employes = optionalEmployes.get();
                employes.setUser(savedUser);
                employesDao.save(employes);
            } else {
                throw new EntityNotFoundException("Employes with ID " + employeeId + " not found");
            }
        }

        return savedUser;
    }

}
