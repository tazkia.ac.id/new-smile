package id.ac.tazkia.newsmile.service;

import id.ac.tazkia.newsmile.dao.FoundationDao;
import id.ac.tazkia.newsmile.dao.User.RoleDao;
import id.ac.tazkia.newsmile.dao.User.UserDao;
import id.ac.tazkia.newsmile.dto.foundation.FoundationDto;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.config.Role;
import id.ac.tazkia.newsmile.entity.config.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;

@Service
@Slf4j
public class FoundationService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private FoundationDao foundationDao;

    @Transactional
    public void createFoundation(FoundationDto foundationDto) {
        Optional<Role> optionalRole = roleDao.findById("adminyayasan");
        if (optionalRole.isEmpty()) {
            log.info("Role 'adminyayasan' not found.");
            // Handle case when role is not found
            return;
        }

        Foundation foundation = new Foundation();
        foundation.setFoundationName(foundationDto.getFoundationName());

        User user = createUserFoundation(foundationDto.getEmail(), foundationDto.getName(), optionalRole.get());
        user.setFoundation(foundation);

        // Initialize users if not initialized
        if (foundation.getUsers() == null) {
            foundation.setUsers(new HashSet<>());
        }

        foundation.getUsers().add(user);

        foundationDao.save(foundation);
    }

    @Transactional
    public void editFoundation(String foundationId, FoundationDto foundationDto) {
        Optional<Foundation> optionalFoundation = foundationDao.findById(foundationId);
        if (optionalFoundation.isEmpty()) {
            log.info("Foundation with ID {} not found.", foundationId);
            // Handle case when foundation is not found
            return;
        }

        Foundation foundation = optionalFoundation.get();
        foundation.setFoundationName(foundationDto.getFoundationName());

        Optional<User> optionalUser = userDao.findByFoundationId(foundationId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setUserName(foundationDto.getEmail());
            user.setUser(foundationDto.getName());
            userDao.save(user);
        } else {
            Role adminRole = roleDao.findById("adminyayasan").orElseThrow(() -> new RuntimeException("Role 'adminyayasan' not found."));
            User newUser = createUserFoundation(foundationDto.getEmail(), foundationDto.getName(), adminRole);
            newUser.setFoundation(foundation);
            foundation.getUsers().add(newUser);
        }

        foundationDao.save(foundation);
    }

    private User createUserFoundation(String email, String name, Role role) {
        User user = new User();
        user.setUserName(email);
        user.setUser(name);
        user.setActive(true);

        // Initialize roles if not initialized
        if (user.getRoles() == null) {
            user.setRoles(new HashSet<>());
        }
        user.getRoles().add(role);

        userDao.save(user);

        return user;
    }
}
