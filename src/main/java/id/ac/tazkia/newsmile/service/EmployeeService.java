package id.ac.tazkia.newsmile.service;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.employee.EmployeeInstitutionDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dto.employee.RequestEmployee;
import id.ac.tazkia.newsmile.entity.employee.EmployeeInstitution;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeService {
    @Autowired
    private InstitutionDao institutionDao;
    @Autowired
    private UserService userService;
    @Autowired
    private EmployesDao employesDao;
    @Autowired
    private EmployeeInstitutionDao employeeInstitutionDao;

    @Transactional
    public Employes createEmployee(RequestEmployee request) {
        Employes employes;

        // Create or update employe
        if (request.getId() == null || request.getId().isEmpty()) {
            employes = new Employes();
        } else {
            Optional<Employes> optionalEmployes = employesDao.findById(request.getId());
            if (optionalEmployes.isPresent()) {
                employes = optionalEmployes.get();
            } else {
                throw new EntityNotFoundException("Employes with ID " + request.getId() + " not found");
            }
        }

        BeanUtils.copyProperties(request, employes);

        employes = employesDao.save(employes);
        createEmployeeInstitution(employes, request.getInstitution());
        userService.createUserWithoutRole(employes.getEmail(), employes.getFullName(), employes.getId());

        return employes;
    }

    private void createEmployeeInstitution(Employes employes, List<String> institutionIds) {
        if (employes.getId() == null || employes.getId().isEmpty()) {
            throw new IllegalArgumentException("Cannot create EmployeeInstitution: Employes ID is null or empty");
        }

        for (String id : institutionIds) {
            Optional<Institution> optionalInstitution = institutionDao.findById(id);
            if (optionalInstitution.isEmpty()) {
                log.error("Institution ID {} not found", id);
                continue;
            }

            Institution institution = optionalInstitution.get();
            EmployeeInstitution existingEmployeeInstitution = employeeInstitutionDao.findByEmployesAndInstitutionAndStatus(
                    employes, institution, StatusRecord.ACTIVE
            );

            if (existingEmployeeInstitution == null) {
                EmployeeInstitution employeeInstitution = new EmployeeInstitution();
                employeeInstitution.setInstitution(institution);
                employeeInstitution.setEmployes(employes);
                employeeInstitutionDao.save(employeeInstitution);
            }
        }

        employeeInstitutionDao.setStatusEmployeeIns(employes.getId(), StatusRecord.DELETED.name(), institutionIds);
    }
}
