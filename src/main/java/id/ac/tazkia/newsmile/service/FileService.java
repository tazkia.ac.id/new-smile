package id.ac.tazkia.newsmile.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@Slf4j
public class FileService {
    public String uploadFile(MultipartFile file, String directory, String fixName) throws IOException {
        if (file.isEmpty()) {
            throw new IOException("Failed to store empty file.");
        }

        String name = file.getOriginalFilename();
        String extension = "";

        int i = name.lastIndexOf('.');
        int p = Math.max(name.lastIndexOf('/'), name.lastIndexOf('\\'));

        if (i > p) {
            extension = name.substring(i + 1);
        }

        String fileName = fixName + "." + extension;
        String filePath = directory;
        new File(filePath).mkdirs();
        File tujuan = new File(filePath + File.separator + fileName);
        file.transferTo(tujuan);

        return fileName;
    }
}
