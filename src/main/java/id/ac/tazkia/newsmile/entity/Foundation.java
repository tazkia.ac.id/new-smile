package id.ac.tazkia.newsmile.entity;

import id.ac.tazkia.newsmile.entity.config.User;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@SQLDelete(sql = "UPDATE foundation SET status = 'DELETED' WHERE id=?")
@Where(clause = "status in ('ACTIVE','INACTIVE')")
public class Foundation extends BaseEntity {
    @NotNull
    @Column(name = "foundation_name")
    private String foundationName;

    @Column(name = "description", columnDefinition = "LONGTEXT")
    private String description;

    @OneToMany(mappedBy = "foundation", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<User> users = new HashSet<>();

    @Override
    public int hashCode() {
        return getId() != null ? getId().hashCode() : 0;
    }
}
