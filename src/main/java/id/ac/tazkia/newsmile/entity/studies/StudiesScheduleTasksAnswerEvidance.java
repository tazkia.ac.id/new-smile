package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_schedule_tasks_answer_evidance SET status = 'DELETED' WHERE id=?")
public class StudiesScheduleTasksAnswerEvidance extends BaseEntity {
    @NotNull(message = "StudiesScheduleTasks cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_schedule_tasks")
    private StudiesScheduleTasks studiesScheduleTasks;

    @NotNull(message = "StudiesScheduleTasksAnswer cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_schedule_tasks_answer")
    private StudiesScheduleTasksAnswer studiesScheduleTasksAnswer;

    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @Column(columnDefinition = "LONGTEXT")
    private String fileName;

    @Column(columnDefinition = "LONGTEXT")
    private String file;
}
