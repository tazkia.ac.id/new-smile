package id.ac.tazkia.newsmile.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.constants.StatusRecord;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@MappedSuperclass
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @LastModifiedDate
    @JsonBackReference
    private LocalDateTime lastUpdate;

    @LastModifiedBy
    @JsonBackReference
    private String userUpdate;

    @Enumerated(EnumType.STRING)
    @JsonBackReference
    private StatusRecord status = StatusRecord.ACTIVE;

    @PrePersist
    @PreUpdate
    public void updateTimestamps() {
        this.lastUpdate = ZonedDateTime.now(ZoneId.of("Asia/Jakarta")).toLocalDateTime();
    }

}
