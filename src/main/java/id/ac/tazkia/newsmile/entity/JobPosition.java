package id.ac.tazkia.newsmile.entity;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.department.Department;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE job_position SET status = 'DELETED' WHERE id=?")
public class JobPosition extends BaseEntity{
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

    @NotNull(message = "Position Name cannot be empty")
    private String positionName;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord activeStatus = StatusRecord.ACTIVE;
}
