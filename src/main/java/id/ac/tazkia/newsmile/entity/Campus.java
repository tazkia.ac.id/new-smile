package id.ac.tazkia.newsmile.entity;

import id.ac.tazkia.newsmile.entity.institution.Institution;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Data
@SQLDelete(sql = "UPDATE campus SET status = 'DELETED' WHERE id=?")
@Where(clause = "status in ('ACTIVE','INACTIVE')")
public class Campus extends BaseEntity{
    @NotNull(message = "Institution cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_institution")
    private Institution institution;

    @NotNull(message = "Campus Name cannot be empty")
    private String campusName;

    private String description;

    private String address;


}
