package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.Bank;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_payment SET status = 'DELETED' WHERE id=?")
public class StudentPayment {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @NotNull(message = "StudentInvoice cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student_invoice")
    private StudentInvoice studentInvoice;

    @NotNull(message = "Bank cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_bank")
    private Bank bank;

    @NotNull(message = "Virtual Account Number cannot be empty")
    private String virtualAccountNumber;

    @NotNull(message = "Amount cannot be empty")
    private BigDecimal amount;

    private LocalDateTime paymentDate;

    private String reference;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    @NotNull(message = "Amount Real cannot be empty")
    private BigDecimal amountReal;

    @NotNull(message = "Student cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student")
    private Student student;

    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_periode_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Period Academic Finance cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_periode_academic_finance")
    private PeriodAcademicFinance periodAcademicFinance;
}
