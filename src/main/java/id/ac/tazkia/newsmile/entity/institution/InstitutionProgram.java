package id.ac.tazkia.newsmile.entity.institution;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE institution_program SET status = 'DELETED' WHERE id=?")
public class InstitutionProgram extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private InstitutionProdi institutionProdi;

    @NotNull
    private String programCode;

    @NotNull
    private String programName;

    @Column(columnDefinition = "LONGTEXT")
    private String description;
}
