package id.ac.tazkia.newsmile.entity.ploting;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.employee.EmployeeLecturer;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE ploting_lecturer SET status = 'DELETED' WHERE id=?")
public class PlotingLecturer extends BaseEntity {
    @NotNull(message = "Ploting cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_ploting")
    private Ploting ploting;

    private String lecturerStatus;

    @NotNull(message = "Employee Lecturer cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_lecturer")
    private EmployeeLecturer employeeLecturer;


}
