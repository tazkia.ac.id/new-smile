package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.constants.StatusApprove;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.time.LocalDateTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_payment_plan SET status = 'DELETED' WHERE id=?")
public class StudentPaymentPlan extends BaseEntity {
    @NotNull(message = "StudentBill cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student_bill")
    private StudentBill studentBill;

    private String paymentPlanTypeId;

    @Enumerated(EnumType.STRING)
    private StatusApprove statusApprove;

    private LocalDateTime dateApprove;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_periode_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Period Academic Finance cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_periode_academic_finance")
    private PeriodAcademicFinance periodAcademicFinance;

    @NotNull(message = "Student cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student")
    private Student student;
}
