package id.ac.tazkia.newsmile.entity.config;

import id.ac.tazkia.newsmile.entity.Foundation;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "s_user")
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", length = 36)
    private String id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "user")
    private String user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_foundation", referencedColumnName = "id")
    private Foundation foundation;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "s_user_role",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns = @JoinColumn(name = "id_role"))
    private Set<Role> roles = new HashSet<>();

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
