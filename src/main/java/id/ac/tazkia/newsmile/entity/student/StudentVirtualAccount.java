package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.entity.Bank;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_virtual_account SET status = 'DELETED' WHERE id=?")
public class StudentVirtualAccount extends BaseEntity {
    @NotNull(message = "StudentInvoice cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student_invoice")
    private StudentInvoice studentInvoice;

    @NotNull(message = "StudentInvoice cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student")
    private Student student;

    @NotNull(message = "Bank cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_bank")
    private Bank bank;

    @NotNull(message = "Virtual Account Number cannot be empty")
    private String virtualAccountNumber;


}
