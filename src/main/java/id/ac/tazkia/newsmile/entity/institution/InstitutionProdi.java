package id.ac.tazkia.newsmile.entity.institution;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.department.Department;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE institution_prodi SET status = 'DELETED' WHERE id=?")
public class InstitutionProdi extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_faculty")
    @JsonBackReference
    private InstitutionFaculty institutionFaculty;

    @ManyToOne
    @JoinColumn(name = "id_department")
    @JsonBackReference
    private Department department;

    @NotNull
    private String prodiName;

    @Column(columnDefinition = "LONGTEXT")
    private String description;
}
