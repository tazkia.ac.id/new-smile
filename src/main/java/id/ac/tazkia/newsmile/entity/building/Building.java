package id.ac.tazkia.newsmile.entity.building;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.Campus;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Data
@SQLDelete(sql = "UPDATE building SET status = 'DELETED' WHERE id=?")
@Where(clause = "status in ('ACTIVE','INACTIVE')")
public class Building extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "id_institution")
    private Institution institution;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_campus")
    private Campus campus;

    @NotNull
    private String buildingName;

    @Column(columnDefinition = "LONGTEXT")
    private String buildingAddress;

    private String buildingArea;
}
