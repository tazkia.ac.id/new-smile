package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_bill_detail SET status = 'DELETED' WHERE id=?")
public class StudentBillDetail extends BaseEntity {
    @NotNull(message = "Student Bill cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student_bill")
    private StudentBill studentBill;

    private String idBillValue;

    private BigDecimal value;

    private String type;

    private String idStudentBillBefore;
}
