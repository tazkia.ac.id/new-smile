package id.ac.tazkia.newsmile.entity.period;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProgram;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE period_academic_prodi SET status = 'DELETED' WHERE id=?")
public class PeriodAcademicProdi extends BaseEntity {
    @NotNull(message = "Institution Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private InstitutionProdi institutionProdi;

    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Institution Program cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_program")
    private InstitutionProgram institutionProgram;

    @NotNull(message = "Ploting Start cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate plotingStart;

    @NotNull(message = "Ploting end cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate plotingEnd;

    @NotNull(message = "Scheduling Start cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate schedulingStart;

    @NotNull(message = "Scheduling End cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate schedulingEnd;

    @NotNull(message = "Krs Start cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate krsStart;

    @NotNull(message = "Krs End cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate krsEnd;

    @NotNull(message = "Final Date cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate finalDate;

}
