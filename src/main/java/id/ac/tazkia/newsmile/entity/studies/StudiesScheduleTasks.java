package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_schedule_tasks SET status = 'DELETED' WHERE id=?")
public class StudiesScheduleTasks extends BaseEntity {
    @NotNull(message = "StudiesSchedule cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_schedule")
    private StudiesSchedule studiesSchedule;

    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "tasksName cannot be empty")
    private String tasksName;

    @Column(columnDefinition = "LONGTEXT")
    private String tasksDescription;

    private LocalDateTime allowFrom;
    private LocalDateTime dueDate;
    private LocalDateTime remainMeToGradeBy;

    private Integer maximumUploadFile;

    private BigDecimal percentaseGrade;
}
