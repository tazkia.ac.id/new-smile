package id.ac.tazkia.newsmile.entity.curriculum;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE curiculum SET status = 'DELETED' WHERE id=?")
public class Curiculum extends BaseEntity {
    @NotNull(message = "Institution Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    @JsonBackReference
    private InstitutionProdi institutionProdi;

    @NotNull(message = "Curiculum Code cannot be empty")
    private String curiculumCode;

    @NotNull(message = "Curiculum Name cannot be empty")
    private String curiculumName;
}
