package id.ac.tazkia.newsmile.entity.curriculum;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE curiculum_subject_rps SET status = 'DELETED' WHERE id=?")
public class CuriculumSubjectRps extends BaseEntity {
    @NotNull(message = "Curiculum Subject cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_curiculum_subject")
    private CuriculumSubject curiculumSubject;

    private Integer session;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    @Column(columnDefinition = "LONGTEXT")
    private String file;
}
