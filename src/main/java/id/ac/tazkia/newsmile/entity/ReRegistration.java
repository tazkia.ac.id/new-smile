package id.ac.tazkia.newsmile.entity;

import id.ac.tazkia.newsmile.constants.StatusApprove;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.time.LocalDateTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE re_registration SET status = 'DELETED' WHERE id=?")
public class ReRegistration extends BaseEntity{
    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Period Academic Finance cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_finance")
    private PeriodAcademicFinance periodAcademicFinance;

    @NotNull(message = "Period Academic Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_prodi")
    private PeriodAcademicProdi periodAcademicProdi;

    @NotNull(message = "Period Academic Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    private LocalDateTime requestDate;

    private String type;

    @Enumerated(EnumType.STRING)
    private StatusApprove statusApprove;

}
