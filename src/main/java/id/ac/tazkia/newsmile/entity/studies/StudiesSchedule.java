package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.*;
import id.ac.tazkia.newsmile.entity.curriculum.Curiculum;
import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubject;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import id.ac.tazkia.newsmile.entity.ploting.Ploting;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_schedule SET status = 'DELETED' WHERE id=?")
public class StudiesSchedule extends BaseEntity {
    @NotNull(message = "Ploting cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_ploting")
    private Ploting ploting;

    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_periode_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_prodi")
    private PeriodAcademicProdi periodAcademicProdi;

    @NotNull(message = "InstitutionProdi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private InstitutionProdi institutionProdi;

    @NotNull(message = "Curiculum cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_curiculum")
    private Curiculum curiculum;

    @NotNull(message = "CuriculumSubject cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_curiculum_subject")
    private CuriculumSubject curiculumSubject;

    @NotNull(message = "Subject cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_subject")
    private Subject subject;

    @NotNull(message = "subjectCode cannot be empty")
    private String subjectCode;

    @NotNull(message = "subjectName cannot be empty")
    private String subjectName;

    @NotNull(message = "subjectNameEnglish cannot be empty")
    private String subjectNameEnglish;

    @NotNull(message = "sks cannot be empty")
    private BigDecimal sks;



}
