package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.time.LocalDateTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_attendance_student SET status = 'DELETED' WHERE id=?")
public class StudiesAttendanceStudent extends BaseEntity {
    @NotNull(message = "StudiesSession cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_session")
    private StudiesSession studiesSession;

    @NotNull(message = "StudiesAttendanceLecturer cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_attendance_lecturer")
    private StudiesAttendanceLecturer studiesAttendanceLecturer;

    @NotNull(message = "StudiesSchedule cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_schedule")
    private StudiesSchedule studiesSchedule;

    private LocalDateTime attendanceIn;
    private LocalDateTime attendanceOut;

    private String attendanceStatus;

    @Column(columnDefinition = "LONGTEXT")
    private String description;
}
