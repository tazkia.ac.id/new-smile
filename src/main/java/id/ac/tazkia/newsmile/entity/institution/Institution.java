package id.ac.tazkia.newsmile.entity.institution;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.constants.TypeInstitution;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.Foundation;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Data
@SQLDelete(sql = "UPDATE institution SET status = 'DELETED' WHERE id=?")
@Where(clause = "status in ('ACTIVE','INACTIVE')")
public class Institution extends BaseEntity {
    @NotNull
    private String institutionName;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "id_foundation")
    private Foundation foundation;

    @NotNull(message = "type institution not null")
    private String typeInstitution;

}
