package id.ac.tazkia.newsmile.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import id.ac.tazkia.newsmile.entity.institution.InstitutionFaculty;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;


@Entity
@Data
@SQLDelete(sql = "UPDATE subject SET status = 'DELETED' WHERE id=?")
public class Subject extends BaseEntity{
    @NotNull(message = "Institution Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    @JsonBackReference
    private InstitutionProdi institutionProdi;

    @NotNull(message = "Code cannot be empty")
    private String code;

    @NotNull(message = "Subject Name cannot be empty")
    private String subjectName;

    @NotNull(message = "Subject Name English cannot be empty")
    private String subjectNameEnglish;

    private String abbreviation;

    @ManyToOne
    @JoinColumn(name = "id_institution")
    @JsonBackReference
    private Institution institution;

    @ManyToOne
    @JoinColumn(name = "id_faculty")
    @JsonBackReference
    private InstitutionFaculty institutionFaculty;

}
