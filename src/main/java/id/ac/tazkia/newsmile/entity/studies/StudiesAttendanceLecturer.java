package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.employee.EmployeeLecturer;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.time.LocalDateTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_attendance_lecturer SET status = 'DELETED' WHERE id=?")
public class StudiesAttendanceLecturer extends BaseEntity {
    @NotNull(message = "StudiesSchedule cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_session")
    private StudiesSession studiesSession;

    @NotNull(message = "PeriodAcademicProdi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_prodi")
    private PeriodAcademicProdi periodAcademicProdi;


    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;


    @NotNull(message = "EmployeeLecturer cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_lecturer")
    private EmployeeLecturer employeeLecturer;

    private LocalDateTime attendanceIn;
    private LocalDateTime attendanceOut;

    private String reportDescription;

    private Integer conformityWithRps;

    private String attendanceStatus;
}
