package id.ac.tazkia.newsmile.entity.building;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE building_floor SET status = 'DELETED' WHERE id=?")
public class BuildingFloor extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_building")
    private Building building;

    @NotNull
    private String floor;
}
