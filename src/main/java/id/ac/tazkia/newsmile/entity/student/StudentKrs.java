package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.entity.*;
import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubject;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import id.ac.tazkia.newsmile.entity.studies.StudiesSchedule;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_krs SET status = 'DELETED' WHERE id=?")
public class StudentKrs extends BaseEntity {
    @NotNull(message = "StudentStatusAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student_status_academic")
    private StudentStatusAcademic studentStatusAcademic;

    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_finance")
    private PeriodAcademicFinance periodAcademicFinance;

    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_prodi")
    private PeriodAcademicProdi periodAcademicProdi;

    @NotNull(message = "StudiesSchedule cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_schedule")
    private StudiesSchedule studiesSchedule;

    @NotNull(message = "CuriculumSubject cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_curiculum_subject")
    private CuriculumSubject curiculumSubject;


    @NotNull(message = "Subject cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_subject")
    private Subject subject;

    @NotNull(message = "subject_code cannot be empty")
    private String subjectCode;

    @NotNull(message = "subjectName cannot be empty")
    private String subjectName;

    @NotNull(message = "subjectNameEnglish cannot be empty")
    private String subjectNameEnglish;

    @NotNull(message = "sks cannot be empty")
    private BigDecimal sks;

    @NotNull(message = "sks cannot be empty")
    private BigDecimal finalScore;

    @NotNull(message = "grade cannot be empty")
    private String grade;

    @NotNull(message = "weight cannot be empty")
    private BigDecimal weight;


}
