package id.ac.tazkia.newsmile.entity.department;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Data
@SQLDelete(sql = "UPDATE department SET status = 'DELETED' WHERE id=?")
@Where(clause = "status in ('ACTIVE','INACTIVE')")
public class Department extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_institution")
    @JsonBackReference
    private Institution institution;

    @NotNull
    private String departmentName;

    @Column(columnDefinition = "LONGTEXT")
    private String description;
}
