package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_status SET status = 'DELETED' WHERE id=?")
public class StudentStatus extends BaseEntity {
    @NotNull
    private String code;

    @NotNull
    private String name;
}
