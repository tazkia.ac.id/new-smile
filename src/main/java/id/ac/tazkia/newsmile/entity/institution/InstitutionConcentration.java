package id.ac.tazkia.newsmile.entity.institution;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE institution_concentration SET status = 'DELETED' WHERE id=?")
public class InstitutionConcentration extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    @JsonBackReference
    private InstitutionProdi institutionProdi;

    @NotNull
    private String concentrationName;

}
