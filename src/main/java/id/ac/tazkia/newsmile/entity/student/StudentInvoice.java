package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_invoice SET status = 'DELETED' WHERE id=?")
public class StudentInvoice {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @NotNull(message = "StudentBill cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student_payment_plan")
    private StudentPaymentPlan studentPaymentPlan;

    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_periode_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Period Academic Finance cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_periode_academic_finance")
    private PeriodAcademicFinance periodAcademicFinance;

    private Integer installmentNumber;

    private BigDecimal nominal;

    private String sendStatus;

    private LocalDate planDate;

    private LocalDate dueDate;

    @Enumerated(EnumType.STRING)
    private StatusRecord status = StatusRecord.ACTIVE;

    @NotNull(message = "Student cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student")
    private Student student;
}
