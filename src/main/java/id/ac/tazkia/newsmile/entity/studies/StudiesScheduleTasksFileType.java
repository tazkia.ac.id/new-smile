package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_schedule_tasks_file_type SET status = 'DELETED' WHERE id=?")
public class StudiesScheduleTasksFileType extends BaseEntity {
    @NotNull(message = "StudiesScheduleTasks cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_schedule_tasks")
    private StudiesScheduleTasks studiesScheduleTasks;

    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    private String fileType;
}
