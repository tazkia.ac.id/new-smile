package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.time.LocalTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_session SET status = 'DELETED' WHERE id=?")
public class StudiesSession extends BaseEntity {
    @NotNull(message = "StudiesSchedule cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_schedule")
    private StudiesSchedule studiesSchedule;

    @NotNull(message = "StudiesScheduleSession cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_schedule_session")
    private StudiesScheduleSession studiesScheduleSession;

    @Column(columnDefinition = "TIME")
    private LocalTime sessionStart;

    @Column(columnDefinition = "TIME")
    private LocalTime sessionEnd;

    private String sessionId;

    private String sessionStatus;
    private String sessionType;




}
