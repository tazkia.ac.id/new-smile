package id.ac.tazkia.newsmile.entity.employee;

import id.ac.tazkia.newsmile.constants.LecturerType;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE employee_lecturer SET status = 'DELETED' WHERE id=?")
public class EmployeeLecturer extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_prodi_home_base")
    private InstitutionProdi institutionProdi;

    @Enumerated(EnumType.STRING)
    private LecturerType lecturerType;

    private String nidn;

    @Column(columnDefinition = "LONGTEXT")
    private String kartuNidn;

    private String noCertificateLecturer;

    private String certificateLecturer;

    private String positionStructural;

    @Column(columnDefinition = "LONGTEXT")
    private String skPositionStructural;

    private String positionFunctional;

    @Column(columnDefinition = "LONGTEXT")
    private String skPositionFungtional;


}
