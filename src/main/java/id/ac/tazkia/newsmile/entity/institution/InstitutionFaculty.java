package id.ac.tazkia.newsmile.entity.institution;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.department.Department;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Data
@SQLDelete(sql = "UPDATE institution_faculty SET status = 'DELETED' WHERE id=?")
@Where(clause = "status in ('ACTIVE','INACTIVE')")
public class InstitutionFaculty extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_institution")
    private Institution institution;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

    @NotNull
    private String facultyName;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    @Enumerated(EnumType.STRING)
    private StatusRecord activeStatus = StatusRecord.ACTIVE;
}
