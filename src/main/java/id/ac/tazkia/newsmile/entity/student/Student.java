package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProgram;
import id.ac.tazkia.newsmile.entity.config.User;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE student SET status = 'DELETED' WHERE id=?")
public class Student extends BaseEntity {
    @NotNull(message = "Institution Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private InstitutionProdi institutionProdi;


    @NotNull(message = "Institution Program cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_program")
    private InstitutionProgram institutionProgram;

    @NotNull(message = "Student Number cannot be empty")
    private String studentNumber;

    @NotNull(message = "Student Name cannot be empty")
    private String studentName;

    @NotNull(message = "Gender cannot be empty")
    private String gender;

    @NotNull(message = "Email cannot be empty")
    private String email;

    @NotNull(message = "Generation cannot be empty")
    private String generation;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;
}
