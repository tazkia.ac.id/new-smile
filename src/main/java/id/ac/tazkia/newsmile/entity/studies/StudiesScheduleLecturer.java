package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.employee.EmployeeLecturer;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_schedule_lecturer SET status = 'DELETED' WHERE id=?")
public class StudiesScheduleLecturer extends BaseEntity {
    @NotNull(message = "StudiesSchedule cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_studies_schedule")
    private StudiesSchedule studiesSchedule;

    @NotNull(message = "EmployeeLecturer cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_lecturer")
    private EmployeeLecturer employeeLecturer;

    private String lecturerName;

    private String lecturerStatus;

}
