package id.ac.tazkia.newsmile.entity.student;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.constants.StatusBanned;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import id.ac.tazkia.newsmile.entity.ReRegistration;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_status_academic SET status = 'DELETED' WHERE id=?")
public class StudentStatusAcademic extends BaseEntity {
//    @NotNull(message = "Re Registration cannot be empty")
//    @ManyToOne
//    @JoinColumn(name = "id_re_registration")
//    private ReRegistration reRegistration;
//
//    @NotNull(message = "Period Academic cannot be empty")
//    @ManyToOne
//    @JoinColumn(name = "id_period_academic")
//    private PeriodAcademic periodAcademic;
//
//    @NotNull(message = "Period Academic Prodi cannot be empty")
//    @ManyToOne
//    @JoinColumn(name = "id_period_academic_prodi")
//    private PeriodAcademicProdi periodAcademicProdi;

//    @NotNull(message = "Student Status cannot be empty")
//    @ManyToOne
//    @JoinColumn(name = "student_status")
//    private StudentStatus studentStatus;

    @NotNull(message = "Student cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student")
    @JsonBackReference
    private Student student;

    private BigDecimal sksTotalBefore;

    private BigDecimal currentSksTotal;

    private BigDecimal currentIp;

    private BigDecimal ipk;

    private Integer semester;

    @Enumerated(EnumType.STRING)
    private StatusBanned bannedStatus;
}
