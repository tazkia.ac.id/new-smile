package id.ac.tazkia.newsmile.entity.curriculum;

import id.ac.tazkia.newsmile.entity.institution.InstitutionConcentration;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.entity.Subject;
import id.ac.tazkia.newsmile.utils.RomanNumeralConverter;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE curiculum_subject SET status = 'DELETED' WHERE id=?")
public class CuriculumSubject{
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @NotNull(message = "Curiculum cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_curiculum")
    private Curiculum curiculum;

    @NotNull(message = "Subject cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_subject")
    private Subject subject;

    private Integer semester;

    private BigDecimal sks;

    @NotNull(message = "Institution Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private InstitutionProdi institutionProdi;

    @ManyToOne
    @JoinColumn(name = "id_konsentrasi")
    private InstitutionConcentration institutionConcentration;

    @Column(columnDefinition = "LONGTEXT")
    private String silabus ;

    private String aksesSubject;

    private String blockNote;

    private String blockSempro;

    private String blockThesis;

    private Boolean allowTranskript = false;

    private String bookReference;

    public String semesterRoman() {
        if (semester != null) {
            return RomanNumeralConverter.toRoman(semester);
        } else {
            return "N/A";
        }
    }
}
