package id.ac.tazkia.newsmile.entity.studies;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.student.Student;
import id.ac.tazkia.newsmile.entity.student.StudentKrs;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Data
@SQLDelete(sql = "UPDATE studies_schedule_tasks_answer SET status = 'DELETED' WHERE id=?")
public class StudiesScheduleTasksAnswer extends BaseEntity {
    @NotNull(message = "StudiesScheduleTasks cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_schedule_tasks")
    private StudiesScheduleTasks studiesScheduleTasks;

    @NotNull(message = "StudentKrs cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_krs")
    private StudentKrs studentKrs;


    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Student cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student")
    private Student student;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

    private LocalDateTime dateAnswer;

    private String answerStatus;

    private BigDecimal score;

    private String scoreStatus;


}
