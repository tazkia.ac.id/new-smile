package id.ac.tazkia.newsmile.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE payment_plan_type SET status = 'DELETED' WHERE id=?")
public class PaymentPlanType extends BaseEntity{
    private String planType;

    private Integer installment;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

}
