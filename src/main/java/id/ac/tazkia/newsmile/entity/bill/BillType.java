package id.ac.tazkia.newsmile.entity.bill;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@Data
@SQLDelete(sql = "UPDATE bill_type SET status = 'DELETED' WHERE id=?")
@Where(clause = "status='ACTIVE'")
public class BillType extends BaseEntity {
    @NotNull
    private String billCode;

    @NotNull
    private String billName;

    @Column(columnDefinition = "LONGTEXT")
    private String description;

}
