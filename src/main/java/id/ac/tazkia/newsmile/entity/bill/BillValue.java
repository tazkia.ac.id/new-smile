package id.ac.tazkia.newsmile.entity.bill;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProgram;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE bill_value SET status = 'DELETED' WHERE id=?")
@Where(clause = "status='ACTIVE'")
public class BillValue extends BaseEntity {
    @NotNull(message = "BillType cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_bill_type")
    private BillType billType;

    @NotNull(message = "PeriodAcademic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "PeriodAcademicFinance cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_finance")
    private PeriodAcademicFinance periodAcademicFinance;

    @NotNull(message = "InstitutionProgram cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_program")
    private InstitutionProgram institutionProgram;

    private String generation;

    @NotNull(message = "InstitutionProdi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private InstitutionProdi institutionProdi;

    private BigDecimal value;

    private String studentStatus;


}
