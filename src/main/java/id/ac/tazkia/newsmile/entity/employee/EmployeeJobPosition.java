package id.ac.tazkia.newsmile.entity.employee;

import com.fasterxml.jackson.annotation.JsonBackReference;
import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.JobPosition;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE employee_job_position SET status = 'DELETED' WHERE id=?")
public class EmployeeJobPosition extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "id_employee")
    @JsonBackReference
    private Employes employes;



    @ManyToOne
    @JoinColumn(name = "id_job_position")
    @JsonBackReference
    private JobPosition jobPosition;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateStart;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateEnd;

    @Enumerated(EnumType.STRING)
    private StatusRecord activeStatus = StatusRecord.ACTIVE;
}
