package id.ac.tazkia.newsmile.entity.department;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE department_head SET status = 'DELETED' WHERE id=?")
public class DepartmentHead extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate periodStart;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate periodEnd;

    @Enumerated(EnumType.STRING)
    private StatusRecord activeStatus = StatusRecord.ACTIVE;

    private String signature;
}
