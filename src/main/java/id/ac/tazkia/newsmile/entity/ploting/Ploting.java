package id.ac.tazkia.newsmile.entity.ploting;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import id.ac.tazkia.newsmile.entity.Subject;
import id.ac.tazkia.newsmile.entity.curriculum.Curiculum;
import id.ac.tazkia.newsmile.entity.curriculum.CuriculumSubject;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE ploting SET status = 'DELETED' WHERE id=?")
public class Ploting extends BaseEntity {
    @NotNull(message = "Period Academic cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Period Academic Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_prodi")
    private PeriodAcademicProdi periodAcademicProdi;

    @NotNull(message = "Institution Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_prodi")
    private InstitutionProdi institutionProdi;

//    @NotNull
//    @ManyToOne
//    @JoinColumn(name = "id_prodi")
//    private Class class;

    @NotNull(message = "Curiculum Subject cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_curiculum_subject")
    private CuriculumSubject curiculumSubject;

    @ManyToOne
    @JoinColumn(name = "id_subject")
    private Subject subject;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_curiculum")
    private Curiculum curiculum;

}
