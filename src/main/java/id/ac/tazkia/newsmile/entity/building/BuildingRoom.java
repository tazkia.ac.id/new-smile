package id.ac.tazkia.newsmile.entity.building;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE building_room SET status = 'DELETED' WHERE id=?")
public class BuildingRoom extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_floor")
    private BuildingFloor buildingFloor;

    @NotNull(message = "Room Name cannot be empty")
    private String roomName;

    private Integer capacity;

    private BigDecimal dimensionLength;
    private BigDecimal dimensionWidth;
    private BigDecimal dimensionHeight;


}
