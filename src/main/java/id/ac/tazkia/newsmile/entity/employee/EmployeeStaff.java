package id.ac.tazkia.newsmile.entity.employee;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.utils.DateUtils;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE employee_staff SET status = 'DELETED' WHERE id=?")
public class EmployeeStaff extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateStart;

    public String getFormattedDateStart() {
        return DateUtils.formatIndonesianDate(this.dateStart);
    }
}
