package id.ac.tazkia.newsmile.entity;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE bank SET status = 'DELETED' WHERE id=?")
public class Bank extends BaseEntity {
    @NotNull
    private String bankCode;

    @NotNull
    private String bankName;

    private BigDecimal virtualAccountPrice;

}
