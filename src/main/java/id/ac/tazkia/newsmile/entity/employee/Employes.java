package id.ac.tazkia.newsmile.entity.employee;

import id.ac.tazkia.newsmile.constants.Gender;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.config.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE employes SET status = 'DELETED' WHERE id=?")
public class Employes extends BaseEntity {
    private String number;

    private String nik;

    private String fullName;

    private String nickName;

    private String frontTitle;

    private String backTitle;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private String email;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    private String photo;
}
