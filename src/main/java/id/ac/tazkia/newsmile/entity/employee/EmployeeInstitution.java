package id.ac.tazkia.newsmile.entity.employee;

import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

@Entity
@Data
@SQLDelete(sql = "UPDATE employee_institution SET status = 'DELETED' WHERE id=?")
public class EmployeeInstitution extends BaseEntity {
    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_employee")
    private Employes employes;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_institution")
    private Institution institution;

}
