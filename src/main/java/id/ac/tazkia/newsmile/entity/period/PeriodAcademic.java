package id.ac.tazkia.newsmile.entity.period;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.entity.BaseEntity;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE period_academic SET status = 'DELETED' WHERE id=?")
public class PeriodAcademic extends BaseEntity {
    @NotNull(message = "Period Code cannot be empty")
    private String periodCode;

    @NotNull(message = "Period Name cannot be empty")
    private String periodName;

    @NotNull(message = "Period Start cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate periodStart;

    @NotNull(message = "Period End cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate periodEnd;

    @NotNull(message = "Institutions cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_institution")
    private Institution institution;

    @Enumerated(EnumType.STRING)
    private StatusRecord activeStatus = StatusRecord.ACTIVE;

    @NotNull(message = "preparation start cannot be empty")
    @Column(columnDefinition = "DATE")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate preparationStart;

    private String type;
}
