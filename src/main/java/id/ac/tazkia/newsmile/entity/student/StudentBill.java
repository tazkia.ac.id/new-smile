package id.ac.tazkia.newsmile.entity.student;

import id.ac.tazkia.newsmile.entity.*;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademic;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicFinance;
import id.ac.tazkia.newsmile.entity.period.PeriodAcademicProdi;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;

import java.math.BigDecimal;

@Entity
@Data
@SQLDelete(sql = "UPDATE student_bill SET status = 'DELETED' WHERE id=?")
public class StudentBill extends BaseEntity {

    @NotNull(message = "Re Registration cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_re_registration")
    private ReRegistration reRegistration;


    @NotNull(message = "Period Academic Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic")
    private PeriodAcademic periodAcademic;

    @NotNull(message = "Period Academic Finance Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_finance")
    private PeriodAcademicFinance periodAcademicFinance;

    @NotNull(message = "Period Academic Prodi cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_period_academic_prodi")
    private PeriodAcademicProdi periodAcademicProdi;

    @NotNull(message = "Student cannot be empty")
    @ManyToOne
    @JoinColumn(name = "id_student")
    private Student student;

    private BigDecimal totalValue;

    private String billStatus;

}
