package id.ac.tazkia.newsmile.entity.config;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table(name = "s_user_role")
@Data
public class UserRole {
    @Id
    private String userId;
    private String role;
    private StatusRecord statusRecord;
}
