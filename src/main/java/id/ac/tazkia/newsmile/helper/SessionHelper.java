package id.ac.tazkia.newsmile.helper;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import id.ac.tazkia.newsmile.dao.CampusDao;
import id.ac.tazkia.newsmile.dao.FoundationDao;
import id.ac.tazkia.newsmile.dao.JobPositionDao;
import id.ac.tazkia.newsmile.dao.building.BuildingDao;
import id.ac.tazkia.newsmile.dao.building.BuildingFloorDao;
import id.ac.tazkia.newsmile.dao.department.DepartmentDao;
import id.ac.tazkia.newsmile.dao.employee.EmployeeInstitutionDao;
import id.ac.tazkia.newsmile.dao.employee.EmployesDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionFacultyDao;
import id.ac.tazkia.newsmile.dao.institution.InstitutionProdiDao;
import id.ac.tazkia.newsmile.entity.Campus;
import id.ac.tazkia.newsmile.entity.Foundation;
import id.ac.tazkia.newsmile.entity.building.Building;
import id.ac.tazkia.newsmile.entity.building.BuildingFloor;
import id.ac.tazkia.newsmile.entity.config.User;
import id.ac.tazkia.newsmile.entity.department.Department;
import id.ac.tazkia.newsmile.entity.employee.EmployeeInstitution;
import id.ac.tazkia.newsmile.entity.employee.Employes;
import id.ac.tazkia.newsmile.entity.institution.Institution;
import id.ac.tazkia.newsmile.entity.institution.InstitutionFaculty;
import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import jakarta.servlet.http.HttpSession;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionHelper {

    public static User getUser(HttpSession session) {
        return (User) session.getAttribute("user");
    }

    public static Foundation getFoundation(HttpSession session) {
        User user = getUser(session);
        return user != null ? user.getFoundation() : null;
    }

    public static List<Foundation> getFoundationList(HttpSession session, FoundationDao foundationDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Foundation> foundationList = new ArrayList<>();
        if (foundation == null) {
            foundationList = foundationDao.findByStatus(StatusRecord.ACTIVE);
        } else {
            foundationList.add(foundationDao.findById(foundation.getId()).get());
        }
        return foundationList;
    }

    public static List<Institution> getInstitutionList(HttpSession session, InstitutionDao institutionDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Institution> institutionList = new ArrayList<>();
        if (foundation == null) {
            institutionList = institutionDao.findByStatus(StatusRecord.ACTIVE);
        } else {
            institutionList = institutionDao.findByStatusAndFoundation(StatusRecord.ACTIVE, foundation);
        }
        return institutionList;
    }

    public static List<Campus> getCampusList(HttpSession session, CampusDao campusDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Campus> campusList = new ArrayList<>();
        if (foundation == null) {
            campusList = campusDao.findByStatusOrderByCampusNameAsc(StatusRecord.ACTIVE);
        } else {
            campusList = campusDao.findByInstitutionFoundationAndStatusOrderByCampusNameAsc(foundation,StatusRecord.ACTIVE);
        }
        return campusList;
    }

    public static List<Building> getBuildingList(HttpSession session, BuildingDao buildingDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Building> buildingList = new ArrayList<>();
        if (foundation == null) {
            buildingList = buildingDao.findByStatusOrderByBuildingNameAsc(StatusRecord.ACTIVE);
        } else {
            buildingList = buildingDao.findByStatusAndInstitutionFoundationOrderByBuildingNameAsc(StatusRecord.ACTIVE, foundation);
        }
        return buildingList;
    }

    public static List<BuildingFloor> getFloorList(HttpSession session, BuildingFloorDao buildingFloorDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<BuildingFloor> floorList = new ArrayList<>();
        if (foundation == null) {
            floorList = buildingFloorDao.findByStatusOrderByFloorAsc(StatusRecord.ACTIVE);
        } else {
            floorList = buildingFloorDao.findByStatusAndBuildingInstitutionFoundationOrderByFloorAsc(StatusRecord.ACTIVE, foundation);
        }
        return floorList;
    }

    public static List<Department> getDepartmentList(HttpSession session, DepartmentDao departmentDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Department> departmentList = new ArrayList<>();
        if (foundation == null) {
            departmentList = departmentDao.findByStatusOrderByDepartmentNameAsc(StatusRecord.ACTIVE);
        } else {
            departmentList = departmentDao.findByStatusAndInstitutionFoundationOrderByDepartmentNameAsc(StatusRecord.ACTIVE, foundation);
        }
        return departmentList;
    }

    public static List<Department> getDepartmentProdi(HttpSession session, DepartmentDao departmentDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<Department> departmentList = new ArrayList<>();
        if (foundation == null) {
            departmentList = departmentDao.departmentNotInFaculty();
        } else {
            departmentList = departmentDao.departmentFoundationNotInFaculty(foundation.getId());
        }
        return departmentList;
    }

    public static List<InstitutionFaculty> getFacultyList(HttpSession session, InstitutionFacultyDao institutionFacultyDao) {
        Foundation foundation = SessionHelper.getFoundation(session);
        List<InstitutionFaculty> facultyList = new ArrayList<>();
        if (foundation == null) {
            facultyList = institutionFacultyDao.findByStatusOrderByFacultyNameAsc(StatusRecord.ACTIVE);
        } else {
            facultyList = institutionFacultyDao.findByStatusAndInstitutionFoundationOrderByFacultyNameAsc(StatusRecord.ACTIVE, foundation);
        }
        return facultyList;
    }

    public static List<InstitutionProdi> getProdiList(HttpSession session, InstitutionProdiDao institutionProdiDao) {
        List<InstitutionProdi> institutionProdis = (List<InstitutionProdi>) session.getAttribute("prodi");


        List<InstitutionProdi> institutionProdiList = new ArrayList<>();
        if (institutionProdis == null || institutionProdis.isEmpty()) {
            institutionProdiList = institutionProdiDao.findByStatusOrderByProdiNameAsc(StatusRecord.ACTIVE);
        } else {
            institutionProdiList.addAll(institutionProdis);
        }
        return institutionProdiList;
    }

    public static List<String> getEmployeeInstitutionId(HttpSession session, InstitutionDao institutionDao, EmployesDao employesDao, EmployeeInstitutionDao employeeInstitutionDao) {
        User user = (User) session.getAttribute("user");
        Employes employes = employesDao.findByUser(user);
        List<Institution> institutionList = new ArrayList<>();
        List<EmployeeInstitution> employeeInstitutions = employeeInstitutionDao.findByEmployesAndStatusOrderByInstitutionInstitutionNameAsc(employes, StatusRecord.ACTIVE);
        if (employeeInstitutions.isEmpty()){
            institutionList.addAll(SessionHelper.getInstitutionList(session,institutionDao));
        }else {
            institutionList.addAll(employeeInstitutions.stream()
                    .map(EmployeeInstitution::getInstitution)
                    .collect(Collectors.toList()));
        }
        List<String> institutionIds = institutionList.stream()
                .map(inst -> inst.getId())
                .collect(Collectors.toList());

        return institutionIds;
    }
}
