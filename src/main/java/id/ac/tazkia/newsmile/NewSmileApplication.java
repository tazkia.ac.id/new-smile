package id.ac.tazkia.newsmile;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import java.util.TimeZone;

@SpringBootApplication
@EnableAspectJAutoProxy
public class NewSmileApplication {

	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Jakarta"));
		SpringApplication.run(NewSmileApplication.class, args);
	}

	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}


	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

}
