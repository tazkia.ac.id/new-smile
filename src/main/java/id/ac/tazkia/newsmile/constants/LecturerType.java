package id.ac.tazkia.newsmile.constants;

public enum LecturerType {
    DOSEN_TETAP("Dosen Tetap"),
    DOSEN_TIDAK_TETAP("Dosen Tidak Tetap"),
    DOSEN_HONORER("Dosen Honorer");

    public final String label;

    private LecturerType(String label) {
        this.label = label;
    }
}
