package id.ac.tazkia.newsmile.constants;

public enum Gender {
    PRIA ("laki Laki"),
    WANITA ("Perempuan");

    public final String label;

    private Gender(String label) {
        this.label = label;
    }
}
