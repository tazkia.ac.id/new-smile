package id.ac.tazkia.newsmile.constants;

public enum StatusBanned {
    BANNED, UNBANNED
}
