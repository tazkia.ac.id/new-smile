package id.ac.tazkia.newsmile.constants;

public enum StatusRecord {
    ACTIVE, INACTIVE, DELETED
}
