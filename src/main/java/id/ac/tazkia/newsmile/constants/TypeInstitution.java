package id.ac.tazkia.newsmile.constants;

public enum TypeInstitution {
    INDUK, CABANG
}
