package id.ac.tazkia.newsmile.constants;

public enum StatusApprove {
    APPROVED, REJECTED, WAITING
}
