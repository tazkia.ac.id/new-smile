package id.ac.tazkia.newsmile.dto.foundation;

import id.ac.tazkia.newsmile.constants.StatusRecord;
import lombok.Data;

@Data
public class FoundationDto {
    private String id;
    private String foundationName;
    private String email;
    private String name;
    private StatusRecord status;

}
