package id.ac.tazkia.newsmile.dto.employee;

import id.ac.tazkia.newsmile.constants.Gender;
import lombok.Data;

import java.util.List;

@Data
public class RequestEmployee {
    private String id;
    private List<String> institution;
    private String nik;
    private String number;
    private String email;
    private String fullName;
    private String nickName;
    private String frontTitle;
    private String backTitle;
    private Gender gender;

}
