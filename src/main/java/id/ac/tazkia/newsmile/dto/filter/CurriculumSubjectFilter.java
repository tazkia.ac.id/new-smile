package id.ac.tazkia.newsmile.dto.filter;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class CurriculumSubjectFilter extends BaseRequestParamDto{
    private String prodi;
    private String curriculum;
    private List<Integer> semester;
    private String urlSemester;
}
