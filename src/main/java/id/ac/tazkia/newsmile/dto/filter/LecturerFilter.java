package id.ac.tazkia.newsmile.dto.filter;

import id.ac.tazkia.newsmile.entity.institution.InstitutionProdi;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
public class LecturerFilter extends BaseRequestParamDto{
    private List<String> type;
    private List<String> prodi;
    private List<String> gender;
    private String trash;
}
