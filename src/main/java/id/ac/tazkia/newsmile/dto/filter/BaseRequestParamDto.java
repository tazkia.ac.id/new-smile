package id.ac.tazkia.newsmile.dto.filter;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class BaseRequestParamDto {
    private String search;
    private int pageSize = 10;
    private int page;
}
