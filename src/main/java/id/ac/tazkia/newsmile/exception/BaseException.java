package id.ac.tazkia.newsmile.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

public class BaseException extends Exception{
    private @Getter
    @Setter String errorCode;
    private @Getter @Setter HttpStatus httpStatusCode;

    public BaseException() {
    }

    public BaseException(String message) {
        super(message);
    }

    public BaseException(String message, HttpStatus httpStatusCode) {
        super(message);
        this.errorCode = httpStatusCode.name();
        this.httpStatusCode = httpStatusCode;
    }

    public BaseException(String errorCode, String message, HttpStatus httpStatusCode) {
        super(message);
        this.errorCode = errorCode;
        this.httpStatusCode = httpStatusCode;
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseException(String errorCode, String message, HttpStatus httpStatusCode, Throwable cause) {
        super(message);
        this.errorCode = errorCode;
        this.httpStatusCode = httpStatusCode;
    }

    public BaseException(Throwable cause) {
        super(cause);
    }

    public BaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
