import "flowbite";

// dark mode toggle
const root = document.documentElement;
const toggleThemeButton = document.getElementById("toggle-theme");
const themeToggleDarkIcon = document.getElementById("theme-toggle-dark-icon");
const themeToggleLightIcon = document.getElementById("theme-toggle-light-icon");
if (
	localStorage.getItem("theme") === "dark" ||
	(!("theme" in localStorage) &&
		window.matchMedia("(prefers-color-scheme: dark)").matches)
) {
	root.classList.add("dark");
	if (toggleThemeButton) {
		themeToggleLightIcon.classList.remove("hidden");
		themeToggleDarkIcon.classList.add("hidden");
	}
} else {
	root.classList.remove("dark");
	if (toggleThemeButton) {
		themeToggleDarkIcon.classList.remove("hidden");
		themeToggleLightIcon.classList.add("hidden");
	}
}

function toggleTheme() {
	root.classList.toggle("dark");
	if (root.classList.contains("dark")) {
		localStorage.setItem("theme", "dark");
		themeToggleLightIcon.classList.remove("hidden");
		themeToggleDarkIcon.classList.add("hidden");
	} else {
		localStorage.setItem("theme", "light");
		themeToggleDarkIcon.classList.remove("hidden");
		themeToggleLightIcon.classList.add("hidden");
	}
}

if (toggleThemeButton) toggleThemeButton.addEventListener("click", toggleTheme);

// horizontal scrollable element

const itemsContainer = document.querySelector(
	".horizontal-scrollable-container"
);
let isDown = false;
let startX;
let scrollLeft;

if (itemsContainer) {
	itemsContainer.addEventListener("mousedown", (e) => {
		isDown = true;
		startX = e.pageX - itemsContainer.offsetLeft;
		scrollLeft = itemsContainer.scrollLeft;
	});
	itemsContainer.addEventListener("mouseleave", () => {
		isDown = false;
	});
	itemsContainer.addEventListener("mouseup", () => {
		isDown = false;
	});
	itemsContainer.addEventListener("mousemove", (e) => {
		if (!isDown) return;
		e.preventDefault();
		const x = e.pageX - itemsContainer.offsetLeft;
		const walk = (x - startX) * 2; //scroll-fast
		itemsContainer.scrollLeft = scrollLeft - walk;
	});
}

import {
	subDays,
	addDays,
	eachDayOfInterval,
	format,
	startOfMonth,
	compareAsc,
	startOfDay,
	addMonths,
	subMonths,
	getDay,
	parseISO,
	isWithinInterval,
	isSameMonth,
} from "date-fns";

let data = [];
let currentDate = startOfDay(new Date());
let currentMonth = startOfMonth(currentDate);

// function get calendar start return date
function getCalendarStart() {
	const dayNumInWeek = getDay(currentMonth);

	const calendarStart = subDays(
		currentMonth,
		dayNumInWeek !== 0 ? dayNumInWeek - 1 : 6
	);

	return calendarStart;
}

// function get event data
function getEvent() {
	const calendarStart = getCalendarStart();
	const calendarEnd = addDays(calendarStart, 41);

	return data; // data from API
}

let selectedDate = currentDate;
let events = [];

// function get days of calendar
function getDays() {
	const calendarStart = getCalendarStart();

	return eachDayOfInterval({
		start: calendarStart,
		end: addDays(calendarStart, 41),
	}).map((date) => {
		return {
			isCurrent: compareAsc(currentDate, date) === 0,
			isCurrentMonth: isSameMonth(date, currentMonth),
			isSelected: compareAsc(selectedDate, date) === 0,
			date: date,
			events: events.filter((event) => {
				return isWithinInterval(date, {
					start: parseISO(event.startDate),
					end: parseISO(event.endDate),
				});
			}),
		};
	});
}

// function select date
let elSelectedDate = document.getElementById("selected-date");

function selectDate(event) {
	let date = new Date(event.currentTarget.getAttribute("data-date-time"));
	selectedDate = date;
	elSelectedDate.innerHTML = format(selectedDate, "EEEE, dd MMMM yyyy");

	if (!isSameMonth(date, currentMonth)) {
		currentMonth = startOfMonth(date);
		events = getEvent();
	}

	renderDays();
}

// function add eventlistener to each days
function addDaysEventListener() {
	const dayList = document.querySelectorAll("#calendar-dates button");
	dayList.forEach((e) => {
		e.addEventListener("click", selectDate);
	});
}

// function update current month to DOM
let elCurrentMonth = document.getElementById("current-month");

function updateCurrentMonth() {
	elCurrentMonth.innerHTML = format(currentMonth, "MMMM yyyy");
}

// render event list to dom
let elEventList = document.getElementById("event-list");

function renderEvent(events) {
	let eventList = "";
	events.map((event) => {
		eventList += `<li class="flex items-center gap-2">
						<div class="size-2 rounded-full bg-orange-500"></div>
							<span class="text-gray-600 dark:text-gray-400">${event.event}</span>
					  </li>`;
	});
	elEventList.innerHTML = eventList;
}

let calendarDates = document.getElementById("calendar-dates");

// function render days to dom
function renderDays() {
	let html = "";
	let dayEvents;
	let days = getDays();

	days.map((day) => {
		let className = "";
		if (
			day.isCurrentMonth &&
			!day.isCurrent &&
			!day.isSelected &&
			!day.events.length > 0
		) {
			className = "text-gray-700 dark:text-gray-300";
		} else if ((day.isCurrent && day.isCurrentMonth) || day.isSelected) {
			className = "bg-blue-600 text-white";
		} else if (day.events.length > 0 && !day.isCurrent && !day.isSelected) {
			className =
				"bg-gray-100 text-gray-700 dark:bg-gray-700 dark:text-gray-400";
		} else {
			className = "text-gray-400 dark:text-gray-600";
		}

		if (day.isSelected) {
			dayEvents = day.events;
		}

		html += `<button type="button" data-date-time='${
			day.date
		}' class="mx-auto flex h-10 w-10 items-center justify-center rounded-lg ${className}"
	>${format(day.date, "d")}</button>`;
	});

	calendarDates.innerHTML = html;
	updateCurrentMonth();
	addDaysEventListener();
	renderEvent(dayEvents);
}

// fetch event data from API
function fetchEvents(start, end) {
	const url = `/public/get-events?start=${format(start, "yyyy-MM-dd")}&end=${format(end, "yyyy-MM-dd")}`;
	return fetch(url)
		.then(response => response.json())
		.then(data => {
			if (data.responseCode === "00") {
				return data.data;
			} else {
				console.error("Failed to fetch events:", data.responseMessage);
				return [];
			}
		})
		.catch(error => {
			console.error("Error fetching events:", error);
			return [];
		});
}

if (calendarDates) {
	// get initial calendar range
	const calendarStart = getCalendarStart();
	const calendarEnd = addDays(calendarStart, 41);

	// fetch events first time and render calendar
	fetchEvents(calendarStart, calendarEnd).then(fetchedEvents => {
		data = fetchedEvents;
		events = getEvent();
		renderDays();

		const prevBtn = document.getElementById("prev-btn");
		const nextBtn = document.getElementById("next-btn");

		prevBtn.addEventListener("click", function () {
			currentMonth = subMonths(currentMonth, 1);
			selectedDate = currentMonth;

			// get new calendar range
			const newCalendarStart = getCalendarStart();
			const newCalendarEnd = addDays(newCalendarStart, 41);

			// fetch events for the new range
			fetchEvents(newCalendarStart, newCalendarEnd).then(newFetchedEvents => {
				data = newFetchedEvents;
				events = getEvent();
				renderDays();
			});
		});

		nextBtn.addEventListener("click", function () {
			currentMonth = addMonths(currentMonth, 1);
			selectedDate = currentMonth;

			// get new calendar range
			const newCalendarStart = getCalendarStart();
			const newCalendarEnd = addDays(newCalendarStart, 41);

			// fetch events for the new range
			fetchEvents(newCalendarStart, newCalendarEnd).then(newFetchedEvents => {
				data = newFetchedEvents;
				events = getEvent();
				renderDays();
			});
		});
	});
}


// form validation
(() => {
	"use strict";

	// Fetch all the forms we want to apply validation styles to
	const forms = document.querySelectorAll("form[novalidate]");

	// Loop over them and prevent submission
	Array.from(forms).forEach((form) => {
		form.addEventListener(
			"submit",
			(event) => {
				if (!form.checkValidity()) {
					event.preventDefault();
					event.stopPropagation();
				}

				const inputs = form.getElementsByClassName("needs-validation");

				Array.from(inputs).forEach((input) => {
					const isValidate = input.checkValidity();
					if (!isValidate) {
						input.classList.add(
							"invalid",
							"border-red-500",
							"dark:border-red-500",
							"bg-red-50",
							"focus:border-red-600",
							"dark:focus:border-red-600",
							"focus:ring-red-600",
							"dark:focus:ring-red-600",
							"text-red-600",
							"dark:text-red-600",
							"placeholder-red-700",
							"dark:placeholder-red-500"
						);

						const errorEl = form.querySelector("#error" + input.name);

						if (errorEl) {
							errorEl.innerHTML = input.validationMessage;
							errorEl.classList.remove("hidden");
						}
					}
				});
			},
			false
		);
	});
})();
