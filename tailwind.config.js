/** @type {import('tailwindcss').Config} */
const defaultTheme = require("tailwindcss/defaultTheme");
module.exports = {
	content: ["./src/main/resources/**/*.{html,js}", "./frontend/main.js"],
	darkMode: "class",
	theme: {
		extend: {
			colors: {
				blue: {
					50: "#edf8ff",
					100: "#d7eeff",
					200: "#b8e3ff",
					300: "#88d3ff",
					400: "#50baff",
					500: "#2799ff",
					600: "#107aff",
					700: "#0962ec",
					800: "#0e4fbf",
					900: "#124596",
					950: "#102b5b",
				},
				orange: {
					50: "#fff7ed",
					100: "#feedd6",
					200: "#fcd8ac",
					300: "#fabb77",
					400: "#f79440",
					500: "#f4761b",
					600: "#e55c11",
					700: "#be4510",
					800: "#973715",
					900: "#7a2f14",
					950: "#421608",
				},
				purple: {
					50: "#f9f5ff",
					100: "#f1e7ff",
					200: "#e6d3ff",
					300: "#d2b0ff",
					400: "#b67eff",
					500: "#9747ff",
					600: "#832af3",
					700: "#701ad6",
					800: "#601aaf",
					900: "#50178c",
					950: "#340269",
				},
			},
			fontFamily: {
				sans: ["Inter", ...defaultTheme.fontFamily.sans],
			},
		},
	},
	plugins: [
		require("flowbite/plugin")({
			charts: true,
		}),
	],
};
